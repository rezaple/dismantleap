<?php

use App\Http\Controllers\Backend\DashboardController;

// All route names are prefixed with 'admin.'.
// Route::redirect('/', '/dashboard', 301);

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/dataku', 'DashboardController@data')->name('data');
// Route::post('/data', 'PengajuanController@storeDoneData')->name('data.store');
// Route::get('/upload', 'DashboardController@showUploadView')->name('upload.view');
Route::get('/datatrend', 'DashboardController@trendline')->name('trendline');
Route::get('profile', [DashboardController::class, 'profile'])->name('profile');

Route::get('/show-submissions', 'DashboardController@showSubmission')->name('show-submission');
Route::get('/data-subs', 'DashboardController@dataSubs')->name('data-subs');
Route::delete('/data-subs/{id?}', 'DashboardController@destroySubs')->name('data-subs.delete');
