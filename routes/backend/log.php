<?php

Route::group([
    'prefix' => 'logs',
    'as' => 'logs.',
    'middleware' => ['permission:view logs'],
], function () {
    Route::get('/', 'ActivityLogController@index')->name('index');
});
