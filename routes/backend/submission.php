<?php

Route::group([
    'prefix' => 'submission',
    'as' => 'submission.',
    'middleware' => ['permission:manage submissions'],
], function () {
    Route::get('/', 'PengajuanController@index')->name('index');
    Route::get('/data', 'PengajuanController@list')->name('list');
    Route::post('/', 'PengajuanController@store')->name('store');
    // Route::post('/test-store', 'PengajuanController@testStore')->name('test.store');
    // Route::post('/{id}/revision', 'PengajuanController@storeRevision')->name('store-revision');
    Route::get('/create', 'PengajuanController@create')->name('create');
    // Route::get('/create-test', 'PengajuanController@createTest')->name('create.test');
    Route::get('/{id?}', 'PengajuanController@show')->name('show');
    Route::get('/{id}/cap', 'PengajuanController@listCAP')->name('cap');
    Route::delete('/{id?}', 'PengajuanController@destroy')->name('delete');
    Route::post('/{id}/to-wdm', 'PengajuanController@submitToWDM')->name('submit-to-wdm');
    Route::post('/{id}/document', 'PengajuanController@uploadDocument')->name('upload-document');
    Route::post('/{id}/revision-to-wdm', 'PengajuanController@submitRevisionToWDM')->name('revision-to-wdm');

    Route::post('/{id}/to-gas', 'PengajuanController@submitToGAS')->name('submit-to-gas');
    Route::post('/{id}/revision-to-gas', 'PengajuanController@submitRevisionToGAS')->name('revision-to-gas');
});
