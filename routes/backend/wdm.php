<?php

Route::group([
    'prefix' => 'wdm',
    'as' => 'wdm.',
    'middleware' => ['permission:manage approval wdm'],
], function () {
    Route::get('/', 'WdmController@index')->name('index');
    Route::get('/data', 'WdmController@list')->name('list');
    Route::get('/{id?}', 'WdmController@show')->name('show');
    Route::get('/{id}/cap', 'WdmController@listCAP')->name('cap');
    Route::post('/{id}/reject', 'WdmController@rejectSubmission')->name('reject');
    Route::post('/{id}/return', 'WdmController@returnSubmission')->name('return');
    Route::get('/{id}/approve', 'WdmController@approveSubmission')->name('approve');
    Route::post('/{id}/approve', 'WdmController@approveSubmission')->name('approve');
});
