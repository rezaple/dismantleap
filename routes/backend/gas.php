<?php

Route::group([
    'prefix' => 'gas',
    'as' => 'gas.',
    'middleware' => ['permission:manage approval gas'],
], function () {
    Route::get('/', 'GasController@index')->name('index');
    Route::get('/data', 'GasController@list')->name('list');
    Route::get('/{id?}', 'GasController@show')->name('show');
    Route::get('/{id}/cap', 'GasController@listCAP')->name('cap');
    Route::get('/{id}/reject', 'GasController@rejectSubmission')->name('reject');
    Route::post('/{id}/reject', 'GasController@rejectSubmission')->name('reject');
    Route::post('/{id}/return', 'GasController@returnSubmission')->name('return');
    Route::get('/{id}/approve', 'GasController@approveSubmission')->name('approve');
    Route::post('/{id}/approve', 'GasController@approveSubmission')->name('approve');
});
