<?php

use App\Mail\ApproveMail;
use App\Models\Auth\User;
use App\Jobs\SendCredentialEmail;

// use App\Http\Controllers\LanguageController;

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */
// Route::get('send-mail', function ()
// {
//     $mail = "nurul.muhsinin@telkom.co.id";
//     $data = [
//         'uuid' => 'asdasdasdasdas',
//         'user' => [
//             'name' => 'asda',
//             'email' => $mail
//         ]
//     ];
//     $data['pesan'] = 'Disetujui';
//     $data['by_user'] = 'User WDM';
//     $userWdm = User::select('id','uuid','email','name')->whereHas('roles', function ($query) {
//         $query->where('name', 'wdm');
//     })->get();
//     $emails = $userWdm->pluck('email');
//     return $emails;

//     \Mail::send('emails.mail', $data, function ($m) use ($userWdm) {
//         $m->from('hello@app.com', 'Your Application');

//         $m->to(['reza.muhsinin@gmail.com','nurul.muhsinin@telkom.co.id'])->subject('Your Tes Mail!');
//     });

    //\Mail::to($mail)->send(new ApproveMail($data));

    // \Mail::send('emails.mail', $data, function ($m) use ($data) {
    //     $m->from('hello@app.com', 'Your Application');

    //     $m->to($data['user']['email'], $data['user']['name'])->subject('Your Reminder!');
    // });
// });

Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    include_route_files(__DIR__.'/frontend/');
});

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'panel', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     * These routes can not be hit if the password is expired
     */
    include_route_files(__DIR__.'/backend/');
});

// Route::get('/clear-cache', function() {
//     $exitCode = Artisan::call('config:clear');
//     $exitCode = Artisan::call('cache:clear');
//     $exitCode = Artisan::call('config:cache');
//     return 'DONE'; //Return anything
// });