<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.logs.index', function ($trail) {
    $trail->push('Data Logs', route('admin.logs.index'));
});

Breadcrumbs::for('admin.profile', function ($trail) {
    $trail->push('Profile', route('admin.profile'));
});

Breadcrumbs::for('admin.submission.index', function ($trail) {
    $trail->push(__('strings.backend.submission.title'), route('admin.submission.index'));
});

Breadcrumbs::for('admin.submission.create', function ($trail) {
    $trail->parent('admin.submission.index');
    $trail->push(__('strings.backend.submission.create'), route('admin.submission.create'));
});

Breadcrumbs::for('admin.submission.show', function ($trail) {
    $trail->parent('admin.submission.index');
    $trail->push('Detail Submission', route('admin.submission.index'));
});

Breadcrumbs::for('admin.wdm.index', function ($trail) {
    $trail->push(__('strings.backend.submission.title'), route('admin.wdm.index'));
});

Breadcrumbs::for('admin.wdm.show', function ($trail) {
    $trail->parent('admin.wdm.index');
    $trail->push('Detail Submission', route('admin.wdm.index'));
});

Breadcrumbs::for('admin.gas.index', function ($trail) {
    $trail->push(__('strings.backend.submission.title'), route('admin.gas.index'));
});

Breadcrumbs::for('admin.gas.show', function ($trail) {
    $trail->parent('admin.gas.index');
    $trail->push('Detail Submission', route('admin.gas.index'));
});

require __DIR__.'/auth.php';
