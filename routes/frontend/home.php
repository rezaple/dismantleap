<?php

use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::group(['middleware' => 'guest'], function () {
    Route::get('/', [HomeController::class, 'index'])->name('index');
    Route::get('/otp/telegram', [HomeController::class, 'showOtpTelegram'])->name('otp.telegram');
    Route::get('/otp', [HomeController::class, 'showVerification'])->name('otp');
    Route::get('/otp/verifiy', [HomeController::class, 'showInputVerification'])->name('otp.verify');
});
/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth']], function () {

    Route::get('/check-user-ldap/{username}', [HomeController::class, 'checkUserLdap']);
    Route::get('/check-user-local/{username}', [HomeController::class, 'checkUserLocal']);

    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        
        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });
});
