import Echo from 'laravel-echo';
window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'ac345b02f7d4608001f3',
    cluster: 'ap1',
    encrypted: true
});
