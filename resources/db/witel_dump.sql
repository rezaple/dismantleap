-- Adminer 4.7.1 PostgreSQL dump

DROP TABLE IF EXISTS "witel";

CREATE TABLE "witel" (
    "id" character varying(255),
    "witel" character varying(255),
    "divisi" character varying(255),
    "urut" character varying(255),
    "witel_txt" character varying(255),
    "witel_2" character varying(255),
    "sink_witel" character varying(255),
    "regional" character varying(255),
    "bsr" character varying(255),
    "provinsi" character varying(255),
    "msn" character varying(255)
) WITH (oids = false);

INSERT INTO "witel" ("id", "witel", "divisi", "urut", "witel_txt", "witel_2", "sink_witel", "regional", "bsr", "provinsi", "msn") VALUES
('AABetmABAAABL+DAAA',	'GORONTALO',	'TIMUR',	'63',	'GORONTALO',	'GORONTALO',	'GORONTALO',	'7',	'10',	'GORONTALO',	'49'),
('AABetmABAAABL+EAAA',	'BANDUNGBRT',	'BARAT',	'64',	'BANDUNG BARAT',	'BANDUNGBRT',	'BANDUNGBRT',	'3',	'5',	'JAWA BARAT',	'62'),
('AABetmABAAABL+FAAA',	'SIDOARJO',	'TIMUR',	'39',	'TELKOM JATIM TENGAH TIMUR (SIDOARJO)',	'SIDOARJO',	'SIDOARJO',	'5',	'7',	'JAWA TIMUR',	'37'),
('AABetmABAAABL+FAAB',	'RIKEP',	'BARAT',	'4',	'TELKOM RIAU KEPULAUAN (BATAM)',	'BATAM',	'BATAM',	'1',	'1',	'RIAU',	'7'),
('AABetmABAAABL+FAAC',	'JAKARTA BARAT',	'BARAT',	'14',	'TELKOM JAKARTA BARAT',	'JAKARTA BARAT',	'JAK. BARAT',	'2',	'4',	'DKI JAKARTA',	'12'),
('AABetmABAAABL+FAAD',	'JAKARTA PUSAT',	'BARAT',	'15',	'TELKOM JAKARTA PUSAT',	'JAKARTA PUSAT',	'JAK. PUSAT',	'2',	'4',	'DKI JAKARTA',	'14'),
('AABetmABAAABL+FAAE',	'JAKARTA TIMUR',	'BARAT',	'17',	'TELKOM JAKARTA TIMUR',	'JAKARTA TIMUR',	'JAK. TIMUR',	'2',	'4',	'DKI JAKARTA',	'16'),
('AABetmABAAABL+FAAF',	'PEKALONGAN',	'TIMUR',	'26',	'TELKOM JATENG BARAT UTARA (PEKALONGAN)',	'PEKALONGAN',	'PEKALONGAN',	'4',	'6',	'JAWA TENGAH',	'27'),
('AABetmABAAABL+FAAG',	'YOGYAKARTA',	'TIMUR',	'32',	'TELKOM DI YOGYAKARTA',	'YOGYAKARTA',	'YOGYAKARTA',	'4',	'6',	'YOGYAKARTA',	'33'),
('AABetmABAAABL+FAAH',	'JABAR BARAT (BOGOR)',	'BARAT',	'21',	'TELKOM JABAR BARAT (BOGOR)',	'BOGOR',	'BOGOR',	'2',	'5',	'JAWA BARAT',	'19'),
('AABetmABAAABL+FAAI',	'SURABAYA UTARA',	'TIMUR',	'37',	'TELKOM JATIM UTARA (GRESIK)',	'GRESIK',	'GRESIK',	'5',	'7',	'JAWA TIMUR',	'39'),
('AABetmABAAABL+FAAJ',	'JAKARTA UTARA',	'BARAT',	'16',	'TELKOM JAKARTA UTARA',	'JAKARTA UTARA',	'JAK. UTARA',	'2',	'4',	'DKI JAKARTA',	'15'),
('AABetmABAAABL+FAAK',	'PAPUA',	'TIMUR',	'61',	'TELKOM PAPUA (JAYAPURA)',	'JAYAPURA',	'JAYAPURA',	'7',	'10',	'PAPUA',	'61'),
('AABetmABAAABL+FAAM',	'SULTRA',	'TIMUR',	'55',	'TELKOM SULTRA (KENDARI)',	'KENDARI',	'KENDARI',	'7',	'10',	'SULAWESI TENGGARA',	'53'),
('AABetmABAAABL+FAAN',	'NTT',	'TIMUR',	'51',	'TELKOM NTT (KUPANG)',	'KUPANG',	'KUPANG',	'5',	'9',	'NUSA TENGGARA TIMUR',	'57'),
('AABetmABAAABL+FAAO',	'MALANG',	'TIMUR',	'36',	'TELKOM JATIM SELATAN (MALANG)',	'MALANG',	'MALANG',	'5',	'7',	'JAWA TIMUR',	'36'),
('AABetmABAAABL+FAAP',	'SULTENG',	'TIMUR',	'54',	'TELKOM SULTENG (PALU)',	'PALU',	'PALU',	'7',	'10',	'SULAWESI TENGAH',	'50'),
('AABetmABAAABL+FAAR',	'SURABAYA SELATAN',	'TIMUR',	'38',	'TELKOM JATIM SURAMADU (SURABAYA)',	'SURABAYA',	'SURABAYA',	'5',	'7',	'JAWA TIMUR',	'38'),
('AABetmABAAABL+FAAS',	'BENGKULU',	'BARAT',	'8',	'TELKOM BENGKULU (BENGKULU)',	'BENGKULU',	'BENGKULU',	'1',	'3',	'BENGKULU',	'8'),
('AABetmABAAABL+FAAT',	'DENPASAR',	'TIMUR',	'48',	'TELKOM BALI SELATAN (DENPASAR)',	'BALI SELATAN',	'BALI SELATAN',	'5',	'9',	'BALI',	'54'),
('AABetmABAAABL+FAAU',	'MEDAN',	'BARAT',	'3',	'TELKOM SUMUT BARAT (MEDAN)',	'MEDAN',	'MEDAN',	'1',	'1',	'SUMATERA UTARA',	'2'),
('AABetmABAAABL+FAAV',	'CIREBON',	'BARAT',	'23',	'TELKOM JABAR TIMUR (CIREBON)',	'CIREBON',	'CIREBON',	'3',	'5',	'JAWA BARAT',	'25'),
('AABetmABAAABL+FAAW',	'KALTENG',	'TIMUR',	'44',	'TELKOM KALTENG (PALANGKARAYA)',	'PALANGKARAYA',	'PALANGKARAYA',	'6',	'8',	'KALIMANTAN TENGAH',	'43'),
('AABetmABAAABL+FAAX',	'BANTEN TIMUR (TANGERANG)',	'BARAT',	'13',	'TELKOM BANTEN TIMUR (TANGERANG)',	'TANGERANG',	'TANGERANG',	'2',	'4',	'BANTEN',	'18'),
('AABetmABAAABL+FAAZ',	'KUDUS',	'TIMUR',	'29',	'TELKOM JATENG TIMUR UTARA (KUDUS)',	'KUDUS',	'KUDUS',	'4',	'6',	'JAWA TENGAH',	'31'),
('AABetmABAAABL+FAAa',	'SINGARAJA',	'TIMUR',	'49',	'TELKOM BALI UTARA (SINGARAJA)',	'BALI UTARA',	'BALI UTARA',	'5',	'9',	'BALI',	'55'),
('AABetmABAAABL+FAAb',	'SUMUT',	'BARAT',	'2',	'TELKOM SUMUT TIMUR (PEMATANG SIANTAR)',	'PEMATANGSIANTAR',	'P. SIANTAR',	'1',	'1',	'SUMATERA UTARA',	'3'),
('AABetmABAAABL+FAAc',	'MADIUN',	'TIMUR',	'34',	'TELKOM JATIM BARAT (MADIUN)',	'MADIUN',	'MADIUN',	'5',	'7',	'JAWA TIMUR',	'34'),
('AABetmABAAABL+FAAd',	'KALTARA',	'TIMUR',	'46',	'TELKOM KALTIMUT (TARAKAN)',	'TARAKAN',	'TARAKAN',	'6',	'8',	'KALIMANTAN TIMUR',	'47'),
('AABetmABAAABL+FAAe',	'BABEL',	'BARAT',	'9',	'TELKOM BANGKA BELITUNG (PANGKAL PINANG)',	'PANGKALPINANG',	'P. PINANG',	'1',	'3',	'BANGKA BELITUNG',	'10'),
('AABetmABAAABL+FAAf',	'KALBAR',	'TIMUR',	'42',	'TELKOM KALBAR (PONTIANAK)',	'PONTIANAK',	'PONTIANAK',	'6',	'8',	'KALIMANTAN BARAT',	'42'),
('AABetmABAAABL+FAAg',	'ACEH',	'BARAT',	'1',	'TELKOM NAD (ACEH)',	'NAD',	'NAD',	'1',	'1',	'NAD ACEH',	'1'),
('AABetmABAAABL+FAAh',	'TASIKMALAYA',	'BARAT',	'24',	'TELKOM JABAR TIMSEL (TASIKMALAYA)',	'TASIKMALAYA',	'TASIKMALAYA',	'3',	'5',	'JAWA BARAT',	'24'),
('AABetmABAAABL+FAAi',	'SAMARINDA',	'TIMUR',	'45',	'TELKOM KALTIMTENG (SAMARINDA)',	'SAMARINDA',	'SAMARINDA',	'6',	'8',	'KALIMANTAN TIMUR',	'46'),
('AABetmABAAABL+FAAj',	'SEMARANG',	'TIMUR',	'28',	'TELKOM JATENG UTARA (SEMARANG)',	'SEMARANG',	'SEMARANG',	'4',	'6',	'JAWA TENGAH',	'32'),
('AABetmABAAABL+FAAk',	'NTB',	'TIMUR',	'50',	'TELKOM NTB (MATARAM)',	'MATARAM',	'MATARAM',	'5',	'9',	'NUSA TENGGARA BARAT',	'56'),
('AABetmABAAABL+FAAl',	'SULSELBAR',	'TIMUR',	'53',	'TELKOM SULSEL BARAT (PARE-PARE)',	'PARE-PARE',	'PARE-PARE',	'7',	'10',	'SULAWESI SELATAN',	'51'),
('AABetmABAAABL+FAAm',	'RIDAR',	'BARAT',	'5',	'TELKOM RIAU DARATAN (PEKANBARU)',	'PEKANBARU',	'PEKANBARU',	'1',	'2',	'RIAU',	'5'),
('AABetmABAAABL+FAAn',	'MALUKU',	'TIMUR',	'59',	'TELKOM MALUKU (AMBON)',	'AMBON',	'AMBON',	'7',	'10',	'MALUKU',	'59'),
('AABetmABAAABL+FAAo',	'LAMPUNG',	'BARAT',	'11',	'TELKOM LAMPUNG (BANDAR LAMPUNG)',	'LAMPUNG',	'LAMPUNG',	'1',	'3',	'LAMPUNG',	'11'),
('AABetmABAAABL+FAAp',	'KALSEL',	'TIMUR',	'43',	'TELKOM KALSEL (BANJARMASIN)',	'BANJARMASIN',	'BANJARMASIN',	'6',	'8',	'KALIMANTAN SELATAN',	'44'),
('AABetmABAAABL+FAAq',	'JABAR BARAT UTARA (BEKASI)',	'BARAT',	'19',	'TELKOM JABAR BARAT UTARA (BEKASI)',	'BEKASI',	'BEKASI',	'2',	'4',	'JAWA BARAT',	'20'),
('AABetmABAAABL+FAAr',	'SUKABUMI',	'BARAT',	'22',	'TELKOM JABAR SELATAN (SUKABUMI)',	'SUKABUMI',	'SUKABUMI',	'3',	'5',	'JAWA BARAT',	'22'),
('AABetmABAAABL+FAAs',	'BANTEN (SERANG)',	'BARAT',	'12',	'TELKOM BANTEN BARAT (SERANG)',	'SERANG',	'SERANG',	'2',	'4',	'BANTEN',	'17'),
('AABetmABAAABL+FAAt',	'MAGELANG',	'TIMUR',	'31',	'TELKOM JATENG SELATAN (MAGELANG)',	'MAGELANG',	'MAGELANG',	'4',	'6',	'JAWA TENGAH',	'28'),
('AABetmABAAABL+FAAv',	'JAKARTA SELATAN',	'BARAT',	'18',	'TELKOM JAKARTA SELATAN',	'JAKARTA SELATAN',	'JAK. SELATAN',	'2',	'4',	'DKI JAKARTA',	'13'),
('AABetmABAAABL+FAAw',	'SUMSEL',	'BARAT',	'10',	'TELKOM SUMATERA SELATAN (PALEMBANG)',	'PALEMBANG',	'PALEMBANG',	'1',	'3',	'SUMATERA SELATAN',	'9'),
('AABetmABAAABL+FAAx',	'PAPUA BARAT',	'TIMUR',	'60',	'TELKOM PAPUA BARAT (SORONG)',	'SORONG',	'SORONG',	'7',	'10',	'PAPUA BARAT',	'60'),
('AABetmABAAABL+FAAy',	'KARAWANG',	'BARAT',	'20',	'TELKOM JABAR UTARA (KARAWANG)',	'KARAWANG',	'KARAWANG',	'3',	'4',	'JAWA BARAT',	'21'),
('AABetmABAAABL+FAAz',	'KEDIRI',	'TIMUR',	'35',	'TELKOM JATIM TENGAH (KEDIRI)',	'KEDIRI',	'KEDIRI',	'5',	'7',	'JAWA TIMUR',	'35'),
('AABetmABAAABL+FAA0',	'SULUT MALUT',	'TIMUR',	'57',	'TELKOM SULUT MALUT',	'MANADO',	'MANADO',	'7',	'10',	'SULAWESI UTARA',	'48'),
('AABetmABAAABL+FAA1',	'PASURUAN',	'TIMUR',	'41',	'TELKOM JATIM SELATAN TIMUR (PASURUAN)',	'PASURUAN',	'PASURUAN',	'5',	'7',	'JAWA TIMUR',	'40'),
('AABetmABAAABL+FAA2',	'PURWOKERTO',	'TIMUR',	'27',	'TELKOM JATENG BARAT SELATAN (PURWOKERTO)',	'PURWOKERTO',	'PURWOKERTO',	'4',	'6',	'JAWA TENGAH',	'26'),
('AABetmABAAABL+FAA3',	'BALIKPAPAN',	'TIMUR',	'47',	'TELKOM KALTIMSEL (BALIKPAPAN)',	'BALIKPAPAN',	'BALIKPAPAN',	'6',	'8',	'KALIMANTAN TIMUR',	'45'),
('AABetmABAAABL+FAA4',	'BANDUNG',	'BARAT',	'25',	'TELKOM JABAR TENGAH (BANDUNG)',	'BANDUNG',	'BANDUNG',	'3',	'5',	'JAWA BARAT',	'23'),
('AABetmABAAABL+FAA5',	'SOLO',	'TIMUR',	'33',	'TELKOM JATENG TIMUR SELATAN (SOLO)',	'SOLO',	'SOLO',	'4',	'6',	'JAWA TENGAH',	'30'),
('AABetmABAAABL+FAA6',	'JAMBI',	'BARAT',	'7',	'TELKOM JAMBI',	'JAMBI',	'JAMBI',	'1',	'2',	'JAMBI',	'6'),
('AABetmABAAABL+FAA7',	'MAKASAR',	'TIMUR',	'52',	'TELKOM SULSEL (MAKASAR)',	'MAKASSAR',	'MAKASSAR',	'7',	'10',	'SULAWESI SELATAN',	'52'),
('AABetmABAAABL+FAA8',	'SUMBAR',	'BARAT',	'6',	'TELKOM SUMATERA BARAT (PADANG)',	'PADANG',	'PADANG',	'1',	'2',	'SUMATERA BARAT',	'4'),
('AABetmABAAABL+FAA9',	'JEMBER',	'TIMUR',	'40',	'TELKOM JATIM TIMUR (JEMBER)',	'JEMBER',	'JEMBER',	'5',	'7',	'JAWA TIMUR',	'41'),
('AABetmABAAABL+HAAA',	'MADURA',	'TIMUR',	'65',	'MADURA',	'MADURA',	'MADURA',	'5',	'7',	'JAWA TIMUR',	'63');

-- 2020-07-27 10:35:56.403057+07
