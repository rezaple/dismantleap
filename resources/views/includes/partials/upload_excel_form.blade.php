<div class="row">
    <div class="col">
    <form action="{{route('admin.data.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.submission.file'))->class('col-md-2 form-control-label')->for('file') }}

            <div class="col-md-10">
            <input type="file" name="file" placeholder="{{__('validation.attributes.backend.submission.list')}}">
            </div><!--col-->
        </div><!--form-group-->
        <input type="submit" class="btn btn-primary btn-md" value="Upload">
        </form>
    </div>
</div>