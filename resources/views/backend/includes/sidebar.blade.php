<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('panel/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            @if($logged_in_user->hasPermissionTo('manage submissions'))
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Request::is('panel/submission/*'))
                }}" href="{{ route('admin.submission.index') }}">
                    <i class="nav-icon fas fa-file-alt"></i>
                    @lang('menus.backend.sidebar.submission')
                </a>
            </li>
            @endif

            @if($logged_in_user->hasPermissionTo('manage approval wdm'))
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Request::is('panel/wdm/*'))
                }}" href="{{ route('admin.wdm.index') }}">
                    <i class="nav-icon fas fa-file-alt"></i>
                    @lang('menus.backend.sidebar.submission-list')
                </a>
            </li>
            @endif

            @if($logged_in_user->hasPermissionTo('manage approval gas'))
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Request::is('panel/gas/*'))
                }}" href="{{ route('admin.gas.index') }}">
                    <i class="nav-icon fas fa-file-alt"></i>
                    @lang('menus.backend.sidebar.submission-list')
                </a>
            </li>
            @endif

            @if ($logged_in_user->isAdmin() || $logged_in_user->hasAnyPermission(['manage roles', 'manage users']))
                <li class="nav-title">
                    @lang('menus.backend.sidebar.system')
                </li>

                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('panel/auth*'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                        active_class(Route::is('panel/auth*'))
                    }}" href="#">
                        <i class="nav-icon far fa-user"></i>
                        @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        @if ($logged_in_user->isAdmin() || $logged_in_user->hasPermissionTo('manage users'))
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('panel/auth/user*'))
                            }}" href="{{ route('admin.auth.user.index') }}">
                                @lang('labels.backend.access.users.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        @endif

                        @if ($logged_in_user->isAdmin() || $logged_in_user->hasPermissionTo('manage roles'))
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('panel/auth/role*'))
                            }}" href="{{ route('admin.auth.role.index') }}">
                                @lang('labels.backend.access.roles.management')
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>

                <li class="divider"></li>
            @endif


            @if($logged_in_user->isAdmin() || $logged_in_user->hasPermissionTo('view logs'))
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Request::is('panel/logs/*'))
                }}" href="{{ route('admin.logs.index') }}">
                    <i class="nav-icon fas fa-list"></i>
                    Data Log
                </a>
            </li>
            @endif



        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
