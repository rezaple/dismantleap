@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@push('css')
<style>
  .highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  .highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  .highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
  }

  .highcharts-data-table td,
  .highcharts-data-table th,
  .highcharts-data-table caption {
    padding: 0.5em;
  }

  .highcharts-data-table thead tr,
  .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
  }

  .highcharts-data-table tr:hover {
    background: #f1f7ff;
  }

  #circlebg {
    font-size: 2rem;
    line-height: 1rem;
    text-align: center;
    box-shadow: 0px 3px 10px rgba(0, 0, 0, .3);
    z-index: 2;
    width: 150px;
    height: 150px;
    border-radius: 100%;
    position: absolute;
    left: 36%;
    top: 21%;
    padding-top: 10%;
  }

  #circlebg span {
    font-size: 1rem;
  }
</style>
@endpush
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
  <div class="row">
    <div class="col-lg-12">
      <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
          <div class="form-group row">
            <label for="example-date-input" class="col-2 col-form-label">Pilih Tanggal</label>
            <div class="col-4">
              <input class="form-control" type="date" value="2020-06-22" id="example-date-input">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @if(auth()->user()->regional != null)
  <div class="row">
  <div class="col-lg-4">
		<div class="kt-portlet kt-portlet--solid-default kt-portlet--height-fluid kt-portlet--bordered"> 
			<div class="kt-portlet__body">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img src="" class="img-responsive" style="max-width: 90%">
					</div>
					<div class="col-sm-9">

						<h4 class="valuebgdash">
							<a class="text" id="jmlDraft">{{ $logged_in_user->name }}</a>
						</h4>
            <h6 class="valuebgdash">
							<a class="text" id="jmlDraft"> Regional {{ $logged_in_user->regional }}</a>
						</h6>
					</div>
				</div>
			</div>
		</div>
  </div>
  
  <div class="col-lg-4">
		<div class="kt-portlet kt-portlet--solid-default kt-portlet--height-fluid kt-portlet--bordered">
			<div class="kt-portlet__body">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img src="{{asset ('assets')}}/images/submit.svg" class="img-responsive" style="max-width: 120%">
					</div>
					<div class="col-sm-9">
						<div class="text-warning">Pengajuan On Progress </div>
						<h4 class="valuebgdash">
              <a class="text-warning" id="jmlDraft">{{$jumlah_submitt}}</a>
            </h4>
          
            <!-- <h4 class="valuebgdash">
              <a class="text-warning" id="jmlDraft">Rp. {{number_format($nilai_submit->nilai,2,',', '.')}}</a>
						</h4> -->
					</div>
				</div>
			</div>
		</div>
  </div>

  <div class="col-lg-4">
		<div class="kt-portlet kt-portlet--solid-default kt-portlet--height-fluid kt-portlet--bordered"> 
			<div class="kt-portlet__body">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img src="{{asset ('assets')}}/images/approve.svg" class="img-responsive" style="max-width: 120%">
					</div>
					<div class="col-sm-9">
						<div class="text-success">Approve Pengajuan</div>
						<h4 class="valuebgdash">
							<a class="text-success" id="jmlDraft">{{$jumlah_approve}}</a>
            </h4>
            <h4 class="valuebgdash">
							<a class="text-success" id="jmlDraft">Rp. {{number_format($nilai_approve->nilai,2,',', '.')}}</a>
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif

@if(auth()->user()->regional == null)
  <div class="row">
    <!--begin: Datatable -->
    <div class="col-md-12">
      <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body table-responsive">
          <table class="table table-striped- table-bordered table-hover table-checkable" id="testing">
            <thead>
              <tr>
                <th rowspan="2" style="text-align:center;background-color: #2284ad;color: #ffffff;">REGIONAL</th>
                <th colspan="2" style="background-color: #eeeeee;text-align:center">PENGAJUAN</th>
                <th colspan="2" style="background-color: #eeeeee;text-align:center">JUMLAH AP</th>
                <th colspan="2" style="background-color: #eeeeee;text-align:center">PENYERAPAN ANGGARAN</th>
              </tr>
              <tr>
                <td style="text-align:center">SUBMIT</td>
                <td style="text-align:center">APPROVE</td>
                <td style="text-align:center">MTD</td>
                <td style="text-align:center">YTD</td>
                <td style="text-align:center">MTD</td>
                <td style="text-align:center">YTD</td>
              </tr>
            </thead>
            <tbody>
             
            </tbody>
            <tfoot>
              <tr>
                <td style="text-align:center;background-color: #2284ad;color: #ffffff;">NASIONAL</td>
                <td style="text-align:right">508</td>
                <td style="text-align:right">690</td>
                <td style="text-align:right">1.049</td>
                <td style="text-align:right">1.993</td>
                <td style="text-align:right">135.766.250</td>
                <td style="text-align:right">400.839.600</td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
  @endif
   <div class="row">
    <!--end: Datatable -->
    <div class="col-md-6">
      <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
          <div id="circlebg">{{$tot}}<span><br>Total</span></div>
          <div id="chartdonut" style="width: 450px; height: 310px; margin: -1rem auto;"></div>
          <!-- <div id="empty" style="width: 450px; height: 310px; margin: -3rem auto;"></div> -->
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="kt-portlet kt-portlet--mobile">
        <figure class="highcharts-figure">
          <div id="container"></div>
        </figure>
      </div>
    </div>
  </div>

</div>

@endsection

@push('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="{{ asset('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script>
var branch = $('#testing').DataTable({
        ajax: {
        		url: '{{route("admin.data")}}',
        		type: 'GET',
        		dataSrc: 'results'
        },
        "columns": [
            {"data": "regional",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + 'Regional ' + data + '</center></font>'
                        }
            },
            {"data": "ap_submit",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
            },
            {"data": "ap_approve",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
              },
            {"data": "ap_mtd",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
            },
            {"data": "ap_ytd",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
                      },
            {"data": "anggaran_mtd",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
              },
            {"data": "anggaran_mtd",
              "render": function(data, type, row) {
                        return '<font color="black"><center>' + data + '</center></font>'
                        }
            },
        ],
        "bPaginate": false,
        "bLengthChange": false,
        "bInfo": false,
        "ordering": false,
        "dom": 'lBfrtip',
        "destroy": true,
        "sErrMode": "throw",
        "scrollCollapse": true
    });
</script>
<script>
var dttb = $('#dt_tb').DataTable({
        initComplete : function() {
        },            
        "stateSave": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ordering": true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
        		url: '{{route("admin.data")}}',
        		type: 'GET',
        		dataSrc: 'results'
        },
        
        /* */
        "lengthMenu": [[10, 15, 25, 50, 100,500,1000,10000], [10, 15, 25, 50, 100,500,1000,10000]],
        //"pageLength": 15,
        columns: [
			{data: 'regional', name:'regional'},
			{data: 'ap_submit',name:'ap_submit'},
			{data: 'ap_approve',name:'ap_approve'},
			{data: 'ap_mtd',name:'ap_mtd'},
			{data: 'ap_ytd',name:'ap_ytd'},
      {data: 'anggaran_mtd',name:'anggaran_mtd'},
      {data: 'anggaran_ytd',name:'anggaran_ytd'},
        ],
        "language": {
            "emptyTable":     "Tidak ada data untuk ditampilkan",
            "info":           "menampilkan START sampai END dari TOTAL data",
            "infoEmpty":      "menampilkan 0 sampai 0 dari 0 data",
            "infoFiltered":   "(cari data dari MAX data yang ada)",
            "lengthMenu":     "tampilkan MENU data",
            "loadingRecords": "menunggu...",
            "processing":     "memproses...",
            "search":         "Cari:",
            "zeroRecords":    "tidak ada data yang cocok",
            "paginate": {
                "first":      "awal",
                "last":       "akhir",
                "next":       "selanjutnya",
                "previous":   "sebelumnya"
            }
        },
        //"dom": 'lfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
	});

</script>
<script>
  var data = [{
    "id": "idData",
    "name": "Data",
    "data":[
        {name: 'TREG 1', y: {{$reg1}}, color: '#0abb87', sliced: true, 
            dataLabels: {
                format: '<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">TREG 1</span></span>',
                style: {
                    fontSize: "2rem",
                }
            }
        },
        {name: 'TREG 2', y: {{$reg2}}, color: '#ac31c7', sliced: true,
            dataLabels: {
                format: '<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">TREG 2</span></span>',
                style: {
                    fontSize: "2rem",
                }
            }
        },
        {name: 'TREG 3', y: {{$reg3}}, color: '#1728ed', sliced: true,
            dataLabels: {
                format: '<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">TREG 3</span></span>',
                style: {
                    fontSize: "2rem",
                }
            }
        },
        {name: 'TREG 4', y: {{$reg4}}, color: '#359bff', sliced: true,
            dataLabels: {
                format: '<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">TREG 4</span></span>',
                style: {
                    fontSize: "2rem",
                }
            }
        },
        {name: 'TREG 5', y: {{$reg5}}, color: '#6afda5', sliced: true,
            dataLabels: {
                format: '<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">TREG 5</span></span>',
                style: {
                    fontSize: "2rem",
                }
            }
        },
        {name: 'TREG 6', y: {{$reg6}}, color: '#84e645', sliced: true,
            dataLabels: {
                format: '<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">TREG 6</span></span>',
                style: {
                    fontSize: "2rem",
                }
            }
        },
        {name: 'TREG 7', y: {{$reg6}}, color: '#f39c12', sliced: true,
            dataLabels: {
                format: '<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">TREG 7</span></span>',
                style: {
                    fontSize: "2rem",
                }
            }
        },
      ]
}];

Highcharts.chart('container', {
  chart: {
    type: 'line'
  },
  title: {
    text: 'Monthly Penyerapan Dismantle AP'
  },
//   subtitle: {
//     text: 'Source: WorldClimate.com'
//   },
  xAxis: {
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  },
  yAxis: {
    title: {
      text: 'Rupiah (Rp)'
    }
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true
      },
      enableMouseTracking: false
    }
  },
  series: [{
    name: '2019',
    data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
  }, {
    name: '2020',
    data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
  }]
});
</script>
@endpush