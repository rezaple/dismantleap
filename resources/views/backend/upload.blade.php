@extends('backend.layouts.app')

@section('Upload List Pertanggungan AP | ' . __('strings.backend.dashboard.title'))

@push('css')
<style>
  .highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  .highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  .highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
  }

  .highcharts-data-table td,
  .highcharts-data-table th,
  .highcharts-data-table caption {
    padding: 0.5em;
  }

  .highcharts-data-table thead tr,
  .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
  }

  .highcharts-data-table tr:hover {
    background: #f1f7ff;
  }

  #circlebg {
    font-size: 1rem;
    line-height: 1rem;
    text-align: center;
    box-shadow: 0px 3px 10px rgba(0, 0, 0, .3);
    z-index: 2;
    width: 150px;
    height: 150px;
    border-radius: 100%;
    position: absolute;
    left: 37%;
    top: 30%;
    padding-top: 10%;
    font-weight: bold;
  }

  #circlebgValue{
    margin-top:10px;
  }

  #circlebg span {
    font-size: 1rem;
  }

  #testing > tbody > tr:last-child {
    background: #f8f8f8;
    font-weight: bold;
  }
</style>
@endpush
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <form class="form-horizontal" action="{{route('admin.data.store')}}" method="post" enctype="multipart/form-data">
        @csrf
           <!-- <form class="kt-form kt-form--label-left" id="formupdate" enctype="multipart/form-data"> -->
               <div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
                   <div class="kt-portlet__body">

                       <div class="form-group row">
                        {{ html()->label('List Pertangguan AP')->class('col-form-label col-lg-3 col-sm-12')->for('file') }}
                        <div class="col-lg-4 col-md-9 col-sm-12">
                           <input type="file" name="file" placeholder="{{__('validation.attributes.backend.submission.list')}}" accept=".xls,.xlsx,.csv,.txt">
                        </div><!--col-->
                       </div><!--form-group-->


                   </div>
               </div>
               <div class="modal-footer">
               {{ form_cancel(route('admin.submission.index'), __('buttons.general.cancel')) }}
               {{ form_submit(__('buttons.general.crud.create')) }}
               </div>
       </form>
</div>

@endsection

