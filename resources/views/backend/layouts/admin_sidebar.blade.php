<style>
	.kt-aside-menu .kt-menu__nav>.kt-menu__section {
		margin: 0px;
	}

	.kt-aside .kt-aside-menu {
		margin-top: 0px;
	}

	#profile-sidebar .kt-user-card .kt-user-card__name {
		font-size: 1rem;
	}

	#profile-sidebar .kt-user-card .kt-user-card__avatar img {
		border-radius: 100%;
	}

	/*#profile-sidebar .kt-user-card .kt-user-card__avatar img {
    width: 60px;
    height: 60px;
    border-radius: 4px;
}*/
	.kt-aside--minimize #profile-sidebar .kt-notification-item-padding-x {
		padding-left: 0.3rem !important;
		padding-right: 0.3rem !important;
	}

	.bgbackblue {
		object-fit: contain;
		background-image: linear-gradient(235deg, #2284ad, #0d3b4e);
	}

	.bgbackblue2 {
		object-fit: contain;
		background-image: linear-gradient(323deg, #2284ad, #0d3b4e);
	}

	.kt-aside-menu .kt-menu__nav {
		padding-top: 0px;
	}

	.nama {
		width: 102px;
		height: 19px;
		font-family: Raleway;
		font-size: 16px;
		font-weight: normal;
		font-stretch: normal;
		font-style: normal;
		line-height: 1.19;
		letter-spacing: normal;
		text-align: center;
		color: #ffffff;
	}

	.nik {
		width: 55px;
		height: 21px;
		font-family: Roboto;
		font-size: 16px;
		font-weight: bold;
		font-stretch: normal;
		font-style: normal;
		line-height: 1.31;
		letter-spacing: normal;
		text-align: left;
		color: #ffffff;
	}

	.menu {
		width: 70px;
		height: 16px;
		font-family: Raleway;
		font-size: 14px;
		font-weight: normal;
		font-stretch: normal;
		font-style: normal;
		line-height: 1.14;
		letter-spacing: normal;
		text-align: left;
		color: #ffffff;
	}

	.Gambar {
		width: 225.3px;
		height: auto;
		object-fit: contain;
	}
</style>

<button class="kt-aside-close" id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

	<!-- begin:: Aside -->
	<div class="kt-aside__brand kt-grid__item bgbackblue2 " id="kt_aside_brand">
		<div class="kt-aside__brand-logo">

		</div>
		<div class="kt-aside__brand-tools">
			<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
				<span>
					<!--begin::Svg Icon | path:/home/keenthemes/www/metronic/themes/metronic/theme/html/demo1/dist/../src/media/svg/icons/General/Other1.svg--><svg
						xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
						height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<circle fill="#000000" cx="12" cy="5" r="2" />
							<circle fill="#000000" cx="12" cy="12" r="2" />
							<circle fill="#000000" cx="12" cy="19" r="2" />
						</g>
					</svg>
					<!--end::Svg Icon--></span>

				<span>
					<!--begin::Svg Icon | path:/home/keenthemes/www/metronic/themes/metronic/theme/html/demo1/dist/../src/media/svg/icons/Layout/Layout-left-panel-2.svg--><svg
						xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
						height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24" />
							<path
								d="M10,4 L21,4 C21.5522847,4 22,4.44771525 22,5 L22,7 C22,7.55228475 21.5522847,8 21,8 L10,8 C9.44771525,8 9,7.55228475 9,7 L9,5 C9,4.44771525 9.44771525,4 10,4 Z M10,10 L21,10 C21.5522847,10 22,10.4477153 22,11 L22,13 C22,13.5522847 21.5522847,14 21,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L21,16 C21.5522847,16 22,16.4477153 22,17 L22,19 C22,19.5522847 21.5522847,20 21,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z"
								fill="#000000" />
							<rect fill="#000000" opacity="0.3" x="2" y="4" width="5" height="16" rx="1" />
						</g>
					</svg>
					<!--end::Svg Icon--></span>
			</button>
		</div>
	</div>

	<!-- end:: Aside -->

	<!-- begin:: Aside Menu -->
	<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid bgbackblue" id="kt_aside_menu_wrapper">
		<div id="kt_aside_menu" class="kt-aside-menu kt-scroll bgbackblue" data-ktmenu-vertical="1"
			data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
			<ul class="kt-menu__nav">
				<li class="kt-menu__section" id="profile-sidebar" style="padding:0px; height: auto;">
					<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x col-sm-12 bgbackblue"
						style="">
						<div class="kt-user-card__avatar">
							<img class="" alt="Pic" src="{{ $logged_in_user->picture }}" />
						</div>
						<div class="kt-user-card__name">
							<span>{{ $logged_in_user->name }}</span><br>
							<b>{{ $logged_in_user->username }}</b>
						</div>
					</div>
				</li>
				<li class="kt-menu__item {{ active_class_menu(Request::is('panel'))}}" aria-haspopup="true">
					<a href="{{ route('admin.dashboard') }}" class="kt-menu__link">
						<span class="kt-menu__link-icon">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon"> <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <rect id="bound" x="0" y="0" width="24" height="24" /> <path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" id="Combined-Shape" fill="#000000" /> <rect id="Rectangle-Copy-2" fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1" /> </g>
							</svg>
						</span>
						<span class="kt-menu__link-text">@lang('menus.backend.sidebar.dashboard')</span>
					</a>
				</li>
				@if($logged_in_user->hasPermissionTo('manage submissions'))
				<li class="kt-menu__item  kt-menu__item--submenu {{ active_class_submenu(Request::is('panel/submission*'))}}" aria-haspopup="true"
					data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
						class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon"><svg
								xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
								width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<rect id="bound" x="0" y="0" width="24" height="24" />
									<path
										d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
										id="Combined-Shape" fill="#000000" />
									<path
										d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
										id="Combined-Shape" fill="#000000" opacity="0.3" />
								</g>
							</svg></span><span class="kt-menu__link-text">Pengajuan</span><i
							class="kt-menu__ver-arrow la la-angle-right"></i></a>
					<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span
									class="kt-menu__link"><span class="kt-menu__link-text">Pengajuan</span></span></li>
							<li class="kt-menu__item {{ active_class_menu(Request::is('panel/submission'))}}" aria-haspopup="true"><a
									href="{{ route('admin.submission.index') }}" class="kt-menu__link "><i
										class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
										class="kt-menu__link-text">List Pengajuan</span></a></li>
							<li class="kt-menu__item {{ active_class_menu(Request::is('panel/submission/create'))}}" aria-haspopup="true"><a
									href="{{ route('admin.submission.create') }}" class="kt-menu__link "><i
										class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
										class="kt-menu__link-text">Pengajuan Baru</span></a></li>
						</ul>
					</div>
				</li>
				@endif


				@if($logged_in_user->hasPermissionTo('manage approval wdm'))
				<li class="kt-menu__item " aria-haspopup="true">
					<a class="kt-menu__link {{
						active_class(Request::is('panel/wdm/*'))
					}}" href="{{ route('admin.wdm.index') }}">

						<span class="kt-menu__link-icon">
						<i class="fas fa-file-alt"></i>
						</span>
						<span class="kt-menu__link-text">@lang('menus.backend.sidebar.submission-list')</span>
					</a>
				</li>
				@endif

				@if($logged_in_user->hasPermissionTo('manage approval gas'))
				<li class="kt-menu__item " aria-haspopup="true">
					<a class="kt-menu__link {{
						active_class(Request::is('panel/gas/*'))
					}}" href="{{ route('admin.gas.index') }}">

						<span class="kt-menu__link-icon">
						<i class="fas fa-file-alt"></i>
						</span>
						<span class="kt-menu__link-text">@lang('menus.backend.sidebar.submission-list')</span>
					</a>
				</li>
				@endif

				@if ($logged_in_user->isAdmin() || $logged_in_user->hasAnyPermission(['manage roles', 'manage users']))
				<li class="kt-menu__item  kt-menu__item--submenu {{ active_class_submenu(Request::is('panel/auth*'))}}" aria-haspopup="true"
					data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
						class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon"><svg
								xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
								width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<rect id="bound" x="0" y="0" width="24" height="24" />
									<path
										d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
										id="Combined-Shape" fill="#000000" />
									<path
										d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
										id="Combined-Shape" fill="#000000" opacity="0.3" />
								</g>
							</svg></span><span class="kt-menu__link-text">User Management</span><i
							class="kt-menu__ver-arrow la la-angle-right"></i></a>
					<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span
									class="kt-menu__link"><span class="kt-menu__link-text">User Management</span></span>
							</li>
							@if ($logged_in_user->isAdmin() || $logged_in_user->hasPermissionTo('manage roles'))
							<li class="kt-menu__item {{ active_class_menu(Request::is('panel/auth/role*'))}}" aria-haspopup="true"><a
									href="{{ route('admin.auth.role.index') }}" class="kt-menu__link "><i
										class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
										class="kt-menu__link-text">Roles</span></a></li>
							@endif
							@if ($logged_in_user->isAdmin() || $logged_in_user->hasPermissionTo('manage users'))
							<li class="kt-menu__item {{ active_class_menu(Request::is('panel/auth/user*'))}}" aria-haspopup="true"><a
									href="{{ route('admin.auth.user.index') }}" class="kt-menu__link "><i
										class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
										class="kt-menu__link-text">Data User</span></a></li>
							@endif
						</ul>
					</div>
				</li>

				@endif
				@if ($logged_in_user->isAdmin() || $logged_in_user->hasPermissionTo('view logs'))
				<li class="kt-menu__item {{ active_class_menu(Request::is('panel/logs'))}}" aria-haspopup="true"><a href="{{ route('admin.logs.index') }}"
						class="kt-menu__link "><span class="kt-menu__link-icon"><svg xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
								viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<rect id="bound" x="0" y="0" width="24" height="24" />
									<path
										d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z"
										id="Combined-Shape" fill="#000000" />
									<rect id="Rectangle-Copy-2" fill="#000000" opacity="0.3"
										transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) "
										x="16.3255682" y="2.94551858" width="3" height="18" rx="1" />
								</g>
							</svg></span><span class="kt-menu__link-text">Logs</span></a></li>
				@endif
			</ul>

			<img src="{{ asset ('assets') }}/images/newbg.png" class="Gambar">
		</div>
	</div>



	<!-- end:: Aside Menu -->
</div>