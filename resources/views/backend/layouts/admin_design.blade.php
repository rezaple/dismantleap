<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Dismantle AP | Dismantle AP</title>
		<meta name="description" content="Updates and statistics">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="{{ asset ('assets')}}/theme/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{ asset ('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{ asset ('assets')}}/theme/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{ asset ('assets')}}/theme/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{ asset ('assets')}}/theme/assets/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="{{ asset ('assets')}}/theme/assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="{{ asset ('assets')}}/theme/assets/demo/default/skins/brand/light.css" rel="stylesheet" type="text/css" />
		<link href="{{ asset ('assets')}}/theme/assets/demo/default/skins/aside/light.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{ asset ('assets')}}/favicon.ico" />

		<style>
			@media (min-width: 1025px) {
				.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
				    padding-top: 59px;
				}
				.kt-aside__brand .kt-aside__brand-tools svg g [fill] {
				    fill: #FFF;
				}
			}
			body {background: #f9f9f9;}
			.modal-lg, .modal-xl {
			    max-width: 80%!important;
			}
			.form-control[readonly] {
			    background-color: #f7f8fa!important;
			}
		</style>

			@yield('meta')
					
			{{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
			@stack('before-styles')
					
			<!-- Check if the language is set to RTL, so apply the RTL layouts -->
			<!-- Otherwise apply the normal LTR layouts -->
			{{ style(mix('css/backend.css')) }}
					
			@stack('after-styles')
	</head>

	<!-- end::Head -->
	
	<!-- begin::Body -->
	<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
	<!-- <body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize"> -->
    @include('backend.layouts.admin_header_mobile')

    <div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				<!-- begin:: Aside -->
                @include('backend.layouts.admin_sidebar')
				<!-- end:: Aside -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            @include('backend.layouts.admin_header')
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
			@yield('content')
            @include('backend.layouts.admin_footer')
       		
         
            <script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<link href="{{ asset('assets')}}/theme/assets/app/custom/invoices/invoice-v1.default.css" rel="stylesheet" type="text/css" />
		<script src="{{ asset('assets')}}/theme/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="{{ asset('assets')}}/theme/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="{{ asset('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
		<script src="{{ asset('assets')}}/theme/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
		<script src="{{ asset('assets')}}/theme/assets/vendors/custom/gmaps/gmaps.js" type="text/javascript"></script>

		<script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
		<script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
		<script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>
		<script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
		<script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js" type="text/javascript"></script>
		<script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
		<script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
		<script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>
		<script src="{{ asset('assets')}}/theme/assets/app/bundle/app.bundle.js" type="text/javascript"></script>
		@stack('before-scripts')
    	{!! script(mix('js/manifest.js')) !!}
    	{!! script(mix('js/vendor.js')) !!}
    	{!! script(mix('js/backend.js')) !!}
    	@stack('after-scripts')
    </body>

</html>