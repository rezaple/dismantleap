<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->

<head>
	<meta charset="utf-8" />
	<title>@yield('title', app_name())</title>
	<meta name="description" content="Updates and statistics">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!--begin::Fonts -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		const csrf = document.getElementsByName("csrf-token")[0].getAttribute('content');
		window.Laravel = {
			csrf
		}

	</script>
	@stack('before-styles')
	<!--end::Fonts -->

	<!--begin::Page Vendors Styles(used by this page) -->
	<link href="{{ asset ('assets')}}/theme/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{{ asset ('assets')}}/theme/assets/demo/default/base/style.bundle.css" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset ('assets')}}/theme/assets/demo/default/skins/header/base/light.css" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset ('assets')}}/theme/assets/demo/default/skins/header/menu/light.css" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset ('assets')}}/theme/assets/demo/default/skins/brand/light.css" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset ('assets')}}/theme/assets/demo/default/skins/aside/light.css" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('assets')}}/theme/assets/app/custom/invoices/invoice-v1.default.css" rel="stylesheet"
		type="text/css" />
	<link rel="shortcut icon" href="{{ asset ('assets')}}/favicon.ico" />

	<style>
		@media (min-width: 1025px) {
			.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
				padding-top: 59px;
			}

			.kt-aside__brand .kt-aside__brand-tools svg g [fill] {
				fill: #FFF;
			}
		}

		#notifications.has-notifications {
			color: #bf5329
		}

		body {
			background: #f9f9f9;
		}

		.modal-lg,
		.modal-xl {
			max-width: 80% !important;
		}

		.form-control[readonly] {
			background-color: #f7f8fa !important;
		}

		@media (max-width: 1024px) {
			.kt-header-mobile .kt-header-mobile__toolbar .kt-header-mobile__topbar-toggler i {
				color: rgba(47, 47, 47) !important;
			}
		}

		.unread{
            background-color:rgb(227, 240, 252) ; 
        }

        .list-notif{
            padding: 4px;
            border-bottom: 1px solid rgb(228, 228, 228);
            cursor: pointer;
        }

        .list-notif:hover{
            background-color:rgb(228, 228, 228); 
		}
		.title-notif{
			font-size: 16pt
		}
		.subtitle-notif{
			font-size: 8pt;
		}

		ul.timeline {
			z-index: 5;
		}
	</style>

	@stack('css')
	@stack('after-styles')
</head>
<!-- end::Head -->

<!-- begin::Body -->

<body
	class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
	<!-- <body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize"> -->
	@include('backend.layouts.admin_header_mobile')

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<!-- begin:: Aside -->
			@include('backend.layouts.admin_sidebar')
			<!-- end:: Aside -->
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
				@include('backend.layouts.admin_header')
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
					@yield('content')
				</div>
				@include('backend.layouts.admin_footer')
			</div>
		</div>
	</div>

	@stack('before-scripts')
	<script>
		var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		
	</script>
	<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.15/lodash.min.js"></script>
	
	<script src="{{ asset('assets')}}/theme/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="{{ asset('assets')}}/theme/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="{{ asset('assets')}}/theme/assets/app/bundle/app.bundle.js" type="text/javascript"></script>

	@stack('js')
	<script>
		var module = {};
	</script>
	<script src="{{asset('js/echo.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/pusher.min.js')}}" type="text/javascript"></script>
	@if(!auth()->guest())
	<script>
		window.Laravel.userId = {{ auth()->user()->id }};
	</script>
	@endif

	<script>

		window.Echo = new Echo({
			authEndpoint : '{{url("broadcasting/auth")}}',
			broadcaster: 'pusher',
			key: 'ac345b02f7d4608001f3',
			cluster: 'ap1',
			encrypted: true
		});

		const NOTIFICATION_TYPES = {
			wdm: 'App\\Notifications\\SubmitSubmissionToWdm',
			gas: ['App\\Notifications\\SubmitSubmissionToGas', 'App\\Notifications\\RevisionSubmissionToGas'],
			user_treg: ['App\\Notifications\\GasApproveSubmission', 'App\\Notifications\\GasRejectSubmission','App\\Notifications\\WdmRejectSubmission','App\\Notifications\\GasReturnSubmission' ,'App\\Notifications\\WdmApproveSubmission',]
		};

		function addNotifications(newNotifications, target) {
			var notifications = [];
			notifications = _.concat(notifications, newNotifications);

			// show only last 5 notifications
			notifications.slice(0, 5);
			showNotifications(notifications, target);
			countNotif()
		}
	
		async function countNotif()
		{
			let response = await fetch("{{route('admin.user.unread-notif')}}");
			let teks = await response.json();		
			let data = await teks;
			if(data.length>0){
				$('#labelnotify').text(data.length)	
			}
		}

		function showNotifications(notifications, target) {
			if(notifications.length) {
				var htmlElements = notifications.map(function (notification) {
					return makeNotification(notification);
				});
				
				$(target + 'Menu').html(htmlElements.join(''));
				$(target).addClass('has-notifications')
			} else {
				$(target + 'Menu').html('<li class="dropdown-header">No notifications</li>');
				$(target).removeClass('has-notifications');
			}
		}

		function makeNotification(notification) {
			var to = routeNotification(notification);
			var status = statusNotification(notification);
			var message = messageNotification(notification);
			var notificationText = makeNotificationText(notification);
			var readStatus = notification.read_at==null?'unread':'';
			return `<a href="javascript:void(0)" data-href="${to}" data-status="${readStatus}"  data-id="${notification.id}" class="btnNotif"><div class="d-flex align-items-center mb-8 list-notif ${readStatus}">
			<div class="d-flex flex-column ml-3">
				<div class="text-dark text-hover-primary mb-1 font-size-lg">
					<small>${status}</small>
					${message}
				</div>
				<small><span class="text-muted">${notification.created_at}</span></small>
			</div>
			</div></a>`;
		}

		function statusNotification(notification){
			const status = notification.data.status;
			if(status === 'reject'){
				return '<span class="badge badge-danger text-xs">Rejected</span>';	
			}else if(status === 'approved'){
				return '<span class="badge badge-success text-xs">Approved</span>';	
			}else if(status === 'submitted'){
				return '<span class="badge badge-info text-xs">Submitted</span>';	
			}else if(status === 'revision'){
				return '<span class="badge badge-info text-xs">Revision</span>';	
			}else if(status === 'return'){
				return '<span class="badge badge-warning text-xs">Returned</span>';	
			}else{
				return '<span class="badge badge-dark text-xs">No Info</span>';
			}
		}

		function messageNotification(notification) {
			let message = '';
			if(notification.type === NOTIFICATION_TYPES.wdm) {
				message = `<span class="font-weight-bold">${notification.data.name} TREG ${notification.data.regional}</span> ${notification.data.message}`;
			}else if (NOTIFICATION_TYPES.user_treg.includes(notification.type)){
				message = `<span class="font-weight-bold">${notification.data.message}</span>`;
			}else if (NOTIFICATION_TYPES.gas.includes(notification.type)){
				message = `<span class="font-weight-bold">${notification.data.name} TREG ${notification.data.regional}</span> ${notification.data.message}`;
			}
			return message;
		}

		function routeNotification(notification) {
			
			var to = '';
			if(notification.type === NOTIFICATION_TYPES.wdm) {
				to = "{{route('admin.wdm.show')}}/" + notification.data.uuid;
			}else if (NOTIFICATION_TYPES.user_treg.includes(notification.type)){
				to = "{{route('admin.submission.show')}}/" + notification.data.uuid;
			}else if (NOTIFICATION_TYPES.gas.includes(notification.type)){
				to = "{{route('admin.gas.show')}}/" + notification.data.uuid;
			}
			return to;
		}

		// get the notification text based on it's type
		function makeNotificationText(notification) {
			var text = '';
			if(notification.type === NOTIFICATION_TYPES.submitted) {
				const name = notification.data.name;
				text += '<strong>' + name + '</strong> submit a submission';
			}
			return text;
		}

		$(document).ready(function() {
			if(Laravel.userId) {
				$.get("{{route('admin.user.list-notif')}}", function (data) {
					addNotifications(data, "#notifications");
				});

				window.Echo.private(`App.User.${Laravel.userId}`)
					.notification((notification) => {
						addNotifications([notification], '#notifications');
					});
			}
		});

		$(document).on('click','#markReadAll', function(){

			fetch("{{route('admin.user.read-all-notif')}}")
			.then(res => res.json())
			.then(res =>{
				const status = res.status;
				if(status == 'Ok'){
					$.get("{{route('admin.user.list-notif')}}", function (data) {
						$('#notificationsMenu').empty();
						addNotifications(data, "#notifications");
					});
				}
			})
		})

		$(document).on('click', '.btnNotif', function(e){
			const status = $(this).data("status");
			const id = $(this).data("id");
			const url = $(this).data("href");
			if(status == 'unread'){
				fetch("{{route('admin.user.read-notif')}}/"+id).then(res=>res.json()).then(res=>{
					window.location = url
				})
			}
			window.location = url;
		});
	</script>

	@stack('after-scripts')
</body>

</html>