<style>
	.smalltextcard {
		font-size: 1rem;
	}

	#labelnotify {
		position: absolute;
		right: 9px;
		top: 5px;
		background: #e27777;
		color: #FFF;
		font-weight: bold;
		font-size: 7px;
		border-radius: 100%;
	}

	.kt-header .kt-header__topbar .kt-header__topbar-item .kt-header__topbar-icon svg g [fill] {
		fill: #969696 !important;
	}

	.kt-pulse.kt-pulse--brand .kt-pulse__ring {
		border-color: rgb(245, 184, 60, .8) !important;
	}

	.kt-header__topbar .kt-header__topbar-item.kt-header__topbar-item--user .kt-header__topbar-user img {
		border-radius: 2rem;
	}

	/*@media (min-width: 1025px) {
	.kt-header--fsixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
	    padding-top: 59px;
	}
}*/
</style>
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

	<!-- begin:: Header Menu -->
	<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
			class="la la-close"></i></button>
	<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
		<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
			<ul class="kt-menu__nav ">
				<li class="kt-menu__item kt-menu__item--active">
					<img alt="Logo" src="{{ asset ('assets')}}/images/big.png" style="max-height: 40px;" />
				</li>
			</ul>
		</div>
	</div>

	<!-- end:: Header Menu -->

	<!-- begin:: Header Topbar -->
	<div class="kt-header__topbar">

		<!--begin: Quick panel toggler -->
		<div class="kt-header__topbar-item kt-header__topbar-item--quick-panel dropdown show mr-2" id="shownotif"
			data-toggle="kt-tooltip" title="Notifications" data-placement="right">
			<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" id="kt_quick_panel_toggler_btn"
				data-toggle="dropdown">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
					height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<path
							d="M17,12 L18.5,12 C19.3284271,12 20,12.6715729 20,13.5 C20,14.3284271 19.3284271,15 18.5,15 L5.5,15 C4.67157288,15 4,14.3284271 4,13.5 C4,12.6715729 4.67157288,12 5.5,12 L7,12 L7.5582739,6.97553494 C7.80974924,4.71225688 9.72279394,3 12,3 C14.2772061,3 16.1902508,4.71225688 16.4417261,6.97553494 L17,12 Z"
							id="Combined-Shape" fill="#000000" />
						<rect id="Rectangle-23" fill="#000000" opacity="0.3" x="10" y="16" width="4" height="4"
							rx="2" />
					</g>
				</svg>
				<div id="labelnotify" class="badge badge-default"></div>
				<span class="kt-pulse__ring" id="alertnotif" style="display: none"></span>
			</span>
			<div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg"
				x-placement="bottom-end"
				style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-296px, 60px, 0px);">
				<form>
					<!--begin::Header-->
					<div class="d-flex flex-column pt-12 bgi-size-cover bgi-no-repeat rounded-top">
						<!--begin::Title-->
						<h4 class="d-flex flex-center rounded-top ml-4 mt-3 text-center">
							<span class="text-black">Notifications</span>
						</h4>
						<!--end::Title-->

						<!--begin::Scroll-->
						<div class="scroll pr-7 mr-n7" data-scroll="true" data-height="100" data-mobile-height="100" style="min-height: 150px; overflow: auto;" id="notificationsMenu">

							<!--begin::Item-->
							
							<!--end::Item-->

						</div>
						<!--end::Scroll-->

						<!--begin::Action-->
					<div class="d-flex flex-center pt-7">
						<a href="{{route('admin.user.notif')}}" class="btn btn-light-primary font-weight-bold text-center">See All</a>
					
						<a href="javascript:void(0)" id="markReadAll" class="btn btn-light-primary font-weight-bold text-center">Mark All as Read</a>
					</div>
						<!--end::Action-->

					</div>
					<!--end::Header-->

				</form>
			</div>

		</div>

		<!--end: Quick panel toggler -->

		<!--begin: User Bar -->
		<div class="kt-header__topbar-item kt-header__topbar-item--user">
			<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
				<div class="kt-header__topbar-user">
					<img src="{{ $logged_in_user->picture }}" class="img-avatar" alt="{{ $logged_in_user->username }}">
				</div>
			</div>
			<div
				class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

				<!--begin: Head -->

				<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x bgbackblue">
					<div class="kt-user-card__avatar">
						<img src="{{ $logged_in_user->picture }}" class="img-avatar"
							alt="{{ $logged_in_user->username }}">
					</div>
					<div class="kt-user-card__name">
						{{ $logged_in_user->name }}<br>
						<span class="smalltextcard">{{ $logged_in_user->username }}</span>
					</div>
					<div class="kt-user-card__badge">

					</div>
				</div>

				<!--end: Head -->

				<!--begin: Navigation -->
				<div class="kt-notification">
					<a href="{{route('admin.profile')}}" class="kt-notification__item">
						<div class="kt-notification__item-icon">
							<i class="flaticon2-calendar-3 kt-font-success"></i>
						</div>
						<div class="kt-notification__item-details">
							<div class="kt-notification__item-title kt-font-bold" href="{{ route('admin.profile') }}">
								My Profile
							</div>
							<div class="kt-notification__item-time">
								Account settings and more
							</div>
						</div>
					</a>
					<div class="kt-notification__custom">
						<a href="{{ route('frontend.auth.logout') }}" class="btn btn-label-danger btn-sm btn-bold"><i
								class="la la-power-off"></i> Sign Out</a>
					</div>
				</div>

				<!--end: Navigation -->
			</div>
		</div>

		<!--end: User Bar -->
	</div>

	<!-- end:: Header Topbar -->
</div>