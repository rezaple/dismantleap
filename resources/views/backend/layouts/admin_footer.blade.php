<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
	<div class="kt-footer__copyright">
		<?PHP echo date('Y'); ?>&nbsp;&copy;&nbsp;<a href="http://telkom.co.id/" target="_blank" class="kt-link">PT Telekomunikasi Indonesia Tbk.</a>
	</div>
</div>
