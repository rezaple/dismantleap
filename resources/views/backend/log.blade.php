@extends('backend.layouts.app')

@section('title', app_name() . ' | Data Log')

@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Data Logs
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>User</th>
                            <th>Activity</th>
                            <th>Subject</th>
                            <th>Data</th>
                            <th>Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($activities as $activity)
                        <?php 
                            if($activity->subject_type){
                                $arr = explode("\\",$activity->subject_type);
                                $data = end($arr);
                            }else{
                                $data= 'N\A';
                            }
                        ?>
                            <tr>
                                <td><img src="{{ $activity->causer->picture }}" class="img-avatar" width="40"></td>
                                <td>{{ $activity->causer->name}}</td>
                                <td>{{ \Str::title($activity->description) }}</td>
                                <td>{{ $data }}</td>
                                <td>{!! print_props($activity->properties) !!}</td>
                                <td>{{ $activity->created_at->diffForHumans() }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $activities->total() !!} activities total
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $activities->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
</div>
@endsection
