@extends('backend.layouts.app')

@section('title', __('labels.backend.submission.management').' | Show Detail')

@push('after-styles')
<link href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" rel="stylesheet" />
<script src="https://unpkg.com/imask"></script>
<style>
    ul.timeline {
        list-style-type: none;
        position: relative;
    }

    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }

    ul.timeline>li {
        margin: 20px 0;
        padding-left: 20px;
    }

    ul.timeline>li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }

    ul.timeline>li.border-default:before {
        border: 3px solid #cecfcf;
    }

    ul.timeline>li.border-success:before {
        border: 3px solid #22e85d;
    }

    ul.timeline>li.border-warning:before {
        border: 3px solid #e8b322;
    }

    ul.timeline>li.border-danger:before {
        border: 3px solid #e82254;
    }
</style>
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Submission {{$submission->name}}
                </h4>
            </div>
            <!--col-->

            <div class="col-sm-7 pull-right">
                <div class="btn-toolbar float-right" role="toolbar"
                    aria-label="@lang('labels.general.toolbar_btn_groups')">
                    <a href="{{ route('admin.submission.index') }}" class="btn btn-warning ml-1 text-gray"
                        data-toggle="tooltip" title="Back"><i class="fas fa-arrow-left"></i></a>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

        <div class="row mt-4">
            <div class="col-sm-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                            aria-controls="home" aria-selected="true">Detail</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                            aria-controls="profile" aria-selected="false">List Candidate Access Point</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                            aria-controls="contact" aria-selected="false">Document</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-sm-6">
                                <dl class="row">
                                    <dt class="col-sm-6">Name</dt>
                                    <dd class="col-sm-6">{{$submission->name}}</dd>
        
                                    <dt class="col-sm-6">Total Access Point</dt>
                                    <dd class="col-sm-6">{{$submission->total_access_point}}
                                    </dd>

                                    <dt class="col-sm-6">Invoice Value</dt>
                                    <dd class="col-sm-6">Rp. {{number_format($submission->payment_value,2,',', '.')}}
                                    </dd>
        
                                    <dt class="col-sm-6">Total Verified Access Point</dt>
                                    <dd class="col-sm-6">{{$submission->verified_access_point}}</dd>
        
        
                                    <dt class="col-sm-6">Total Unverified Access Point</dt>
                                    <dd class="col-sm-6">{{$submission->unverified_access_point}}</dd>
        
                                    <dt class="col-sm-6 text-truncate">User</dt>
                                    <dd class="col-sm-6">{{$submission->user->name}}</dd>
        
                                    <dt class="col-sm-6 text-truncate">Created Date</dt>
                                    <dd class="col-sm-6">{{$submission->created_at->format('d-m-Y')}}</dd>
        
                                    <dt class="col-sm-6 text-truncate">Status</dt>
                                    <dd class="col-sm-6">
                                        {!! print_status($submission->status) !!}
                                    </dd>
                                </dl>
                                <br>
                                <div class="row pl-3">
                                    @if($submission->status === 'draft')
                                    <form action="{{route('admin.submission.submit-to-wdm', $submission->uuid)}}" method="post">
                                        @csrf
                                        <button type="submit" class="btn btn-md btn-primary">Submit</button>
                                    </form>
                                    @elseif($submission->status === 'return-by-wdm')
                                    <form action="{{route('admin.submission.revision-to-wdm', $submission->uuid)}}" method="post">
                                        @csrf
                                        <button type="submit" class="btn btn-md btn-primary">Submit Revision</button>
                                    </form>
                                    @elseif($submission->status === 'approved-by-wdm')
                                    <form action="{{route('admin.submission.submit-to-gas', $submission->uuid)}}" method="post">
                                        @csrf
                                        <button type="submit" class="btn btn-md btn-primary">Submit to GAS</button>
                                    </form>
                                    @elseif($submission->status === 'return-by-gas')
                                    <form action="{{route('admin.submission.revision-to-gas', $submission->uuid)}}" method="post">
                                        @csrf
                                        <button type="submit" class="btn btn-md btn-primary">Submit Revision</button>
                                    </form>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>History</h4>
                                <ul class="timeline">
                                    @foreach($submission->submissionTracks as $track)
                                    <li class="border-{{$track->label}}">
                                    <a >{{$track->title}}</a>
                                        <a href="#" class="float-right">{{$track->created_at->format('d M Y')}}</a>
                                        
                                        <p><small>{!! print_status($track->status) !!}</small> <br>{{$track->message}}</p>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        @if($submission->status === 'return-by-wdm' || $submission->status === 'draft' || $submission->status === 'return-by-gas')
                        <form class="form-horizontal" action="{{route('admin.submission.store-revision', $submission->uuid)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <h4 class="card-title mb-0">
                                                <small class="text-muted">Upload List Dismantle AP</small>
                                            </h4>
                                        </div><!--col-->
                                    </div><!--row-->
                    
                                    <hr>
                    
                                    <div class="row">
                                        <div class="col">
                    
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                <input type="file" name="file" placeholder="{{__('validation.attributes.backend.submission.list')}}" accept=".xls,.xlsx,.csv,.txt">
                                                </div><!--col-->
                                                <div class="col-md-4">
                                                    <input type="submit" class="btn btn-primary" value="Upload">
                                                </div>
                                            </div><!--form-group-->
                    
                                        </div><!--col-->
                                    </div><!--row-->
                                </div><!--card-body-->
                            </div><!--card-->
                        </form>
                        @endif
                        <div class="responsive">
                            <div id="wrapper" class="table"></div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        @if(in_array($submission->status, ['draft', 'submitted-to-wdm', 'return-by-wdm', 'revision-to-wdm']))
                            Need Approval from WDM to access this section...
                        @elseif($submission->status === 'approved-by-wdm' || $submission->status === 'return-by-gas')
                            <div class="row">
                                <div class="col-sm-6">
                                    <form method="post" enctype="multipart/form-data" action="{{route('admin.submission.upload-document', $submission->uuid)}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="invoice_value">Invoice Value</label>
                                        <input type="text" class="form-control" id="invoice_value" name="invoice_value" value="{{$submission->payment_value}}" accept=".pdf">
                                        </div>

                                        <div class="form-group">
                                            <label for="nde_date">NDE Date</label>
                                        <input type="date" class="form-control-file" id="nde_date" name="nde_date" value="{{$submission->nde_date}}">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="spk">SPK File</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="spk" name="spk" accept=".pdf">
                                                <label class="custom-file-label" for="spk">Choose file</label>
                                            </div>
                                            
                                        </div>

                                        <div class="form-group">
                                            <label for="invoice">Invoice File</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="invoice" name="invoice" accept=".pdf">
                                                <label class="custom-file-label" for="invoice">Choose file</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="bast">BAST File</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="bast" name="bast" accept=".pdf">
                                                <label class="custom-file-label" for="bast">Choose file</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="pajak">Pajak File</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="pajak" name="pajak" accept=".pdf">
                                                <label class="custom-file-label" for="pajak">Choose file</label>
                                            </div>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-primary">Upload Document</button>
                                    </form>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card" style="width: 18rem;">
                                        <div class="card-header">
                                            Uploaded Document:
                                          </div>
                                        <ul class="list-group list-group-flush">
                                          
                                          @foreach ($submission->documents as $doc)
                                          <li class="list-group-item">
                                              <a target="_blank" href="{{asset('storage/'.$doc->filepath)}}">
                                                  <i class="fas fa-file"></i>
                                                  {{$doc->type}}
                                              </a>
                                            </li>
                                      @endforeach
                                        </ul>
                                      </div>
                                </div>
                            </div>
                        @else
                        <div class="card" style="width: 18rem;">
                            <div class="card-header">
                                Uploaded Document:
                              </div>
                            <ul class="list-group list-group-flush">
                              
                              @foreach ($submission->documents as $doc)
                              <li class="list-group-item">
                                  <a target="_blank" href="{{asset('storage/'.$doc->filepath)}}">
                                      <i class="fas fa-file"></i>
                                      {{$doc->type}}
                                  </a>
                                </li>
                          @endforeach
                            </ul>
                          </div>
                        @endif
                    </div>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->
    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@push('after-scripts')
<script src="https://unpkg.com/gridjs/dist/gridjs.production.min.js"></script>

<script>
    const grid = new gridjs.Grid({
            search: true,  
            columns: [
                'AP Name', 'Mac Address', 'Regional', 'Witel', 'Period', 'Type', 'Note',
                { 
                    name: 'Status',
                    formatter: (data, _) => {
                        const res = data ==='verified'?`<span class="badge badge-success badge-pill">Verified</span>`
                                                : `<span class="badge badge-danger badge-pill">Unverified</span>`;
                        return gridjs.html(res)
                    }
                }
            ],
            pagination: {
                limit: 10,
                server: {
                    url: (prev, page, limit) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return `${prev}${symbol}limit=${limit}&offset=${page * limit}`;
                    }
                }
            },
            search: {
                server: {
                    url: (prev, keyword) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return keyword ? `${prev}${symbol}q=${keyword}` : prev;
                    }
                }
            },
            server: {
                url: '{{route("admin.submission.cap", $submission->uuid)}}',
                then: data => data.results.map(ap => [ap.ap_name, ap.mac_address, ap.regional, ap.witel, ap.periode, ap.tipe, ap.note, ap.status]),
                total: data => data.count
            } 
        }).render(document.getElementById("wrapper"));

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        var currencyMask = IMask(
        document.getElementById('invoice_value'),
        {
            mask: 'num',
            blocks: {
            num: {
                // nested masks are available!
                mask: Number,
                thousandsSeparator: '.'
            }
            }
        });
</script>
@endpush
