@extends('backend.layouts.app')

@section('title', __('labels.backend.submission.management'))

@push('after-styles')
<link href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" rel="stylesheet" />
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row mb-4">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{__('strings.backend.submission.title')}}
                </h4>
            </div>
            <!--col-->

            <div class="col-sm-7 pull-right">
                <div class="btn-toolbar float-right" role="toolbar"
                    aria-label="@lang('labels.general.toolbar_btn_groups')">
                    <a href="{{ route('admin.submission.create') }}" class="btn btn-success ml-1" data-toggle="tooltip"
                        title="{{__('strings.backend.submission.create')}}"><i class="fas fa-plus-circle"></i></a>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->
        <div class="col-sm-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                        aria-controls="home" aria-selected="true">Draft</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                        aria-controls="profile" aria-selected="false">Process By WDM User</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                        aria-controls="contact" aria-selected="false">Process By GAS User</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="responsive">
                        <div id="wrapper-draft" class="table"></div>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="responsive">
                        <div id="wrapper-wdm" class="table"></div>
                    </div>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div id="wrapper-gas" class="table"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--card-body-->

<!--card-->
@endsection

@push('after-scripts')
<script src="https://unpkg.com/gridjs/dist/gridjs.production.min.js"></script>

<script>
    function generateDatatable(wrapperID, type){
        const grid = new gridjs.Grid({
            columns: [
                'Name', 'Total AP', 'Verified AP','Unverified AP','Created At', 
                {
                    name: 'Status',
                    formatter: (status, _) => {
                        let statusBadge = '';
                        if(status === 'draft'){
                            statusBadge = `<span class="badge badge-secondary badge-pill">Draft</span>`;
                        }else if(status === 'submitted-to-wdm' || status === 'submitted-to-gas'){
                            statusBadge = `<span class="badge badge-info badge-pill">Submitted</span>`;
                        }else if(status === 'approved-by-wdm' || status === 'approved-by-gas'){
                            statusBadge = `<span class="badge badge-success badge-pill">Approved</span>`;
                        }else if(status === 'return-by-wdm' || status === 'return-by-gas'){
                            statusBadge = `<span class="badge badge-warning badge-pill">Return</span>`;
                        }else if(status === 'reject-by-wdm' || status === 'reject-by-gas'){
                            statusBadge = `<span class="badge badge-danger badge-pill">Reject</span>`;
                        }

                        return gridjs.html(statusBadge)
                    }
                },
                { 
                    name: 'Action',
                    formatter: ({uuid, status}, _) => {
                        const action = (status==='draft' || status==='reject') ?` <form action="{{route('admin.submission.delete')}}/${uuid}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{route('admin.submission.show')}}/${uuid}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.view')" class="btn btn-info">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Delete" class="btn btn-danger" onclick="confirm('Are you sure you want to delete this data?') ? this.parentElement.submit() : ''">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </form>`: `<a href="{{route('admin.submission.show')}}/${uuid}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.view')" class="btn btn-info">
                                            <i class="fas fa-eye"></i>
                                        </a>`;
                        return gridjs.html(action)
                    }
                }
            ],
            pagination: {
                limit: 10,
                server: {
                    url: (prev, page, limit) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return `${prev}${symbol}limit=${limit}&offset=${page * limit}&type=${type}`;
                    }
                }
            },
            search: {
                server: {
                    url: (prev, keyword) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return keyword ? `${prev}${symbol}q=${keyword}` : prev;
                    }
                }
            },
            server: {
                url: '{{route("admin.submission.list")}}',
                then: data => data.results.map(subs => [
                    subs.name, subs.total_access_point,subs.verified_access_point, subs.unverified_access_point, subs.created_at, subs.status,{ uuid: subs.uuid, status: subs.status}
                ]),
                total: data => data.count
            } 
        }).render(document.getElementById(wrapperID));
    }

    generateDatatable('wrapper-draft', 'draft')

    generateDatatable('wrapper-wdm', 'wdm')

    generateDatatable('wrapper-gas', 'gas')
</script>
@endpush
