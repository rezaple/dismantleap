@extends('backend.layouts.app')

@section('title', 'Detail Pengajuan Invoice Dismantle AP')

@push('after-styles')
<link href="{{ asset ('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet"
    type="text/css" />

<script src="{{asset('js/imask.min.js')}}"></script>
<style>
    ul.timeline {
        list-style-type: none;
        position: relative;
    }

    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }

    ul.timeline>li {
        margin: 20px 0;
        padding-left: 20px;
    }

    ul.timeline>li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }

    ul.timeline>li.border-default:before {
        border: 3px solid #cecfcf;
    }

    ul.timeline>li.border-success:before {
        border: 3px solid #22e85d;
    }

    ul.timeline>li.border-warning:before {
        border: 3px solid #e8b322;
    }

    ul.timeline>li.border-danger:before {
        border: 3px solid #e82254;
    }

    .titlepagecust {
		font-size: 18px;
		line-height: 2;
		text-align: left;
		color: #b3b3b3;
	}
</style>
@endpush

@section('content')
<?php
function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);

	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
} ?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    @include('includes.partials.messages')
    <div>
        <h3 class="titlepagecust">Detail Pengajuan</h3>
    </div>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                    <ul class="nav nav-tabs  nav-tabs-line " role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#detail" role="tab">
                                <b><i class="flaticon flaticon-file-2"></i> Pengajuan Dismantle</b>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#list" role="tab">
                                <b><i class="flaticon flaticon2-checking"></i> List Candidate AP</b>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#history" role="tab">
                                <b><i class="flaticon flaticon-list-3"></i> Document</b>
                            </a>
                        </li>
                    </ul>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{ route('admin.submission.index') }}" class="btn btn-warning text-gray"
                            data-toggle="tooltip" title="Back"><i class="fas fa-arrow-left"></i></a>
                    </div>
                </div>
            </div>

        </div>

        <div class="kt-portlet__body">
            <div class="tab-content">
                <div class="tab-pane" id="detail" role="tabpanel">
                    <div class="row">
                        <div class="col-sm-6">
                            <dl class="row">
                                <dt class="col-sm-6">Nama</dt>
                                <dd class="col-sm-6">{{$submission->name}}</dd>
                                <dt class="col-sm-6">Total Access Point</dt>
                                <dd class="col-sm-6">{{$submission->total_access_point}}
                                </dd>
                                <dt class="col-sm-6">Nilai Invoice</dt>
                                <dd class="col-sm-6">Rp. {{number_format($submission->payment_value,2,',', '.')}}
                                </dd>
                                <dt class="col-sm-6">Total Verified Access Point</dt>
                                <dd class="col-sm-6">{{$submission->verified_access_point}}</dd>
                                <dt class="col-sm-6">Total Unverified Access Point</dt>
                                <dd class="col-sm-6">{{$submission->unverified_access_point}}</dd>

                                <dt class="col-sm-6 text-truncate">User</dt>
                                <dd class="col-sm-6">{{$submission->user->name}}</dd>

                                <dt class="col-sm-6 text-truncate">Tanggal NDE</dt>
                                <dd class="col-sm-6">{{$submission->nde_date?date('d-m-Y', strtotime($submission->nde_date)):' - '}}</dd>

                                <dt class="col-sm-6 text-truncate">Tanggal Pegajuan dibuat</dt>
                                <dd class="col-sm-6">{{$submission->created_at->format('d-m-Y')}}</dd>

                                <dt class="col-sm-6 text-truncate">Status</dt>
                                <dd class="col-sm-6">
                                    {!! print_status($submission->status) !!}
                                </dd>
                            </dl>
                            <br>
                            <div class="row pl-3">
                                @if($submission->status === 'draft')
                                <form action="{{route('admin.submission.submit-to-wdm', $submission->uuid)}}"
                                    method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-md btn-primary">Submit</button>
                                </form>
                                @elseif($submission->status === 'return-by-wdm')
                                <form action="{{route('admin.submission.revision-to-wdm', $submission->uuid)}}"
                                    method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-md btn-primary">Submit Revision</button>
                                </form>
                                @elseif($submission->status === 'approved-by-wdm')
                                <form action="{{route('admin.submission.submit-to-gas', $submission->uuid)}}"
                                    method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-md btn-primary">Submit to GAS</button>
                                </form>
                                @elseif($submission->status === 'return-by-gas')
                                <form action="{{route('admin.submission.revision-to-gas', $submission->uuid)}}"
                                    method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-md btn-primary">Submit Revision</button>
                                </form>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h4>History</h4>
                            <ul class="timeline">
                                @foreach($submission->submissionTracks as $track)
                                <li class="border-{{$track->label}}">
                                    <a>{{$track->title}}</a>
                                    <a href="#" class="float-right">{{$track->created_at->format('d M Y, H:i')}} </a>
                                    <p><small>{!! print_status($track->status) !!}</small> <br>{{$track->message}}</p>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="list" role="tabpanel">


                    <table class="table table-striped- table-bordered table-hover table-checkable" id="dt_tb">
                        <thead>
                            <tr>
                                <th></th>
                                <th>AP Name</th>
                                <th>Mac Address</th>
                                <th>Regional</th>
                                <th>Witel</th>
                                <th>Period</th>
                                <th>Type</th>
                                <th>Note</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th>AP Name</th>
                                <th>Mac Address</th>
                                <th>Regional</th>
                                <th>Witel</th>
                                <th>Period</th>
                                <th>Type</th>
                                <th>Note</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="tab-pane" id="history" role="tabpanel">
                    @if(in_array($submission->status, ['draft', 'submitted-to-wdm', 'return-by-wdm',
                    'revision-to-wdm']))
                    Need Approval from WDM to access this section...
                    @elseif($submission->status === 'approved-by-wdm' || $submission->status === 'return-by-gas')
                    <div class="row">
                        <div class="col-sm-6">
                            <form method="post" enctype="multipart/form-data"
                                action="{{route('admin.submission.upload-document', $submission->uuid)}}">
                                @csrf
                                <div class="form-group">
                                    <label for="invoice_value">Nilai Invoice</label>
                                    <input type="text" class="form-control" id="invoice_value" name="invoice_value"
                                        value="{{$submission->payment_value}}" accept=".pdf">
                                </div>

                                <div class="form-group">
                                    <label for="nde_date">Tanggal NDE</label>
                                    <input type="date" class="form-control-file" id="nde_date" name="nde_date" placeholder="dd/mm/yyyy" value="{{$submission->nde_date}}">
                                </div>

                                <div class="form-group">
                                    <label for="spk">File SPK</label>
                                    <p><small>Dokumen berisi NDE dan RAB</small></p>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="spk" name="spk" accept=".pdf">
                                        <label class="custom-file-label" for="spk">Pilih file</label>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="invoice">File Invoice</label>
                                    <p><small>Dokumen terdiri dari kwitansi dan surat permohonan pembayaran</small></p>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="invoice" name="invoice"
                                            accept=".pdf">
                                        <label class="custom-file-label" for="invoice">Pilih file</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="bast">File BAST</label>
                                    <p><small>Dokumen BAST yang sudah ditandatangan</small></p>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="bast" name="bast"
                                            accept=".pdf">
                                        <label class="custom-file-label" for="bast">Pilih file</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pajak">Faktur Pajak</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="pajak" name="pajak"
                                            accept=".pdf">
                                        <label class="custom-file-label" for="pajak">Pilih file</label>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Upload Dokumen</button>
                            </form>
                        </div>
                        <div class="col-sm-6">
                            <div class="card" style="width: 80%;">
                                <div class="card-header">
                                    <b>Dokumen yang telah diupload:</b>
                                </div>
                                <ul class="list-group list-group-flush">

                                    @foreach ($submission->documents as $doc)
                                    <li class="list-group-item">
                                        <a target="_blank" href="{{asset('storage/'.$doc->filepath)}}" class="btn btn-info btn-md btn-block">
                                            <i class="fas fa-file"></i>
                                            <b>{{strtoupper($doc->type)}}</b>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="card" style="width: 24rem;">
                        <div class="card-header">
                            <b>Dokumen yang telah diupload:</b>
                        </div>
                        <ul class="list-group list-group-flush">

                            @foreach ($submission->documents as $doc)
                            <li class="list-group-item">
                                <a target="_blank" href="{{asset('storage/'.$doc->filepath)}}" class="btn btn-info btn-md btn-block">
                                    <i class="fas fa-file"></i>
                                    <b>{{strtoupper($doc->type)}}</b>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                <!-- ISI TAB -->
                <div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('js')

    <script src="{{ asset('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.js"
        type="text/javascript"></script>
    <script>
    const inputInvoice = document.getElementById('invoice_value');

    const currentTabActive = localStorage.getItem('tabActive') ?? 'detail';

    $(".nav-link").removeClass('active');

    $(".tab-pane").removeClass('active');

    $(`.nav-link[href='#${currentTabActive}']`).addClass("active");

    $(`.tab-pane[id='${currentTabActive}']`).addClass("active");

    $(".nav-link").click(function (e) {
        e.preventDefault();
        const tabActive = $(this).attr('href');
        localStorage.setItem('tabActive', tabActive.replace('#',''))
    });

    if(inputInvoice){
        var currencyMask = IMask(
        document.getElementById('invoice_value'),
        {
            mask: 'num',
            blocks: {
            num: {
                // nested masks are available!
                mask: Number,
                thousandsSeparator: '.'
            }
            }
        });
    }

var t = $('#dt_tb').DataTable({
        initComplete : function() {
            var input = $('.dataTables_filter input').unbind();
            var self = this.api();
            var $searchButton = $('<button class="btn btn-sm btn-primary">')
                                .text('cari').click(function() {
                                            self.search(input.val()).draw();
                                });

            var $clearButton = $('<button class="btn btn-sm btn-danger">')
                                .text('reset').click(function() {
                                    input.val('');
                                    $searchButton.click(); 
                                });

            $(document).keypress(function (event) {
                if (event.which == 13) {
                    $searchButton.click();
                }
            });

            $('.dataTables_filter').append($searchButton, $clearButton);
        },            
        stateSave: false,
        responsive: true,
        processing: true,
        ordering: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
            url: '{{route("admin.submission.cap", $submission->uuid)}}',
            type: 'GET',
            dataSrc: 'results'
        },
        lengthMenu: [[10, 15, 25, 50, 100,500,1000,10000], [10, 15, 25, 50, 100,500,1000,10000]],
        columns: [
            {
                data: "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {data: 'ap_name', name:'ap_name'},
            {data: 'mac_address', name:'mac_address'},
			{data: 'regional',name:'regional'},
			{data: 'witel',name:'witel'},
			{data: 'periode',name:'periode'},
			{data: 'tipe',name:'tipe'},
			{data: 'note',name:'note',},
			{data: 'status',name:'status',}
        ],
        columnDefs: [
            {
                targets: -1,
                render: function(data, type, full, meta) {
                    if(data === 'unverified'){
                        return '<span class="kt-badge ' + 'kt-badge--danger' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
                    }

                    return '<span class="kt-badge ' + 'kt-badge--primary' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
                },
            },
		],
        language: {
            emptyTable:     "Tidak ada data untuk ditampilkan",
            info:           "",
            infoEmpty:      "",
            infoFiltered:   "(cari data dari MAX data yang ada)",
            lengthMenu:     "tampilkan MENU data",
            loadingRecords: "menunggu...",
            processing:     "memproses...",
            search:         "Cari:",
            zeroRecords:    "tidak ada data yang cocok",
            paginate: {
                first:      "awal",
                last:       "akhir",
                next:       "selanjutnya",
                previous:   "sebelumnya"
            }
        },
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });


    </script>
    @endpush