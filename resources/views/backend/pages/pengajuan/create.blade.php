@extends('backend.layouts.app')

@section('title', 'Create Submissions')

@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    @include('includes.partials.messages')
                    <form class="form-horizontal" action="{{route('admin.submission.store')}}" method="post" enctype="multipart/form-data">
                     @csrf
                        <!-- <form class="kt-form kt-form--label-left" id="formupdate" enctype="multipart/form-data"> -->
							<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
								<div class="kt-portlet__body">

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Dismantle Name *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class='input-group'>
                                            {{ html()->text('name')
                                            ->class('form-control')
                                            ->placeholder('')
                                            ->attribute('maxlength', 191)
                                            ->required()
                                            ->autofocus() }}
											</div>
                                        </div>
                                    </div>   
                                    <!-- formgroup -->

                                    <div class="form-group row">
                                     {{ html()->label(__('validation.attributes.backend.submission.file'))->class('col-form-label col-lg-3 col-sm-12')->for('file') }}
                                     <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="file" name="file" placeholder="{{__('validation.attributes.backend.submission.list')}}" accept=".xls,.xlsx,.csv,.txt">
                                     </div><!--col-->
                                    </div><!--form-group-->


                                </div>
							</div>
							<div class="modal-footer">
                            {{ form_cancel(route('admin.submission.index'), __('buttons.general.cancel')) }}
                            {{ form_submit(__('buttons.general.crud.create')) }}
								<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                                <!-- {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}
                                {{ form_submit(__('buttons.general.crud.create')) }} -->
                                <!-- <button type="submit" id="saveupdate" class="btn btn-primary">Save</button> -->
							</div>
                    </form>
</div>
@endsection