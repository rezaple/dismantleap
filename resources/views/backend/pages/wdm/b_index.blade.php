@extends('backend.layouts.app')

@section('title', __('labels.backend.submission.list'))

@push('after-styles')
<link href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" rel="stylesheet" />
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row mb-4">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{__('strings.backend.submission.title')}}
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->
        <div class="col-sm-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                        aria-controls="home" aria-selected="true">Need Action</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                        aria-controls="profile" aria-selected="false">Approved</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="reject-tab" data-toggle="tab" href="#reject" role="tab"
                        aria-controls="reject" aria-selected="false">Rejected</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="responsive">
                        <div id="wrapper-need" class="table"></div>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="responsive">
                        <div id="wrapper-approved" class="table"></div>
                    </div>
                </div>

                <div class="tab-pane fade" id="reject" role="tabpanel" aria-labelledby="reject-tab">
                    <div class="responsive">
                        <div id="wrapper-rejected" class="table"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@push('after-scripts')
<script src="https://unpkg.com/gridjs/dist/gridjs.production.min.js"></script>

<script>
    function generateDatatable(wrapperID, type){
        const grid = new gridjs.Grid({
            columns: [
                'Name', 'Total AP', 'User', 'Created At',
                {
                    name: 'Status',
                    formatter: (status, _) => {
                        let statusBadge = '';
                        if(status === 'draft'){
                            statusBadge = `<span class="badge badge-secondary badge-pill">Draft</span>`;
                        }else if(status === 'submitted-to-wdm'){
                            statusBadge = `<span class="badge badge-info badge-pill">Submitted</span>`;
                        }else if(status === 'approved-by-wdm'){
                            statusBadge = `<span class="badge badge-success badge-pill">Approved</span>`;
                        }else if(status === 'revision-to-wdm'){
                            statusBadge = `<span class="badge badge-info badge-pill">Revision</span>`;
                        }else if(status === 'return-by-wdm' ){
                            statusBadge = `<span class="badge badge-warning badge-pill">Return</span>`;
                        }else if(status === 'reject-by-wdm'){
                            statusBadge = `<span class="badge badge-danger badge-pill">Reject</span>`;
                        }

                        return gridjs.html(statusBadge)
                    }
                },
                { 
                    name: 'Action',
                    formatter: ({uuid, status}, _) => {
                        const action = `<a href="{{route('admin.wdm.show')}}/${uuid}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.view')" class="btn btn-info">
                                            <i class="fas fa-eye"></i>
                                        </a>`;
                        return gridjs.html(action)
                    }
                }
            ],
            pagination: {
                limit: 10,
                server: {
                    url: (prev, page, limit) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return `${prev}${symbol}limit=${limit}&offset=${page * limit}&type=${type}`;
                    }
                }
            },
            search: {
                server: {
                    url: (prev, keyword) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return keyword ? `${prev}${symbol}q=${keyword}` : prev;
                    }
                }
            },
            server: {
                url: '{{route("admin.wdm.list")}}',
                then: data => data.results.map(subs => [
                    subs.name, subs.verified_access_point, subs.username, subs.created_at, subs.status,{ uuid: subs.uuid, status: subs.status}
                ]),
                total: data => data.count
            } 
        }).render(document.getElementById(wrapperID));
    }

    generateDatatable('wrapper-need', 'need')

    generateDatatable('wrapper-approved', 'approved')

    generateDatatable('wrapper-rejected', 'rejected')

</script>
@endpush
