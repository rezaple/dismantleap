@extends('backend.layouts.app')

@section('title', __('labels.backend.submission.list'))

@push('after-styles')
<link href="{{asset('css/mermaid.min.css')}}" rel="stylesheet" />
<style>
    .titlepagecust {
		font-size: 18px;
		line-height: 2;
		text-align: left;
		color: #b3b3b3;
	}
</style>
@endpush

@section('content')
        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
            @include('includes.partials.messages')
            <div>
                <h3 class="titlepagecust">Daftar Pengajuan</h3>
            </div>
                 <div class="kt-portlet kt-portlet--mobile">
            		<div class="kt-portlet__head">
            			<div class="kt-portlet__head-label">
            				<h3 class="kt-portlet__head-title">
            					<ul class="nav nav-tabs  nav-tabs-line " role="tablist">
            						<li class="nav-item">
            							<a class="nav-link active" data-toggle="tab" href="#home" role="tab">
            								<b><i class="flaticon flaticon-file-2"></i> Need Approval</b>
            							</a>
            						</li>
            				
            							<li class="nav-item">
            								<a class="nav-link" data-toggle="tab" href="#profile" role="tab">
            									<b><i class="flaticon flaticon2-checking"></i>Approved</b>
            								</a>
            							</li>
            				
            						<li class="nav-item">
            							<a class="nav-link" data-toggle="tab" href="#reject" role="tab">
            								<i class="flaticon flaticon-list-3"></i> Rejected
            							</a>
            						</li>
					            </ul>
				            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
                        <div class="tab-content" id="myTabContent">
		            		<div class="tab-pane active" id="home" role="tabpanel">
                                <div class="responsive">
                                    <div id="wrapper-need" class="table"></div>
                                </div>
                            </div>

                            <div class="tab-pane" id="profile" role="tabpanel">
                                <div class="responsive">
                                    <div id="wrapper-approved" class="table"></div>
                                </div>
                            </div>

                            <div class="tab-pane" id="reject" role="tabpanel">
                                <div class="responsive">
                                     <div id="wrapper-rejected" class="table"></div>
                                </div>
                            </div>

                        <!-- ISI TAB -->
                        <div>
                    </div>
                 </div>
            </div>
        </div>
<!--card-->
@endsection

@push('after-scripts')
<script src="{{asset('js/gridjs.production.min.js')}}"></script>

<script>
    function generateDatatable(wrapperID, type){
        const grid = new gridjs.Grid({
            columns: [
                'Name', 'User','Regional', 'Total AP',  'Created At',
                {
                    name: 'Status',
                    formatter: (status, _) => {
                        let statusBadge = '';
                        if(status === 'draft'){
                            statusBadge = `<span class="badge badge-secondary badge-pill">Draft</span>`;
                        }else if(status === 'submitted-to-wdm'){
                            statusBadge = `<span class="badge badge-info badge-pill">Submitted</span>`;
                        }else if(status === 'approved-by-wdm'){
                            statusBadge = `<span class="badge badge-success badge-pill">Approved</span>`;
                        }else if(status === 'revision-to-wdm'){
                            statusBadge = `<span class="badge badge-info badge-pill">Revision</span>`;
                        }else if(status === 'return-by-wdm' ){
                            statusBadge = `<span class="badge badge-warning badge-pill">Return</span>`;
                        }else if(status === 'reject-by-wdm'){
                            statusBadge = `<span class="badge badge-danger badge-pill">Reject</span>`;
                        }

                        return gridjs.html(statusBadge)
                    }
                },
                { 
                    name: 'Action',
                    formatter: ({uuid, status}, _) => {
                        const action = `<a href="{{route('admin.wdm.show')}}/${uuid}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.view')" class="btn btn-info">
                                            <i class="fas fa-eye"></i>
                                        </a>`;
                        return gridjs.html(action)
                    }
                }
            ],
            pagination: {
                limit: 10,
                server: {
                    url: (prev, page, limit) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return `${prev}${symbol}limit=${limit}&offset=${page * limit}&type=${type}`;
                    }
                }
            },
            search: {
                server: {
                    url: (prev, keyword) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return keyword ? `${prev}${symbol}q=${keyword}` : prev;
                    }
                }
            },
            server: {
                url: '{{route("admin.wdm.list")}}',
                then: data => data.results.map(subs => [
                    subs.name, subs.username, subs.regional,subs.verified_access_point, subs.created_at, subs.status,{ uuid: subs.uuid, status: subs.status}
                ]),
                total: data => data.count
            } 
        }).render(document.getElementById(wrapperID));
    }

    generateDatatable('wrapper-need', 'need')

    generateDatatable('wrapper-approved', 'approved')

    generateDatatable('wrapper-rejected', 'rejected')

</script>
@endpush
