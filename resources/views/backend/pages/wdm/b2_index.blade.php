
@extends('backend.layouts.app')

@section('title', __('labels.backend.submission.list'))

@push('after-styles')
<link href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" rel="stylesheet" />
@endpush

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                 <div class="kt-portlet kt-portlet--mobile">
            		<div class="kt-portlet__head">
            			<div class="kt-portlet__head-label">
            				<h3 class="kt-portlet__head-title">
            					<ul class="nav nav-tabs  nav-tabs-line " role="tablist">
            						<li class="nav-item">
            							<a class="nav-link active" data-toggle="tab" href="#need" role="tab">
            						
            								<b><i class="flaticon flaticon-file-2"></i> Need Approval</b>
            							</a>
            						</li>
            				
            							<li class="nav-item">
            								<a class="nav-link" data-toggle="tab" href="#approved" role="tab">
            									<b><i class="flaticon flaticon2-checking"></i>Approved</b>
            								</a>
            							</li>
            				
            						<li class="nav-item">
            							<a class="nav-link" data-toggle="tab" href="#reject" role="tab">
            								<i class="flaticon flaticon-list-3"></i> Rejected
            							</a>
            						</li>
					            </ul>
				            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
		            	<div class="tab-content">
		            		<div class="tab-pane active" id="need" role="tabpanel">
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableneed">
					            	<thead>
					            		<tr>
					            			<th>Name</th>
					            			<th>Total AP</th>
					            			<th>User</th>
					            			<th>Created At</th>
					            			<th>Status</th>
                                            <th>Actions</th>
					            		</tr>
					            	</thead>
					            	<tfoot>
					            		<tr>
                                            <th>Name</th>
					            			<th>Total AP</th>
					            			<th>User</th>
					            			<th>Created At</th>
					            			<th>Status</th>
                                            <th>Actions</th>
					            		</tr>
					            	</tfoot>
					            </table>

                            </div>

                            <div class="tab-pane" id="approved" role="tabpanel">
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="tableapprove">
					            	<thead>
					            		<tr>
                                             <th>Name</th>
					            			<th>Total AP</th>
					            			<th>User</th>
					            			<th>Created At</th>
					            			<th>Status</th>
                                            <th>Actions</th>
					            		</tr>
					            	</thead>
					            	<tfoot>
					            		<tr>
                                            <th>Name</th>
					            			<th>Total AP</th>
					            			<th>User</th>
					            			<th>Created At</th>
					            			<th>Status</th>
                                            <th>Actions</th>
					            		</tr>
					            	</tfoot>
					            </table>
                            </div>

                            <div class="tab-pane" id="reject" role="tabpanel">
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="tablereject">
					            	<thead>
					            		<tr>
                                            <th>Name</th>
					            			<th>Total AP</th>
					            			<th>User</th>
					            			<th>Created At</th>
					            			<th>Status</th>
                                            <th>Actions</th>
					            		</tr>
					            	</thead>
					            	<tfoot>
					            		<tr>
                                            <th>Name</th>
					            			<th>Total AP</th>
					            			<th>User</th>
					            			<th>Created At</th>
					            			<th>Status</th>
                                            <th>Actions</th>
					            		</tr>
					            	</tfoot>
					            </table>
                            </div>

                        <!-- ISI TAB -->
                        <div>
                    </div>
                 </div>
            </div>
        </div>

@endsection

@push('js')

<script>
var dttb = $('#tableneed').DataTable({
        initComplete : function() {
        var input = $('.dataTables_filter input').unbind(),
            self = this.api(),
            $searchButton = $('<button class="btn btn-sm btn-primary">')
                    .text('cari')
                    .click(function() {
                        self.search(input.val()).draw();
                    }),
            $clearButton = $('<button class="btn btn-sm btn-danger">')
                    .text('reset')
                    .click(function() {
                        input.val('');
                        $searchButton.click(); 
                    });
        $(document).keypress(function (event) {
            if (event.which == 13) {
                $searchButton.click();
            }
        });
        $('.dataTables_filter').append($searchButton, $clearButton);
        },            
        "stateSave": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ordering": true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
        		url: '{{route("admin.wdm.list")}}',
        		type: 'GET',
        		dataSrc: 'results'
        },
        
        /* */
        "lengthMenu": [[10, 15, 25, 50, 100,500,1000,10000], [10, 15, 25, 50, 100,500,1000,10000]],
        //"pageLength": 15,
        columns: [
            {data: 'name', name:'name'},
            {data: 'verified_access_point', name:'verified_access_point'},
			{data: 'username',name:'username'},
			{data: 'created_at',name:'created_at'},
			{data: 'status',name:'status'},
			{data: 'uuid',name:'uuid'}
        ],
        columnDefs: [
				{
					targets: -2,
					render: function(data, type, full, meta) {
						if(data === 'draft'){
							return '<span class="kt-badge ' + 'kt-badge--danger' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
						}
							return '<span class="kt-badge ' + 'kt-badge--primary' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
					},
                },
                {
					targets: -1,
					render: function(data, type, full, meta) {
							return `<a href="{{route('admin.wdm.show')}}/${data}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                                <i data-toggle="tooltip" title="Detail" class="la la-edit"></i>
                            </a>`;
					},
				},
			],
        "language": {
            "emptyTable":     "Tidak ada data untuk ditampilkan",
            "info":           "menampilkan START sampai END dari TOTAL data",
            "infoEmpty":      "menampilkan 0 sampai 0 dari 0 data",
            "infoFiltered":   "(cari data dari MAX data yang ada)",
            "lengthMenu":     "tampilkan MENU data",
            "loadingRecords": "menunggu...",
            "processing":     "memproses...",
            "search":         "Cari:",
            "zeroRecords":    "tidak ada data yang cocok",
            "paginate": {
                "first":      "awal",
                "last":       "akhir",
                "next":       "selanjutnya",
                "previous":   "sebelumnya"
            }
        },
        //"dom": 'lfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>
<script>
    var dttb = $('#tableapprove').DataTable({
        initComplete : function() {
        var input = $('.dataTables_filter input').unbind(),
            self = this.api(),
            $searchButton = $('<button class="btn btn-sm btn-primary">')
                    .text('cari')
                    .click(function() {
                        self.search(input.val()).draw();
                    }),
            $clearButton = $('<button class="btn btn-sm btn-danger">')
                    .text('reset')
                    .click(function() {
                        input.val('');
                        $searchButton.click(); 
                    });
        $(document).keypress(function (event) {
            if (event.which == 13) {
                $searchButton.click();
            }
        });
        $('.dataTables_filter').append($searchButton, $clearButton);
        },            
        "stateSave": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ordering": true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
        		url: '{{route("admin.wdm.list")}}',
        		type: 'GET',
                dataSrc: 'results',
                data:{type:'name'},
        },
        
        /* */
        "lengthMenu": [[10, 15, 25, 50, 100,500,1000,10000], [10, 15, 25, 50, 100,500,1000,10000]],
        //"pageLength": 15,
        columns: [
            {data: 'name', name:'name'},
            {data: 'verified_access_point', name:'verified_access_point'},
			{data: 'username',name:'username'},
			{data: 'created_at',name:'created_at'},
			{data: 'status',name:'status'},
			{data: 'uuid',name:'uuid'}
        ],
        columnDefs: [
				{
					targets: -2,
					render: function(data, type, full, meta) {
						if(data === 'draft'){
							return '<span class="kt-badge ' + 'kt-badge--danger' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
						}
							return '<span class="kt-badge ' + 'kt-badge--primary' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
					},
                },
                {
					targets: -1,
					render: function(data, type, full, meta) {
							return `<a href="{{route('admin.wdm.show')}}/${data}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                                <i data-toggle="tooltip" title="Detail" class="la la-edit"></i>
                            </a>`;
					},
				},
			],
        "language": {
            "emptyTable":     "Tidak ada data untuk ditampilkan",
            "info":           "menampilkan START sampai END dari TOTAL data",
            "infoEmpty":      "menampilkan 0 sampai 0 dari 0 data",
            "infoFiltered":   "(cari data dari MAX data yang ada)",
            "lengthMenu":     "tampilkan MENU data",
            "loadingRecords": "menunggu...",
            "processing":     "memproses...",
            "search":         "Cari:",
            "zeroRecords":    "tidak ada data yang cocok",
            "paginate": {
                "first":      "awal",
                "last":       "akhir",
                "next":       "selanjutnya",
                "previous":   "sebelumnya"
            }
        },
        //"dom": 'lfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    var dttbreject = $('#tablereject').DataTable({
        initComplete : function() {
        var input = $('.dataTables_filter input').unbind(),
            self = this.api(),
            $searchButton = $('<button class="btn btn-sm btn-primary">')
                    .text('cari')
                    .click(function() {
                        self.search(input.val()).draw();
                    }),
            $clearButton = $('<button class="btn btn-sm btn-danger">')
                    .text('reset')
                    .click(function() {
                        input.val('');
                        $searchButton.click(); 
                    });
        $(document).keypress(function (event) {
            if (event.which == 13) {
                $searchButton.click();
            }
        });
        $('.dataTables_filter').append($searchButton, $clearButton);
        },            
        "stateSave": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ordering": true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
        		url: '{{route("admin.wdm.list")}}',
        		type: 'GET',
        		dataSrc: 'results'
        },
        
        /* */
        "lengthMenu": [[10, 15, 25, 50, 100,500,1000,10000], [10, 15, 25, 50, 100,500,1000,10000]],
        //"pageLength": 15,
        columns: [
            {data: 'name', name:'name'},
            {data: 'verified_access_point', name:'verified_access_point'},
			{data: 'username',name:'username'},
			{data: 'created_at',name:'created_at'},
			{data: 'status',name:'status'},
			{data: 'uuid',name:'uuid'}
        ],
        columnDefs: [
				{
					targets: -2,
					render: function(data, type, full, meta) {
						if(data === 'draft'){
							return '<span class="kt-badge ' + 'kt-badge--danger' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
						}
							return '<span class="kt-badge ' + 'kt-badge--primary' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
					},
                },
                {
					targets: -1,
					render: function(data, type, full, meta) {
							return `<a href="{{route('admin.wdm.show')}}/${data}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                                <i data-toggle="tooltip" title="Detail" class="la la-edit"></i>
                            </a>`;
					},
				},
			],
        "language": {
            "emptyTable":     "Tidak ada data untuk ditampilkan",
            "info":           "menampilkan START sampai END dari TOTAL data",
            "infoEmpty":      "menampilkan 0 sampai 0 dari 0 data",
            "infoFiltered":   "(cari data dari MAX data yang ada)",
            "lengthMenu":     "tampilkan MENU data",
            "loadingRecords": "menunggu...",
            "processing":     "memproses...",
            "search":         "Cari:",
            "zeroRecords":    "tidak ada data yang cocok",
            "paginate": {
                "first":      "awal",
                "last":       "akhir",
                "next":       "selanjutnya",
                "previous":   "sebelumnya"
            }
        },
        //"dom": 'lfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>

@endpush
