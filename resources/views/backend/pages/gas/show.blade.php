@extends('backend.layouts.app')

@section('title', 'Detail Pengajuan Invoice Dismantle AP')

@push('after-styles')

<link href="{{ asset ('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" rel="stylesheet" />
<style>
    ul.timeline {
        list-style-type: none;
        position: relative;
    }

    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }

    ul.timeline>li {
        margin: 20px 0;
        padding-left: 20px;
    }

    ul.timeline>li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }

    ul.timeline>li.border-default:before {
        border: 3px solid #cecfcf;
    }

    ul.timeline>li.border-success:before {
        border: 3px solid #22e85d;
    }

    ul.timeline>li.border-warning:before {
        border: 3px solid #e8b322;
    }

    ul.timeline>li.border-danger:before {
        border: 3px solid #e82254;
    }

    .titlepagecust {
		font-size: 18px;
		line-height: 2;
		text-align: left;
		color: #b3b3b3;
	}
</style>
@endpush

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    @include('includes.partials.messages')
    <div>
        <h3 class="titlepagecust">Detail Pengajuan</h3>
    </div>
                 <div class="kt-portlet kt-portlet--mobile">
            		<div class="kt-portlet__head">
            			<div class="kt-portlet__head-label">
            				<h3 class="kt-portlet__head-title">
            					<ul class="nav nav-tabs  nav-tabs-line " role="tablist">
            						<li class="nav-item">
            							<a class="nav-link" data-toggle="tab" href="#detail" role="tab">
            						
            								<b><i class="flaticon flaticon-file-2"></i> Pengajuan Dismantle</b>
            							</a>
            						</li>
            				
            							<li class="nav-item">
            								<a class="nav-link" data-toggle="tab" href="#jawaban" role="tab">
            									<b><i class="flaticon flaticon2-checking"></i> List Candidate AP</b>
            								</a>
            							</li>
            				
            						<li class="nav-item">
            							<a class="nav-link" data-toggle="tab" href="#history" role="tab">
            								<b><i class="flaticon flaticon-list-3"></i> Document</b>
            							</a>
            						</li>
					            </ul>
				            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <a href="{{ route('admin.gas.index') }}" class="btn btn-warning text-gray"
                                        data-toggle="tooltip" title="Back"><i class="fas fa-arrow-left"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
		            	<div class="tab-content">
		            		<div class="tab-pane" id="detail" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <dl class="row">
                                            <dt class="col-sm-6">Nama</dt>
                                            <dd class="col-sm-6">{{$submission->name}}</dd>

                                            <dt class="col-sm-6">Total Access Point</dt>
                                            <dd class="col-sm-6">{{$submission->verified_access_point}}</dd>

                                            <dt class="col-sm-6">Nilai Invoice</dt>
                                            <dd class="col-sm-6">Rp. {{number_format($submission->payment_value,2,',', '.')}}
                                            </dd>

                                            <dt class="col-sm-6 text-truncate">User</dt>
                                            <dd class="col-sm-6">{{$submission->user->name}}</dd>

                                            <dt class="col-sm-6 text-truncate">Regional</dt>
                                            <dd class="col-sm-6">TREG {{$submission->regional}}</dd>

                                            <dt class="col-sm-6 text-truncate">Tanggal NDE</dt>
                                            <dd class="col-sm-6">{{$submission->nde_date?date('d-m-Y', strtotime($submission->nde_date)):' - '}}</dd>

                                            <dt class="col-sm-6 text-truncate">Tanggal Pegajuan dibuat</dt>
                                            <dd class="col-sm-6">{{$submission->created_at->format('d-m-Y')}}</dd>

                                            <dt class="col-sm-6 text-truncate">Status</dt>
                                            <dd class="col-sm-6">
                                                {!! print_status($submission->status) !!}
                                            </dd>
                                        </dl>
                                        <br>
                                         <div class="row pl-3">
                                            @if($submission->status === 'submitted-to-gas' || $submission->status === 'revision-to-gas')
                                            <button type="button" class="btn btn-md btn-success" id="btnApprove"
                                                data-id="{{$submission->uuid}}">Approved</button>
                                            <button type="button" class="btn btn-md btn-warning ml-2" id="btnReturn"
                                                data-id="{{$submission->uuid}}">Return</button>
                                            <button type="button" class="btn btn-md btn-danger ml-2" id="btnReject"
                                                data-id="{{$submission->uuid}}">Reject</button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h4>History</h4>
                                        <ul class="timeline">
                                            @foreach($submission->submissionTracks as $track)
                                            <li class="border-{{$track->label}}">
                                                <a>{{$track->title}}</a>
                                                <a href="#" class="float-right">{{$track->created_at->format('d M Y, H:i')}}</a>

                                                <p><small>{!! print_status($track->status) !!}</small> <br>{{$track->message}}
                                                </p>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="jawaban" role="tabpanel">
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="dt_tb">
					            	<thead>
					            		<tr>
                                            <th></th>
					            			<th>AP Name</th>
					            			<th>Mac Address</th>
					            			<th>Regional</th>
					            			<th>Witel</th>
					            			<th>Period</th>
					            			<th>Type</th>
                                            <th>Note</th>
                                            <th>Status</th>
					            		</tr>
					            	</thead>
					            	<tfoot>
					            		<tr>
                                            <th></th>
                                            <th>AP Name</th>
					            			<th>Mac Address</th>
					            			<th>Regional</th>
					            			<th>Witel</th>
					            			<th>Period</th>
					            			<th>Type</th>
                                            <th>Note</th>
                                            <th>Status</th>
					            		</tr>
					            	</tfoot>
					            </table>
                            </div>

                            <div class="tab-pane" id="history" role="tabpanel">
                                <div class="card" style="width: 24rem;">
                                    <div class="card-header">
                                        <b>Dokumen yang telah diupload:</b>
                                    </div>
                                    <ul class="list-group list-group-flush">
    
                                        @foreach ($submission->documents as $doc)
                                        <li class="list-group-item">
                                            <a target="_blank" href="{{asset('storage/'.$doc->filepath)}}" class="btn btn-info btn-md btn-block">
                                                <i class="fas fa-file"></i>
                                                <b>{{strtoupper($doc->type)}}</b>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                             </div>
                            </div>

                        <!-- ISI TAB -->
                        <div>
                    </div>
                 </div>
            </div>
        </div>
       
@endsection

@push('after-scripts')
<script src="{{ asset('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="https://unpkg.com/gridjs/dist/gridjs.production.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>

    const currentTabActive = localStorage.getItem('tabActive') ?? 'detail';

    $(".nav-link").removeClass('active');

    $(".tab-pane").removeClass('active');

    $(`.nav-link[href='#${currentTabActive}']`).addClass("active");

    $(`.tab-pane[id='${currentTabActive}']`).addClass("active");

    $(".nav-link").click(function (e) {
        e.preventDefault();
        const tabActive = $(this).attr('href');
        localStorage.setItem('tabActive', tabActive.replace('#',''))
    });

    var dttb = $('#dt_tb').DataTable({
        initComplete : function() {
        var input = $('.dataTables_filter input').unbind(),
            self = this.api(),
            $searchButton = $('<button class="btn btn-sm btn-primary">')
                    .text('cari')
                    .click(function() {
                        self.search(input.val()).draw();
                    }),
            $clearButton = $('<button class="btn btn-sm btn-danger">')
                    .text('reset')
                    .click(function() {
                        input.val('');
                        $searchButton.click(); 
                    });
        $(document).keypress(function (event) {
            if (event.which == 13) {
                $searchButton.click();
            }
        });
        $('.dataTables_filter').append($searchButton, $clearButton);
        },            
        "stateSave": false,
        "responsive": true,
        "processing": true,
        "ordering": true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
        		url: '{{route("admin.gas.cap", $submission->uuid)}}',
        		type: 'GET',
        		dataSrc: 'results'
        },
        
        /* */
        "lengthMenu": [[10, 15, 25, 50, 100,500,1000,10000], [10, 15, 25, 50, 100,500,1000,10000]],
        //"pageLength": 15,
        columns: [
            {
                data: "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {data: 'ap_name', name:'ap_name'},
            {data: 'mac_address', name:'mac_address'},
			{data: 'regional',name:'regional'},
			{data: 'witel',name:'witel'},
			{data: 'periode',name:'periode'},
			{data: 'tipe',name:'tipe'},
			{data: 'note',name:'note',},
			{data: 'status',name:'status',}
        ],
        columnDefs: [
				{
					targets: -1,
					render: function(data, type, full, meta) {
						if(data === 'unverified'){
							return '<span class="kt-badge ' + 'kt-badge--danger' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
						}
							return '<span class="kt-badge ' + 'kt-badge--primary' + ' kt-badge--inline kt-badge--pill">' + data + '</span>';
					},
				},
			],
        "language": {
            "emptyTable":     "Tidak ada data untuk ditampilkan",
            "info":           "",
            "infoEmpty":      "",
            "infoFiltered":   "(cari data dari MAX data yang ada)",
            "lengthMenu":     "tampilkan MENU data",
            "loadingRecords": "menunggu...",
            "processing":     "memproses...",
            "search":         "Cari:",
            "zeroRecords":    "tidak ada data yang cocok",
            "paginate": {
                "first":      "awal",
                "last":       "akhir",
                "next":       "selanjutnya",
                "previous":   "sebelumnya"
            }
        },
        //"dom": 'lfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
	});
        
    const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content;

    function showAlert({title, icon, text, route}){
        const input = icon === 'success'?'':'textarea';
        Swal.fire({
            title: title,
            text: text,
            input: input,
            inputPlaceholder: 'Type your message here...',
            inputAttributes: {
                'aria-label': 'Type your message here'
            },
            icon: icon,
            showCancelButton: true,
            confirmButtonText: "Yes",
        }).then(({value:message, isConfirmed})=>{
            if(isConfirmed){
                fetch(route, {
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "X-Requested-With": "XMLHttpRequest",
                        "X-CSRF-Token": csrfToken
                    },
                    method: "post",
                    credentials: "same-origin",
                    body: JSON.stringify({
                        message
                    }),
                })
                .then(response => response.json())
                .then(data => {
                    Swal.fire({
                        icon: 'success',
                        title: data.message
                    }).then(_=>{
                        location.reload();
                    })
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'danger',
                        title: error.message
                    })
                });
            }
        });
    }

        const btnReject = document.getElementById('btnReject');
        if(typeof(btnReject) != 'undefined' && btnReject != null){
            document.getElementById('btnReject').addEventListener('click', function() {
                const data = {
                    title: "Reject this submission?",
                    icon: "error",
                    text: "this action can't be undo.",
                    route: "{{route('admin.gas.reject', $submission->uuid)}}"
                }
                showAlert(data)
            });
        }

        const btnApprove = document.getElementById('btnApprove');
        if(typeof(btnApprove) != 'undefined' && btnApprove != null){
            document.getElementById('btnApprove').addEventListener('click', function() {
                const data = {
                    title: "Approve this submission?",
                    icon: "success",
                    text: "",
                    route: "{{route('admin.gas.approve', $submission->uuid)}}"
                }
                showAlert(data)
            });
        }

        const btnReturn = document.getElementById('btnReturn');
        if(typeof(btnReturn) != 'undefined' && btnReturn != null){
            btnReturn.addEventListener('click', function() {
                const data = {
                    title: "Return this submission?",
                    icon: "warning",
                    text: "",
                    route: "{{route('admin.gas.return', $submission->uuid)}}"
                }
                showAlert(data)
            });
        }   
</script>
@endpush