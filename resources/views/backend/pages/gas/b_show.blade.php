@extends('backend.layouts.app')

@section('title', __('labels.backend.submission.management').' | Show Detail')

@push('after-styles')

<link href="{{ asset ('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" rel="stylesheet" />
<style>
    ul.timeline {
        list-style-type: none;
        position: relative;
    }

    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }

    ul.timeline>li {
        margin: 20px 0;
        padding-left: 20px;
    }

    ul.timeline>li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }

    ul.timeline>li.border-default:before {
        border: 3px solid #cecfcf;
    }

    ul.timeline>li.border-success:before {
        border: 3px solid #22e85d;
    }

    ul.timeline>li.border-warning:before {
        border: 3px solid #e8b322;
    }

    ul.timeline>li.border-danger:before {
        border: 3px solid #e82254;
    }
</style>
@endpush

@section('content')
@include('includes.partials.messages')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Submission {{$submission->name}}
                </h4>
            </div>
            <!--col-->

            <div class="col-sm-7 pull-right">
                <div class="btn-toolbar float-right" role="toolbar"
                    aria-label="@lang('labels.general.toolbar_btn_groups')">
                    <a href="{{ route('admin.gas.index') }}" class="btn btn-warning ml-1 text-gray"
                        data-toggle="tooltip" title="Back"><i class="fas fa-arrow-left"></i></a>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

        <div class="row mt-4">
            <div class="col-sm-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                            aria-controls="home" aria-selected="true">Detail</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                            aria-controls="profile" aria-selected="false">List Candidate Access Point</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                            aria-controls="contact" aria-selected="false">Document</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-sm-6">
                                <dl class="row">
                                    <dt class="col-sm-6">Name</dt>
                                    <dd class="col-sm-6">{{$submission->name}}</dd>

                                    <dt class="col-sm-6">Total Access Point</dt>
                                    <dd class="col-sm-6">{{$submission->verified_access_point}}</dd>

                                    <dt class="col-sm-6">Invoice Value</dt>
                                    <dd class="col-sm-6">Rp. {{number_format($submission->payment_value,2,',', '.')}}
                                    </dd>

                                    <dt class="col-sm-6 text-truncate">User</dt>
                                    <dd class="col-sm-6">{{$submission->user->name}}</dd>

                                    <dt class="col-sm-6 text-truncate">Regional</dt>
                                    <dd class="col-sm-6">TREG {{$submission->regional}}</dd>

                                    <dt class="col-sm-6 text-truncate">NDE Date</dt>
                                    <dd class="col-sm-6">{{$submission->nde_date?date('d-m-Y', strtotime($submission->nde_date)):' - '}}</dd>

                                    <dt class="col-sm-6 text-truncate">Created Date</dt>
                                    <dd class="col-sm-6">{{$submission->created_at->format('d-m-Y')}}</dd>

                                    <dt class="col-sm-6 text-truncate">Status</dt>
                                    <dd class="col-sm-6">
                                        {!! print_status($submission->status) !!}
                                    </dd>
                                </dl>
                                <br>
                                <div class="row pl-3">
                                    @if($submission->status === 'submitted-to-gas' || $submission->status === 'revision-to-gas')
                                    <button type="button" class="btn btn-md btn-success" id="btnApprove"
                                        data-id="{{$submission->uuid}}">Approved</button>
                                    <button type="button" class="btn btn-md btn-warning ml-2" id="btnReturn"
                                        data-id="{{$submission->uuid}}">Return</button>
                                    <button type="button" class="btn btn-md btn-danger ml-2" id="btnReject"
                                        data-id="{{$submission->uuid}}">Reject</button>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>History</h4>
                                <ul class="timeline">
                                    @foreach($submission->submissionTracks as $track)
                                    <li class="border-{{$track->label}}">
                                        <a>{{$track->title}}</a>
                                        <a href="#" class="float-right">{{$track->created_at->format('d M Y')}}</a>

                                        <p><small>{!! print_status($track->status) !!}</small> <br>{{$track->message}}
                                        </p>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="responsive">
                            <div id="wrapper" class="table"></div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="card" style="width: 18rem;">
                            <div class="card-header">
                                Uploaded Document:
                              </div>
                            <ul class="list-group list-group-flush">
                              
                              @foreach ($submission->documents as $doc)
                              <li class="list-group-item">
                                  <a target="_blank" href="{{asset('storage/'.$doc->filepath)}}">
                                      <i class="fas fa-file"></i>
                                      {{$doc->type}}
                                  </a>
                                </li>
                          @endforeach
                            </ul>
                          </div>
                    </div>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->
    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@push('after-scripts')
<script src="{{ asset('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="https://unpkg.com/gridjs/dist/gridjs.production.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>
    const grid = new gridjs.Grid({
            search: true,  
            columns: [
                'AP Name', 'Mac Address', 'Regional', 'Witel', 'Period', 'Type', 'Note',
                { 
                    name: 'Status',
                    formatter: (data, _) => {
                        const res = data ==='verified'?`<span class="badge badge-success badge-pill">Verified</span>`
                                                : `<span class="badge badge-danger badge-pill">Unverified</span>`;
                        return gridjs.html(res)
                    }
                }
            ],
            pagination: {
                limit: 10,
                server: {
                    url: (prev, page, limit) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return `${prev}${symbol}limit=${limit}&offset=${page * limit}`;
                    }
                }
            },
            search: {
                server: {
                    url: (prev, keyword) => {
                        var arr = prev.split('?');
                        const symbol = (prev.length > 1 && arr[1])? '&' : '?';
                        return keyword ? `${prev}${symbol}q=${keyword}` : prev;
                    }
                }
            },
            server: {
                url: '{{route("admin.gas.cap", $submission->uuid)}}',
                then: data => data.results.map(ap => [ap.ap_name, ap.mac_address, ap.regional, ap.witel, ap.periode, ap.tipe, ap.note, ap.status]),
                total: data => data.count
            } 
        }).render(document.getElementById("wrapper"));
        
        const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content;

        function showAlert({title, icon, text, route}){
            const input = icon === 'success'?'':'textarea';
            Swal.fire({
                title: title,
                text: text,
                input: input,
                inputPlaceholder: 'Type your message here...',
                inputAttributes: {
                    'aria-label': 'Type your message here'
                },
                icon: icon,
                showCancelButton: true,
                confirmButtonText: "Yes",
            }).then(({value:message, isConfirmed})=>{
                if(isConfirmed){
                    fetch(route, {
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json",
                            "X-Requested-With": "XMLHttpRequest",
                            "X-CSRF-Token": csrfToken
                        },
                        method: "post",
                        credentials: "same-origin",
                        body: JSON.stringify({
                            message
                        }),
                    })
                    .then(response => response.json())
                    .then(data => {
                        Swal.fire({
                            icon: 'success',
                            title: data.message
                        }).then(_=>{
                            location.reload();
                        })
                        
                    })
                    .catch((error) => {
                        Swal.fire({
                            icon: 'danger',
                            title: error.message
                        })
                    });
                }
            });
        }

        const btnReject = document.getElementById('btnReject');
        if(typeof(btnReject) != 'undefined' && btnReject != null){
            document.getElementById('btnReject').addEventListener('click', function() {
                const data = {
                    title: "Reject this submission?",
                    icon: "error",
                    text: "this action can't be undo.",
                    route: "{{route('admin.gas.reject', $submission->uuid)}}"
                }
                showAlert(data)
            });
        }

        const btnApprove = document.getElementById('btnApprove');
        if(typeof(btnApprove) != 'undefined' && btnApprove != null){
            document.getElementById('btnApprove').addEventListener('click', function() {
                const data = {
                    title: "Approve this submission?",
                    icon: "success",
                    text: "",
                    route: "{{route('admin.gas.approve', $submission->uuid)}}"
                }
                showAlert(data)
            });
        }

        const btnReturn = document.getElementById('btnReturn');
        if(typeof(btnReturn) != 'undefined' && btnReturn != null){
            btnReturn.addEventListener('click', function() {
                const data = {
                    title: "Return this submission?",
                    icon: "warning",
                    text: "",
                    route: "{{route('admin.gas.return', $submission->uuid)}}"
                }
                showAlert(data)
            });
        }   
</script>
@endpush