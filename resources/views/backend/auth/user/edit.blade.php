@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.edit'))

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    @include('includes.partials.messages')
{{ html()->modelForm($user, 'PATCH', route('admin.auth.user.update', $user->id))->class('form-horizontal')->open() }}
<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">@lang('labels.backend.access.users.management')
                <small class="text-muted">@lang('labels.backend.access.users.edit')</small><b id="namedata"></b></h3>
        </div>
    </div>
    <div class="kt-portlet__body">

        @if($user->ldap)
        <div class="form-group row">
            {{ html()->label('LDAP')->class('col-md-2 form-control-label')->for('ldap') }}

            <div class="col-md-10">
                <span class="badge badge-pill badge-success">TRUE</span>
            </div><!--col-->
        </div><!--form-group-->
        @endif

        <div class="form-group row">
            {{ html()->label(__('labels.backend.access.users.table.username'))->class('col-md-2 form-control-label')->for('username') }}
            <div class="col-md-10">
                {{ html()->text('username')
                    ->class('form-control')
                    ->placeholder(__('labels.backend.access.users.table.username'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
        {{ html()->label(__('validation.attributes.backend.access.users.name'))->class('col-md-2 form-control-label')->for('name') }}
            <div class="col-md-10">
                {{ html()->text('name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.name'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            <label class="col-form-label col-md-2">Regional</label>
            <div class="col-md-10">
                <div class='input-group'>
                    <select name="regional" id="regional" class="form-control">
                        @foreach ($regionals as $reg)
                            <option value="{{$reg->regional}}" {{$user->regional===$reg->regional?'selected':''}}>TREG {{$reg->regional}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.users.email'))->class('col-md-2 form-control-label')->for('email') }}

            <div class="col-md-10">
                {{ html()->email('email')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.email'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->
    

        <div class="form-group row">
            {{ html()->label('Phone Number')->class('col-md-2 form-control-label')->for('phone_number') }}

            <div class="col-md-10">
                {{ html()->text('phone_number')
                    ->class('form-control')
                    ->placeholder('Phone Number')
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->


        <div class="form-group row">
            <label class="col-form-label col-md-2 col-sm-12">Roles</label>
            <div class="col-md-10 col-sm-12">
                <div class='input-group'>
                    <select name="roles" id="roles" class="form-control">
                        @if($roles->count())
                        @foreach($roles as $role)
                            <option value="{{$role->id}}" {{in_array($role->id, $userRoles)?'selected':''}}>{{ucwords($role->name)}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
 
    </div>

<div class="modal-footer">
    {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}
    {{ form_submit(__('buttons.general.crud.update')) }}
</div>
</div>
{{ html()->closeModelForm() }}
</div>
@endsection
