<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.avatar')</th>
                <td><img src="{{ $user->picture }}" class="user-profile-image" /></td>
            </tr>

            @if($user->ldap)
            <tr>
                <th>LDAP</th>
                <td><span class="badge badge-success">Yes</span></td>
            </tr>
            @endif

            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.name')</th>
                <td>{{ $user->name }}</td>
            </tr>

            <tr>
                <th>Username</th>
                <td>{{ $user->username }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.email')</th>
                <td>{{ $user->email ??' - '}}</td>
            </tr>

            <tr>
                <th>Phone Number</th>
                <td>{{ $user->phone_number ??' - '}}</td>
            </tr>

            <tr>
                <th>Role</th>
                <td>{{ count($user->roles)>0?ucwords($user->roles[0]->name):' - ' }}</td>
            </tr>

            <tr>
                <th>Regional</th>
                <td>TREG {{ $user->regional }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.status')</th>
                <td>@include('backend.auth.user.includes.status', ['user' => $user])</td>
            </tr>

        </table>
    </div>
</div><!--table-responsive-->
