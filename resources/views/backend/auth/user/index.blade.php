@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@push('css')
<style>
.titlepagecust {
		font-size: 18px;
		line-height: 2;
		text-align: left;
		color: #b3b3b3;
	}
</style>
@endpush

@section('content')
<!-- begin:: Content -->

    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

	@include('includes.partials.messages')
        <div>
		    <h3 class="titlepagecust">Data User</h3>
	    </div>
						
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
                    <span>
				        <b>Perhatian!</b><br>
				        Silahkan klik button "<i>Tambah User Baru</i>" untuk melanjutkan pengisian Form yang tersedia.
				    </span>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							<div class="dropdown dropdown-inline">
							
							</div>
							&nbsp;
							<a href="{{ route('admin.auth.user.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
								<i class="la la-plus"></i>
								Tambah User Baru
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
					<thead>
						<tr>
							<th></th>
							<th>Name</th>
							<th>UserName</th>
							<th>E-Mail</th>
							<th>Roles</th>
							<th>LDAP</th>
							<th>Active</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
					@foreach($users as $user)
                            <tr>
                                <td><img src="{{ $user->picture }}" class="img-avatar" width="50"></td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->username }}</td>
                                <td>{{ $user->email??'N/A' }}</td>
                                <td>{{ $user->roles_label }}</td>
                                <td>{!! $user->ldap?'<span class="badge badge-pill badge-success">Yes</span>':'<span class="badge badge-pill badge-warning">No</span>' !!}</td>
                                <td>{!! $user->active?'<span class="badge badge-pill badge-info">Active</span>':'<span class="badge badge-pill badge-danger">Deactive</span>' !!}</td>
                                <td class="btn-td">@include('backend.auth.user.includes.actions', ['user' => $user])</td>
                            </tr>
                     @endforeach
					</tbody>
				</table>
				{{$users->links()}}
				<!--end: Datatable -->
			</div>
		</div>
	</div>


@endsection

@push('js')
		{{-- <script src="{{ asset ('assets')}}/theme/assets/app/custom/general/crud/datatables/basic/paginations.js" type="text/javascript"></script> --}}
@endpush