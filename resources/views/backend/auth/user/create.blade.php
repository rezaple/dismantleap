@extends('backend.layouts.app')

@section('title', 'Manage User | Dismantle AP')

@push('after-styles')
    <style>
        .red{
            color: rgb(221, 79, 79);
        }

        .green{
            color: rgb(48, 228, 168)
        }

        .dark-green{
            color: green;
        }
    </style>
@endpush

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    @include('includes.partials.messages')
    {{ html()->form('POST', route('admin.auth.user.store'))->class('kt-form kt-form--label-left')->open() }}
    <div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Tambah User <b id="namedata"></b></h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="form-group row">
                {{ html()->label('LDAP')->class('col-lg-3 col-sm-12 form-control-label')->for('ldap') }}
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <label class="switch switch-label switch-pill switch-primary">
                        {{ html()->checkbox('ldap', false)->class('switch-input') }}
                        <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                    </label>
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Username *</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class='input-group'>
                        {{ html()->text('username')
                                            ->class('form-control')
                                            ->placeholder('Username or NIK')
                                            ->attribute('maxlength', 191)
                                            ->attribute('autocomplete', 'user-username')
                                            ->required() }}
                    </div>
                    <div id="user-available"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Name *</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class='input-group'>
                        {{ html()->text('name')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.users.name'))
                                            ->attribute('maxlength', 191)
                                            ->autofocus() }}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Regional *</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class='input-group'>
                        <select name="regional" id="regional" class="form-control">
                            @foreach ($regionals as $reg)
                            <option value="{{$reg->regional}}">TREG {{$reg->regional}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Email Address *</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class='input-group'>
                        {{ html()->email('email')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.users.email'))
                                            ->attribute('maxlength', 191)
                                            ->required() }}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Phone Number*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class='input-group'>
                        {{ html()->text('phone_number')
                                            ->class('form-control')
                                            ->placeholder('Phone Number')
                                            ->attribute('maxlength', 191)
                                            ->required() }}
                    </div>

                    <div id="phone-available"></div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Password *</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class='input-group'>
                        {{ html()->password('password')
                                            ->class('form-control')
                                            ->attribute('autocomplete', 'user-password')
                                            ->placeholder(__('validation.attributes.backend.access.users.password'))
                                            ->required() }}
                                <span class="input-group-addon" >
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalGeneratePassword" id="generateGroup">
                                        <i class="fas fa-lock"></i>
                                      </button></span>             

                    </div>
                    <strong id="password-strength-text"></strong>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Password Confirmation *</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class='input-group'>
                        {{ html()->password('password_confirmation')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.users.password_confirmation'))
                                            ->required() }}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                {{ html()->label(__('validation.attributes.backend.access.users.active'))->class('col-lg-3 col-sm-6 col-xs-6 form-control-label')->for('active') }}

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                    <label class="switch switch-label switch-pill switch-primary">
                        {{ html()->checkbox('active', true)->class('switch-input') }}
                        <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                    </label>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Roles *</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class='input-group'>
                        <select name="roles" id="roles" class="form-control">
                            @if($roles->count())
                            @foreach($roles as $role)
                            <option value="{{$role->id}}">{{ucwords($role->name)}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </div>

    <div class="modal-footer">
        {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}
        <button class="btn btn-success btn-sm pull-right" type="submit" id="create-btn">Create</button>
    </div>
    </div>
    {{ html()->form()->close() }}
</div>

  <!-- Modal -->
  <div class="modal fade" id="modalGeneratePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Password Generator</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="text" name="generatePassword" id="passwordResult" class="form-control">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="generatePassword">Generate</button>
          <button type="button" class="btn btn-info" id="copyToClip">Copy to Clipboard</button>
        </div>
      </div>
    </div>
  </div>
  <?php 
    $sessionToken = session()->get('token');
    $accessToken = $sessionToken['access_token'];
  ?>

    <div id="otp-url" data-url="{{config('otp.url')}}"></div>
    <div id="base-url" data-url="{{url('')}}"></div>
    <div id="access-token" data-url="{{$accessToken }}"></div>
@endsection

@push('after-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js" integrity="sha512-quHCp3WbBNkwLfYUMd+KwBAgpVukJu5MncuQaWXgCrfgcxCJAq/fo+oqrRKOj+UKEmyMCG3tb8RB63W+EmrOBg==" crossorigin="anonymous"></script>
<script src="{{asset('js/form.js')}}"></script>
<script>
    const ldapValue = document.getElementById('ldap');
    const phone = document.getElementById('phone_number');
    const userName = document.getElementById("username");
    const name = document.getElementById('name');
    const email = document.getElementById('email');
    const createBtn = document.getElementById('create-btn');
    const userAvailableMessage = document.getElementById('user-available');
    const phoneAvailableMessage = document.getElementById('phone-available');

    const otpUrl = document.getElementById('otp-url').getAttribute('data-url');
    const baseUrl = document.getElementById('base-url').getAttribute('data-url');
    const accessToken = document.getElementById('access-token').getAttribute('data-url');

    userName.addEventListener("keyup", async function(){
        const isChecked = ldapValue.checked;
        const username = this.value.trim();
        const phoneNumber = "00";
        userAvailableMessage.style.color = ``;
        userAvailableMessage.innerText = ``;
        if(!isChecked && username.length > 4){
            const res = await axios.get(`{{route('admin.auth.user.check')}}?username=${username}&phone=${phoneNumber}`,
                        {
                            headers: {
                                'Authorization': `Bearer ${accessToken}`
                            }
                        })
                        .then(res => {
                            const statusMessage = res.data.status.message;
                            if(statusMessage === "User not found"){
                                userAvailableMessage.innerText = `Username available`;
                                userAvailableMessage.style.color = `green`;
                                createBtn.disabled = false;
                            }else{
                                userAvailableMessage.innerText = `Username has been taken`;
                                userAvailableMessage.style.color = `red`;
                                createBtn.disabled = true;
                            }
                        })
                        .catch(error => {
                            const statusMessage = error.response.data.status.message;
                            if(statusMessage === "User not found"){
                                userAvailableMessage.innerText = `Username available`;
                                userAvailableMessage.style.color = `green`;
                                createBtn.disabled = false;
                            }else{
                                userAvailableMessage.innerText = `Username has been taken`;
                                userAvailableMessage.style.color = `red`;
                                createBtn.disabled = true;
                            }
                        });
        }else if(isChecked && username.length > 5){
           await axios.get(`${baseUrl}/check-user-ldap/${username}`)
                        .then(async res => {
                            const user = res.data;
                            if(user.username === null){
                                userAvailableMessage.innerText = `User LDAP Not Found`;
                                userAvailableMessage.style.color = `red`;
                                createBtn.disabled = true;
                            }else{
                                await axios.get(`${baseUrl}/check-user-local/${username}`)
                                .then(res=>{
                                    const userLocal = res.data;
                                    if(userLocal.exist){
                                        console.log('adas')
                                        userAvailableMessage.innerText = `Username has been taken`;
                                        userAvailableMessage.style.color = `red`;
                                        phone.value = null;
                                        name.value = null;
                                        email.value = null;
                                        createBtn.disabled = true;
                                    }else{
                                        phone.value = user.phone;
                                        name.value = user.name;
                                        email.value = user.email;
                                        createBtn.disabled = false;
                                    }
                                }).catch(error => {
                                    userAvailableMessage.innerText = `Get User Error.`;
                                    userAvailableMessage.style.color = `red`;
                                });
                            }
                        })
                        .catch(error => {
                                userAvailableMessage.innerText = `Get User Error.`;
                                userAvailableMessage.style.color = `red`;
                        });
        }
    });

    phone.addEventListener("keyup", async function () {
        const isChecked = ldapValue.checked;
        const phoneNumber = this.value.trim();
        const username = "";
        phoneAvailableMessage.style.color = ``;
        phoneAvailableMessage.innerText = ``;

        if(!isChecked && phoneNumber.length > 8){
            const res = await axios.get(`{{route('admin.auth.user.check')}}?username=${username}&phone=${phoneNumber}`)
                        .then(res => {
                            const statusMessage = res.data.status.message;
                            if(statusMessage === "User not found"){
                                phoneAvailableMessage.innerText = `Phone Number available`;
                                phoneAvailableMessage.style.color = `green`;
                                createBtn.disabled = false;
                            }else{
                                phoneAvailableMessage.innerText = `Phone Number has been taken`;
                                phoneAvailableMessage.style.color = `red`;
                                createBtn.disabled = true;
                            }
                        })
                        .catch(error => {
                            const statusMessage = error.response.data.status.message;
                            if(statusMessage === "User not found"){
                                phoneAvailableMessage.innerText = `Phone Number available`;
                                phoneAvailableMessage.style.color = `green`;
                                createBtn.disabled = false;
                            }else{
                                phoneAvailableMessage.innerText = `Phone Number has been taken`;
                                phoneAvailableMessage.style.color = `red`;
                                createBtn.disabled = true;
                            }
                        });
        }
    });
</script>
@endpush