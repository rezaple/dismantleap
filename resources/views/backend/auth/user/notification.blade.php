@extends('backend.layouts.app')

@section('title','Notifikasi')

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
        <h4>Notifications</h4>
        <div class="scroll pr-7 mr-n7 kt-scroll" data-scroll="true"  id="notificationsMenu">
            @if(count($notifications) > 0)
                @foreach($notifications as $notif)
                @if(auth()->user()->hasRole('treg'))
                     <a href="javascript:void(0)" data-href="{{route('admin.submission.show',$notif->data['uuid'] )}}" data-status="{{$notif->read_at==null?'unread':''}}" data-id="{{$notif->id}}" class="btnNotif">
                    <div class="d-flex align-items-center mb-8 list-notif {{$notif->read_at==null?'unread':''}}">
                        <div class="d-flex flex-column font-weight-bold ml-2">
                            <span class="text-dark text-hover-primary mb-1 font-size-lg">
                                <small>{!! print_notif($notif->data['status']) !!}</small>
                                <span>{{$notif->data['message']}}</span></span>
                            <small><span class="text-muted">{{$notif->created_at}}</span></small>
                        </div>
                    </div>
                    </a>
                @elseif(auth()->user()->hasRole('wdm'))
                    <a href="javascript:void(0)" data-href="{{route('admin.wdm.show',$notif->data['uuid'] )}}" data-status="{{$notif->read_at==null?'unread':''}}" data-id="{{$notif->id}}" class="btnNotif">
                    <div class="d-flex align-items-center mb-8 list-notif {{$notif->read_at==null?'unread':''}}">
                        <div class="d-flex flex-column ml-2">
                            <span class="mb-1 font-size-lg text-dark">
                                <small>{!! print_notif($notif->data['status']) !!}</small>
                                <span class="font-weight-bold">{{$notif->data['name']}} TREG {{$notif->data['regional']}}</span> {{$notif->data['message']}}
                            </span>
                            <small><span class="text-muted">{{$notif->created_at}}</span></small>
                        </div>
                    </div>
                    </a>
                @elseif(auth()->user()->hasRole('gas'))
                <a href="javascript:void(0)" data-href="{{route('admin.gas.show',$notif->data['uuid'] )}}" data-status="{{$notif->read_at==null?'unread':''}}" data-id="{{$notif->id}}" class="btnNotif">
                    <div class="d-flex align-items-center mb-8 list-notif {{$notif->read_at==null?'unread':''}}">
                        <div class="d-flex flex-column ml-2">
                            <span class="mb-1 font-size-lg text-dark">
                                <small>{!! print_notif($notif->data['status']) !!}</small>
                                <span class="font-weight-bold">{{$notif->data['name']}} TREG {{$notif->data['regional']}}</span> {{$notif->data['message']}}
                            </span>
                            <small><span class="text-muted">{{$notif->created_at}}</span></small>
                        </div>
                    </div>
                    </a>
                @endif
                @endforeach
                {{$notifications->render()}}
            @else
                empty
            @endif
        </div>
    </div>
</div>
@endsection