@extends('backend.layouts.app')

@section('title', __('labels.backend.access.roles.management') . ' | ' . __('labels.backend.access.roles.create'))

@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    @include('includes.partials.messages')
{{ html()->modelForm($role, 'PATCH', route('admin.auth.role.update', $role))->class('kt-form kt-form--label-left')->open() }}                     
<!-- <form class="kt-form kt-form--label-left" id="formupdate" enctype="multipart/form-data"> -->
							<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
								<div class="kt-portlet__head">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">Ubah Role <b id="namedata"></b></h3>
									</div> 
								</div>
								<div class="kt-portlet__body">
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">NAMA ROLE *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class='input-group'>
                                            {{ html()->text('name')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.roles.name'))
                                            ->attribute('maxlength', 191)
                                            ->required() }}
											</div>
                                        </div>
                                    </div>
                                    @if($permissions->count())
                                    @foreach($permissions as $permission)
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 col-sm-12">Access</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                                            {{ html()->label(
                                                html()->checkbox('permissions[]', in_array($permission->name, $rolePermissions), $permission->name)
                                                        ->class('switch-input')
                                                        ->id('permission-'.$permission->id)
                                                    . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                                ->class('switch switch-label switch-pill switch-primary mr-2')
                                            ->for('permission-'.$permission->id) }}
                                        {{ html()->label(ucwords($permission->name))->for('permission-'.$permission->id) }}
											</span>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                
							<div class="modal-footer">
								
                                {{ form_cancel(route('admin.auth.role.index'), __('buttons.general.cancel')) }}
                                {{ form_submit(__('buttons.general.crud.update')) }}
							</div>
							</div>
                            {{ html()->closeModelForm() }}
</div>
@endsection
