@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@push('css')
<style>
.titlepagecust {
		font-size: 18px;
		line-height: 2;
		text-align: left;
		color: #b3b3b3;
	}
</style>
@endpush
@section('content')
<!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
		@include('includes.partials.messages')
        <div>
		    <h3 class="titlepagecust">Data Roles</h3>
	    </div>
						
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
                    <span>
				        <b>Perhatian!</b><br>
				        Silahkan klik button "<i>Tambah Roles Baru</i>" untuk melanjutkan pengisian Form yang tersedia.
				    </span>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							<div class="dropdown dropdown-inline">
								<!-- <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="la la-download"></i> Export
								</button> -->
								<div class="dropdown-menu dropdown-menu-right">
									<ul class="kt-nav">
										<li class="kt-nav__section kt-nav__section--first">
											<span class="kt-nav__section-text">Choose an option</span>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-print"></i>
												<span class="kt-nav__link-text">Print</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-copy"></i>
												<span class="kt-nav__link-text">Copy</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-file-excel-o"></i>
												<span class="kt-nav__link-text">Excel</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-file-text-o"></i>
												<span class="kt-nav__link-text">CSV</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link">
												<i class="kt-nav__link-icon la la-file-pdf-o"></i>
												<span class="kt-nav__link-text">PDF</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							&nbsp;
							<a href="{{ route('admin.auth.role.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
								<i class="la la-plus"></i>
								Tambah Roles Baru
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
					<thead>
						<tr>
							<th>Role</th>
							<th>Permissions</th>
							<th>Number Of Users</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						 @foreach($roles as $role)
                                <tr>
                                    <td>{{ ucwords($role->name) }}</td>
                                    <td>
                                        @if($role->id === 1)
                                            @lang('labels.general.all')
                                        @else
                                            @if($role->permissions->count())
                                                @foreach($role->permissions as $permission)
													<span class="badge badge-pill badge-info">{{ ucwords($permission->name) }}</span>
                                                @endforeach
                                            @else
                                                @lang('labels.general.none')
                                            @endif
                                        @endif
                                    </td>
                                    <td>{{ $role->users->count() }}</td>
                                    <!-- <td>
                                    <div class="btn-group" role="group">
										<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Dropdown
										</button>
										<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
											<a class="dropdown-item" href="{{ route('admin.auth.role.edit', $role->id) }}">Edit</a>
											<a class="dropdown-item" href="#">Hapus</a>
											<a class="dropdown-item" href="#">Dropdown link</a>
										</div>
                                    </div>
                                    </td> -->
                                    <td>@include('backend.auth.role.includes.actions', ['role' => $role])</td>
                                </tr>
                            @endforeach
					</tbody>
				</table>

				<!--end: Datatable -->
			</div>
		</div>
	</div>


@endsection

@push('js')

		<script src="{{ asset ('assets')}}/theme/assets/app/custom/general/crud/datatables/basic/paginations.js" type="text/javascript"></script>
@endpush