@if ($role->id !== 1)
    <form action="{{ route('admin.auth.role.destroy', $role) }}" method="post">
        @csrf
        @method('delete')
        <div class="btn-group btn-group-sm" role="group" aria-label="@lang('labels.backend.access.users.user_actions')">
            <a href="{{ route('admin.auth.role.edit', $role) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.edit')">
                <i class="fas fa-edit"></i>
            </a>
            <a href='javascript:void(0)' href="" data-toggle="tooltip" data-placement="top" title="Delete data" class="btn btn-danger"
            onclick="confirm('Are you sure you want to delete this data?') ? this.parentNode.parentNode.submit() : ''">
            <i class="fas fa-trash"></i>
            </a>
        </div>
    </form>
@else
    N/A
@endif
