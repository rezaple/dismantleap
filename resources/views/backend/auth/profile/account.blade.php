@extends('backend.layouts.app')

@section('title', 'Profile')

@push('after-styles')
    <style>
            .d-icon-left{
                flex:1;
                display: flex;
                justify-content: flex-end;
            }
            .d-icon-left a {
                text-decoration: none;
                color:white;
            }
            .title{
                flex:2;
                display: flex;
                justify-content: center;
            }
            .d-icon-rigth{
                flex:1;
            }

            .cotp__form-input{
                display: flex;
                align-items: center;
                flex-direction: column;
                margin-bottom: 15px;
            }
            .cotp__box--change{
                width: 100%;
                color: black;
                padding: 25px 50px;
                display: flex;
                align-items: center;
                flex-direction: column;
            }
            .cotp-change--title{
                margin-bottom: 30px;
            }
            .cotp__change-list{
                padding: 15px 5px;
                display: flex;
                justify-content: space-between;
                align-items: center;
                width: 450px;
                border-radius: 4px;
                box-shadow: 1px 1px 3px grey;
                margin-bottom:10px;
                cursor: pointer;
            }
            .cotp__change-list-left{
                font-size: 2em;
                color:gray;
                flex:1;
                display: flex;
                justify-content: center;
            }
            .cotp__change-list-right{
                font-size: 1.25em;
                flex:6;
            }
            .cotp__box--change-method{
                margin-bottom: 5px;
            }

            .cotp__change-list:hover{
                background-color: rgb(247, 247, 247); 
            }

            .otp-number-input{
                width:150px;
                height: 33px;
                margin: auto;
                margin-right: 2.3px;
                border-top: 0;
                border-left: 0;
                border-right: 0;
                border-bottom: 2px solid rgba(0,0,0,.2);
                padding: 0;
                color: rgba(0,0,0,.7);
                margin-bottom: 0;
                padding-bottom: 0;
                font-size: 28px;
                text-shadow: none;
                box-shadow: none;
                text-align: center;
                background-color: transparent;
                font-weight: 600;
                line-height: 0;
                border-radius: 0;
                border-image-width: 0;
            }
            .otp-number-input:hover{
                outline: none;
                border-bottom: 2px solid #037482;
            }

            .otp-number-input:focus{
                outline: none;
                border-bottom: 2px solid #037482;
            }

            .otp-number-input input:not([value=""]):not(:focus):invalid{
                border-bottom: 2px solid #037482;
            }

            .btn-success:disabled {
                color: rgb(105, 105, 105);
                background-color: #c5c5c5;
                border-color: #c5c5c5;
            }

            .cotp__box--input{
                width: 100%;
                color: black;
                padding: 25px 50px;
                display: flex;
                align-items: center;
                flex-direction: column;
            }
            .text-black{
                font-weight: 600;
            }
            .btn-verify{
                width: 250px;
                font-weight: 500;
            }
            .cotp__text__top, .cotp__text__bottom{
                text-align: center;
            }
            .cotp__text--resend{
                color: #025863;
                font-weight: 500;
                cursor: pointer;
            }

            .red{
                color: rgb(221, 79, 79);
            }

            .green{
                color: rgb(48, 228, 168)
            }

            .dark-green{
                color: green;
            }
		</style>
@endpush

@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row justify-content-center align-items-center">
        <div class="col col-sm-10 align-self-center">

            @include('includes.partials.messages')
            <div class="card">
                <div class="card-header">
                    <strong>
                        @lang('navs.frontend.user.account')
                    </strong>
                </div>
                <div class="card-body">
                    <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="#profile" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab">@lang('navs.frontend.user.profile')</a>
                            </li>

                            <li class="nav-item">
                                <a href="#edit" class="nav-link" aria-controls="edit" role="tab" data-toggle="tab">@lang('labels.frontend.user.profile.update_information')</a>
                            </li>

                            @if($logged_in_user->canChangePassword() && $logged_in_user->ldap === false)
                                <li class="nav-item">
                                    <a href="#tabPassword" class="nav-link" aria-controls="tabPassword" role="tab" data-toggle="tab">@lang('navs.frontend.user.change_password')</a>
                                </li>
                            @endif
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade show pt-3" id="profile" aria-labelledby="profile-tab">
                                @include('backend.auth.profile.tabs.profile')
                            </div><!--tab panel profile-->

                            <div role="tabpanel" class="tab-pane fade show pt-3" id="edit" aria-labelledby="edit-tab">
                                @include('backend.auth.profile.tabs.edit')
                            </div><!--tab panel profile-->

                            @if($logged_in_user->canChangePassword())
                                <div role="tabpanel" class="tab-pane fade show pt-3" id="tabPassword" aria-labelledby="password-tab">
                                    @include('backend.auth.profile.tabs.change-password')
                                </div><!--tab panel change password-->
                            @endif
                        </div><!--tab content-->
                    </div><!--tab panel-->
                </div><!--card body-->
            </div><!-- card -->
        </div><!-- col-xs-12 -->
    </div><!-- row -->
</div>

<div class="modal fade" id="modalGeneratePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Password Generator</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="text" name="generatePassword" id="passwordResult" class="form-control">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="generatePassword">Generate</button>
          <button type="button" class="btn btn-info" id="copyToClip">Copy to Clipboard</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('after-scripts')
<script src="{{asset('js/form.js')}}"></script>
<script>
    function activeTab() {
        const activeTab = localStorage.getItem('tabID') ?? '';

        document.querySelector(`.nav-link`).classList.remove("active");
        document.querySelector(`.tab-pane`).classList.remove("active");
        
        if(activeTab){
            document.querySelector(`[aria-controls='${activeTab}']`).classList.add("active");
            document.getElementById(activeTab).classList.add("active");
        }else{
            document.querySelector(`[aria-controls='profile']`).classList.add("active");
            document.getElementById('profile').classList.add("active");
        }
    }

    activeTab();

    document.addEventListener('click', function (event) {

        // If the clicked element doesn't have the right selector, bail
        if (!event.target.matches('.nav-link')) return;

        event.preventDefault();

        const tabID = event.target.getAttribute('href');

        storeTab(tabID)

    }, false);

    function storeTab(id) {
        const tab = id.replace('#','');
        localStorage.setItem('tabID', tab);
    }

    const formRequestOtp = document.getElementById("form-request-otp");
    if(formRequestOtp){
        formRequestOtp.addEventListener("click", function () {
            this.submit();
        });
    }

    const otpCodeInput = document.getElementById('otp-input');
    if(otpCodeInput){

        function toggleBtnVerify(){
            const btn = document.getElementById('confirm');
            const otp = document.getElementById('otp-input').value;

            if(otp.length > 5){
                btn.removeAttribute("disabled");
            }else{
                btn.setAttribute("disabled","true");
            }
        }

        otpCodeInput.focus();
        otpCodeInput.addEventListener('input', function(){
            toggleBtnVerify();
        })
    }

</script>
@endpush
