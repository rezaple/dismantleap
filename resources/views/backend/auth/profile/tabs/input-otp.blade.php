<div class="splash-box cotp__box cotp__box--input">
<form method="post" action="{{route('admin.otp.verify')}}" id="form-otp" class="cotp__form-input">
    @csrf
    <input name="question" value="1" type="hidden">

    <h2 class="cotp__text--title">Masukkan Kode Verifikasi</h2>

    <?php 
        $phone=str_repeat('*', strlen($logged_in_user->phone_number) - 3) . substr($logged_in_user->phone_number, -3);
    ?>
    <p class="cotp__text--dest text-black54 cotp--sms" style="display: block;">
        Kode verifikasi telah dikirimkan melalui 
        <br> Telegram ke 
        <span class="text-black">
            {{$phone}}
        </span>
        
    </p>


    <div class="control-group text-center" style="margin-bottom: 20px">
        <noscript>
            <div id="otp-fail" class="message message-warning text-center mt-10 mb-0 box-dashed">
                <h4>Perhatian!</h4>
                <p class="m-0 messages">Mohon aktifkan Javascript pada browser anda agar dapat mengirim OTP.</p>
            </div>
        </noscript>

        <div id="otp-input2" class="row-fluid cotp__container-input">
            <p class="text-black54">Kode Verifikasi</p>
            <div class="input-append mt-10">
                <div class="pincode-input-container">
                    <input id="otp-input" type="tel" maxlength="6" name="otp_code" autocomplete="off" pattern="[0-9]*" class="otp-number-input" required>
                </div>
            </div>
        </div>
    </div>
    <section class="row-fluid cta-fix">
        <input id="confirm" name="submit" type="submit" value="Verifikasi" class="btn btn-success btn-verify" disabled>
    </section>
</form>
</div>