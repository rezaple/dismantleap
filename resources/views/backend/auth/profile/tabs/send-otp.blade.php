<div class="cotp__box--change">
    <h2 class="cotp__text--method-change">Pilih Metode Verifikasi</h2>
    <p class="cotp-change--title">
        Pilih salah satu metode di bawah ini untuk mendapatkan kode verifikasi.
    </p>

    <form action="{{route('admin.otp.send')}}" method="post" id="form-request-otp">
        @csrf
        <div id="cotp__method--sms" class="cotp__change-list">
            <div class="cotp__change-list-left">
                <i class="fas fa-mobile-alt"></i>
            </div>
            <?php 
                $phone=str_repeat('*', strlen($logged_in_user->phone_number) - 3) . substr($logged_in_user->phone_number, -3);
            ?>
            <div class="cotp__change-list-right">
                Melalui 
                <b>Telegram </b>  ke 
                <span class="inline-block">{{$phone}}</span>
            </div>
            <span class="cotp__change-btn"> </span>
        </div>
    </form>
</div>