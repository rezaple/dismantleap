@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@push('css')

<link href="{{ asset ('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<style>
.titlepagecust {
		font-size: 18px;
		line-height: 2;
		text-align: left;
		color: #b3b3b3;
	}

</style>
@endpush
@section('content')
<!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
		@include('includes.partials.messages')
        <div>
		    <h3 class="titlepagecust">Daftar Pengajuan</h3>
	    </div>
						
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__body">
					<table class="table table-striped- table-bordered table-hover table-checkable" id="dt_tb">
						<thead>
							<tr>
								<th>NAME</th>
								<th>Total AP</th>
								<th>Verified AP</th>
								<th>Regional</th>
								<th>Created At</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>NAME</th>
								<th>Total AP</th>
								<th>Verified AP</th>
								<th>Regional</th>
								<th>Created At</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</tfoot>
					</table>


				<!--end: Datatable -->
			</div>
		</div>
	</div>


@endsection

@push('js')

<script src="{{ asset('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script>

var dttb = $('#dt_tb').DataTable({
        initComplete : function() {
        var input = $('.dataTables_filter input').unbind(),
            self = this.api(),
            $searchButton = $('<button class="btn btn-sm btn-primary">')
                    .text('cari')
                    .click(function() {
                        self.search(input.val()).draw();
                    }),
            $clearButton = $('<button class="btn btn-sm btn-danger">')
                    .text('reset')
                    .click(function() {
                        input.val('');
                        $searchButton.click(); 
                    });
        $(document).keypress(function (event) {
            if (event.which == 13) {
                $searchButton.click();
            }
        });
        $('.dataTables_filter').append($searchButton, $clearButton);
        },            
        "stateSave": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ordering": true,
        dom: 'Bfrtip',
        ajax: {
        		url: '{{route("admin.data-subs")}}',
        		type: 'GET',
        		dataSrc: 'results'
        },
        
        /* */
        "lengthMenu": [[10, 15, 25, 50, 100,500,1000,10000], [10, 15, 25, 50, 100,500,1000,10000]],
        //"pageLength": 15,
		columns: [
			{data: 'name', name:'name'},
			{data: 'total_access_point',name:'total_access_point'},
			{data: 'verified_access_point',name:'verified_access_point'},
			{data: 'regional',name:'regional'},
			{data: 'created_at',name:'created_at'},
			{data: 'status',
			  name:'status',},
			  {data: 'uuid',
			  name:'uuid',}
        	],
			columnDefs: [
				{
					targets: -2,
					render: function(data, type, full, meta) {
						if(data === 'draft'){
							return '<span class="badge badge-secondary">Draft</span>';
						}
						if(data === 'approved-by-gas'){
							return '<span class="badge badge-success">Approved By GAS</span>';
						}
						if(data === 'approved-by-wdm'){
							return '<span class="badge badge-success">Approved By WDM</span>';
						}

						if(data === 'submitted-to-gas'){
							return '<span class="badge badge-info">Submitted to GAS</span>';
						}

						if(data === 'submitted-to-wdm' ){
							return '<span class="badge badge-info">Submitted to WDM</span>';
						}

						if(data === 'return-by-gas'){
							return '<span class="badge badge-warning">Returned</span>';
						}

						if(data === 'revision-to-gas'){
							return '<span class="badge badge-primary">Revision</span>';
						}

						if(data === 'reject-by-wdm'){
							return '<span class="badge badge-danger">Reject By WDM</span>';
						}

						if(data === 'reject-by-gas'){
							return '<span class="badge badge-danger">Reject By GAS</span>';
						}

						return '<span class="badge" ' + 'style="background-color:#c0392b;color:#ffffff">No Info</span>';
					},
				},
				{
					targets: -1,
					render: function(data, type, full, meta) {
							return `
								<span class="dropdown">
            		                <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
            		                  <i class="la la-ellipsis-h"></i>
            		                </a>
									<div class="dropdown-menu dropdown-menu-right">
									<form action="{{route('admin.data-subs.delete')}}/${data}" method="post">
                                        @csrf
                                        @method('delete')
										<a class="dropdown-item" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" onclick="confirm('Are you sure you want to delete this data?') ? this.parentElement.submit() : ''"><i class="la la-leaf"></i>Delete</a>
									</form>
										</div>
            		            </span>
            		            `;
					},
				},
			],
        "language": {
            "emptyTable":     "Tidak ada data untuk ditampilkan",
            "info":           "",
            "infoEmpty":      "",
            "infoFiltered":   "(cari data dari MAX data yang ada)",
            "lengthMenu":     "tampilkan MENU data",
            "loadingRecords": "menunggu...",
            "processing":     "memproses...",
            "search":         "Cari:",
            "zeroRecords":    "tidak ada data yang cocok",
            "paginate": {
                "first":      "awal",
                "last":       "akhir",
                "next":       "selanjutnya",
                "previous":   "sebelumnya"
            }
        },
        //"dom": 'lfrtip',
	});
	
	
'use strict';
var KTDatatablesDataSourceAjaxClient = function() {

	var initTable1 = function() {
		var table = $('#kt_table_1');

		// begin first table
		table.DataTable({
			responsive: true,
			ajax: {
				url: '{{route("admin.data-subs")}}',
				type: 'GET',
				dataSrc: 'results'
			},
			columns: [
			{data: 'name', name:'name'},
			{data: 'total_access_point',name:'total_access_point'},
			{data: 'verified_access_point',name:'verified_access_point'},
			{data: 'regional',name:'regional'},
			{data: 'created_at',name:'created_at'},
			{data: 'status',
			  name:'status',},
			  {data: 'uuid',
			  name:'uuid',}
        	],
			columnDefs: [
				{
					targets: -2,
					render: function(data, type, full, meta) {
						if(data === 'draft'){
							return '<span class="badge" ' + 'style="background-color:#999999;color:#ffffff">' + data + '</span>';
						}
						if(data === 'approved-by-wdm' || data === 'approved-by-gas'){
							return '<span class="badge" ' + 'style="background-color:#1abc9c;color:#ffffff">' + data + '</span>';
						}
						return '<span class="badge" ' + 'style="background-color:#c0392b;color:#ffffff">' + data + '</span>';
					},
				},
				{
					targets: -1,
					render: function(data, type, full, meta) {
							return `
								<span class="dropdown">
            		                <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
            		                  <i class="la la-ellipsis-h"></i>
            		                </a>
									<div class="dropdown-menu dropdown-menu-right">
									<form action="{{route('admin.data-subs.delete')}}/${data}" method="post">
                                        @csrf
                                        @method('delete')
										<a class="dropdown-item" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" onclick="confirm('Are you sure you want to delete this data?') ? this.parentElement.submit() : ''"><i class="la la-leaf"></i>Delete</a>
									</form>
										</div>
            		            </span>
            		            `;
					},
				},
			],
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxClient.init();
});

</script>
@endpush