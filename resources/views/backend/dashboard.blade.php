@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@push('css')
<style>
  .highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  .highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  .highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
  }

  .highcharts-data-table td,
  .highcharts-data-table th,
  .highcharts-data-table caption {
    padding: 0.5em;
  }

  .highcharts-data-table thead tr,
  .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
  }

  .highcharts-data-table tr:hover {
    background: #f1f7ff;
  }

  #circlebg {
    font-size: 1rem;
    line-height: 1rem;
    text-align: center;
    box-shadow: 0px 3px 10px rgba(0, 0, 0, .3);
    z-index: 2;
    width: 150px;
    height: 150px;
    border-radius: 100%;
    position: absolute;
    left: 37%;
    top: 30%;
    padding-top: 10%;
    font-weight: bold;
  }

  #circlebgValue{
    margin-top:10px;
  }

  #circlebg span {
    font-size: 1rem;
  }

  #testing > tbody > tr:last-child {
    background: #f8f8f8;
    font-weight: bold;
  }
</style>
@endpush
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

  @if(auth()->user()->hasRole('treg'))
  <div class="row">
    <div class="col-lg-4">
      <div class="kt-portlet kt-portlet--solid-default kt-portlet--height-fluid kt-portlet--bordered">
        <div class="kt-portlet__body">
          <div class="row">
            <div class="col-sm-3 text-center">
              <img src="" class="img-responsive" style="max-width: 90%">
            </div>
            <div class="col-sm-9">

              <h4 class="valuebgdash">
                <a class="text" id="jmlDraft">{{ ucwords($logged_in_user->name) }}</a>
              </h4>
              <h6 class="valuebgdash">
                <a class="text" id="jmlDraft"> Regional {{ $logged_in_user->regional }}</a>
              </h6>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="kt-portlet kt-portlet--solid-default kt-portlet--height-fluid kt-portlet--bordered">
        <div class="kt-portlet__body">
          <div class="row">
            <div class="col-sm-3 text-center">
              <img src="{{asset ('assets')}}/images/submit.svg" class="img-responsive" style="max-width: 120%">
            </div>
            <div class="col-sm-9">
              <div class="text-warning">Pengajuan Dalam Proses </div>
              <h4 class="valuebgdash">
                <a class="text-warning" id="jmlDraft">{{$data->jumlah_on_progress}}</a>
              </h4>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="kt-portlet kt-portlet--solid-default kt-portlet--height-fluid kt-portlet--bordered">
        <div class="kt-portlet__body">
          <div class="row">
            <div class="col-sm-3 text-center">
              <img src="{{asset ('assets')}}/images/approve.svg" class="img-responsive" style="max-width: 120%">
            </div>
            <div class="col-sm-9">
              <div class="text-success">Pengajuan Disetujui</div>
              <h4 class="valuebgdash">
                <a class="text-success" id="jmlDraft">{{$data->jumlah_approve}}</a>
              </h4>
              <h4 class="valuebgdash">
                <a class="text-success" id="jmlDraft">Rp. {{number_format($data->nilai_approve,2,',', '.')}}</a>
              </h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @else

  <div class="row">
    <!--begin: Datatable -->
    <div class="col-md-12">
      <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body table-responsive">
          <div class="row mb-4">
            <div class="col-md-5 col-sm-12">
              <div class="form-group row">
                <label for="example-date-input" class="col-4 col-form-label">Pilih Tanggal</label>
                <div class="col-8">
                  <input class="form-control" type="date" value="<?php echo date('Y-m-d') ?>" id="example-date-input">
                  <small>Month To Date: <span id="mtd-label">{{date('01 M Y')}} - {{date('d M Y')}}</span> <br>
                    Year To Date: <span id="ytd-label">{{date('d M Y ',strtotime('first day of january this year'))}} -
                      {{date('d M Y')}}</span></small>
                </div>
              </div>
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="float-right">
                <div class="dt-buttons btn-group">           
                  <button class="btn btn-secondary buttons-excel buttons-html5" tabindex="0" aria-controls="testing" type="button" id="excelBtn" disabled><span>Excel</span></button>
                  <button class="btn btn-secondary buttons-pdf buttons-html5" tabindex="0" aria-controls="testing" type="button" data-pdf="" id="pdfBtn" disabled><span>PDF</span></button>
                </div>
              </div>
              
            </div>
          </div>
          <table class="table table-striped- table-bordered table-hover table-checkable" id="testing">
            <thead>
              <tr>
                <th rowspan="2" style="text-align:center;background-color: #2284ad;color: #ffffff;">REGIONAL</th>
                <th colspan="3" style="background-color: #f8f8f8;text-align:center; border-right:1px solid #EBEBEB"><b>PENGAJUAN</b> </th>
                <th colspan="2" style="background-color: #f8f8f8;text-align:center;border-right:1px solid #EBEBEB"> <b> JUMLAH AP</b></th>
                <th colspan="2" style="background-color: #f8f8f8;text-align:center;border-right:1px solid #EBEBEB"> <b>PENYERAPAN ANGGARAN</b> </th>
                <th rowspan="2" style="background-color: #f8f8f8;text-align:center"> <b>%</b> </th>
              </tr>
              <tr>
                <td style="text-align:center">PROGRESS</td>
                <td style="text-align:center">APPROVE<br>MTD</td>
                <td style="text-align:center">APPROVE<br>YTD</td>
                <td style="text-align:center">MTD</td>
                <td style="text-align:center">YTD</td>
                <td style="text-align:center">MTD</td>
                <td style="text-align:center;border-right:1px solid #EBEBEB">YTD</td>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @endif
  <div class="row">
    <!--end: Datatable -->
    <div class="col-md-6">
      <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
          <div id="circlebg"><div id="circlebgValue">{{$tot}}</div><span><br>Total</span></div>
          <div id="chartdonut" style="width: 450px; height: 400px; margin: -1rem auto;"></div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="kt-portlet kt-portlet--mobile">
        <figure class="highcharts-figure">
          <div id="container"></div>
        </figure>
      </div>
    </div>
  </div>

</div>

@endsection

@push('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.28/dayjs.min.js"></script>
<script src="{{ asset('assets')}}/theme/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript">
</script>
<script>
  $.getJSON('{{route("admin.trendline")}}', function(json) {

      const monthLabel = Object.keys(json).map(val=>val.toUpperCase())

      var chart_base = {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Penyerapan Anggaran Bulanan'
        },
        subtitle: 'Dismantle AP',
        xAxis: {
            categories: monthLabel, 
            crosshair: true, 
            tickInterval: 1
        },
        yAxis: {
            labels: {
                formatter: function () {
                    var ret,
                        numericSymbols = ['Rb', 'Jt', 'Mi', 'Tr', 'P', 'E'],
                        i = 6;
                    
                    if(this.value >=1000) {
                        while (i-- && ret === undefined) {
                            multi = Math.pow(1000, i + 1);
                            if (this.value >= multi && numericSymbols[i] !== null) {
                                ret = (this.value / multi) + numericSymbols[i];
                            }

                        }
                    }
                    return (ret ? ret : this.value);
                }
            }
        },
        series: [{
              name: "{{date('Y')}}",
              data: Object.values(json)
        }]
      };

      Highcharts.setOptions({
          lang: {
              thousandsSep: '.'
          }
      });

      chart = new Highcharts.Chart('container',chart_base);
  });

  function generateDonutChart(data, titleText) {
      Highcharts.chart('chartdonut', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          height: 350,
          style: {
              fontFamily: 'Poppins'
          },
          plotShadow: false,
      },
      credits: {
          enabled: false
      },
      plotOptions: {
          pie: {
              center: ['50%', '50%'],
              innerSize: '100%',
              borderWidth: 20,
              borderColor: null,
              slicedOffset: 0,
              dataLabels: {
                  connectorWidth: 3,
                  color: '#555'
              }
          }
      },
      title: {
          text: titleText,
          verticalAlign: 'top'
      },
      series: data,
    });
  }
</script>

@if(!auth()->user()->hasRole('treg'))
<script>

  function exportToPdf(data, tanggal){
    const dataBody = [[
                          {text: 'REGIONAL', style: 'tableHeader', rowSpan: 2},
                          {text: 'PENGAJUAN', style: 'tableHeader', colSpan: 3, alignment: 'center'}, 
                          {},
                          {}, 
                          {text: 'JUMLAH AP', style: 'tableHeader', colSpan: 2, alignment: 'center'}, 
                          {}, 
                          {text: 'PENYERAPAN ANGGARAN', style: 'tableHeader', colSpan: 2, alignment: 'center'},
                          {},  
                          {text: '%', rowSpan: 2, style: 'tableHeader', alignment: 'center'}
                        ],
                        [
                          {},
                          {text: 'PROGRESS', style: 'tableHeader',alignment: 'center'},
                          {text: 'APPROVE MTD', style: 'tableHeader',alignment: 'center'},
                          {text: 'APPROVE YTD', style: 'tableHeader',alignment: 'center'},
                          {text: 'MTD', style: 'tableHeader',alignment: 'center'},
                          {text: 'YTD', style: 'tableHeader',alignment: 'center'},
                          {text: 'MTD', style: 'tableHeader',alignment: 'center'},
                          {text: 'YTD', style: 'tableHeader',alignment: 'center'},
                          {}
                        ]];
    data.forEach(v=>{
      let styleCell = '';
      if(v.regional=='NASIONAL'){
        styleCell = 'tableHeader';
      }
      dataBody.push([
                          {text: v.regional, style: styleCell},
                          {text: v.jumlah_on_progress,alignment: 'center', style: styleCell},
                          {text: v.jumlah_pengajuan_mtd,alignment: 'center', style: styleCell},
                          {text: v.jumlah_pengajuan_ytd,alignment: 'center', style: styleCell},
                          {text: v.jumlah_ap_mtd,alignment: 'center', style: styleCell},
                          {text: v.jumlah_ap_ytd,alignment: 'center', style: styleCell},
                          {text: Intl.NumberFormat('id-ID').format(parseFloat(v.anggaran_mtd)),alignment: 'right', style: styleCell},
                          {text: Intl.NumberFormat('id-ID').format(parseFloat(v.anggaran_ytd)),alignment: 'right', style: styleCell},
                          {text: v.persentase,alignment: 'center', style: styleCell}
                        ]);
    })
    var docDefinition = {
                content: [
                  {text: 'REKAPITULASI PENYERAPAN ANGGARAN DISMANTLE AP', style: 'subheader', alignment: 'center'},
		              {text: `Posisi: ${tanggal}`, alignment: 'center'},
                  {
                    style: 'tableExample',
                    color: '#444',
                    table: {
                      headerRows: 2,
                      // keepWithHeaderRows: 1,
                      body: dataBody
                    }
                  }
                ],styles: {
                  header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 0, 0, 10]
                  },
                  subheader: {
                    fontSize: 14,
                    bold: true,
                    margin: [0, 10, 0, 5]
                  },
                  tableExample: {
                    margin: [0, 5, 0, 15]
                  },
                  tableHeader: {
                    bold: true,
                    fontSize: 11,
                    color: 'black',
                    fillColor: '#dddddd',
                  }
                },
                defaultStyle: {
                  // alignment: 'justify'
                }
              };
    const filename = `rekap_penyerapan_anggaran_${tanggal.replace(/ /g,"_")}.pdf`;
    pdfMake.createPdf(docDefinition).download(filename);
  }

  function exportToExcel(data, tanggal){
  var htmls = "";
            var uri = 'data:application/vnd.ms-excel;base64,';
            var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'; 
            var base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            };

            var format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                })
            };
            let dataBody = "";
            data.forEach(v=>{
              let additionStyle=""
              if(v.regional=="NASIONAL"){
                additionStyle=";background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB";
              }
              dataBody +=`<tr>
                <td style="text-align:center;${additionStyle}">${v.regional}</td>
                <td style="text-align:center;${additionStyle}">${v.jumlah_on_progress}</td>
                <td style="text-align:center;${additionStyle}">${v.jumlah_pengajuan_mtd}</td>
                <td style="text-align:center;${additionStyle}">${v.jumlah_pengajuan_ytd}</td>
                <td style="text-align:center;${additionStyle}">${v.jumlah_ap_mtd}</td>
                <td style="text-align:center;${additionStyle}">${v.jumlah_ap_ytd}</td>
                <td style="text-align:right;${additionStyle}">${Intl.NumberFormat('id-ID').format(parseFloat(v.anggaran_mtd))}</td>
                <td style="text-align:right;${additionStyle}">${Intl.NumberFormat('id-ID').format(parseFloat(v.anggaran_ytd))}</td>
                <td style="text-align:center;${additionStyle}">${v.persentase}</td>
                </tr>`;
            })
            htmls = `
            <table>
            <tr>
              <td colspan="8" style="text-align:center;">REKAPITULASI PENYERAPAN ANGGARAN DISMANTLE AP</td>
            </tr>
            <tr>
              <td colspan="8" style="text-align:center;">Posisi: ${tanggal}</td>
            </tr>
            </table>
            <table>
            <thead>
              <tr>
                <th rowspan="2" style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB">REGIONAL</th>
                <th colspan="3" style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB"><b>PENGAJUAN</b> </th>
                <th colspan="2" style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB"> <b> JUMLAH AP</b></th>
                <th colspan="2" style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB"> <b>PENYERAPAN ANGGARAN</b> </th>
                <th rowspan="2" style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB""> <b>%</b> </th>
              </tr>
              <tr>
                <td style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB"">PROGRESS</td>
                <td style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB"">APPROVE<br>MTD</td>
                <td style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB"">APPROVE<br>YTD</td>
                <td style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB"">MTD</td>
                <td style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB"">YTD</td>
                <td style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB"">MTD</td>
                <td style="text-align:center;background-color: #2284ad;color: #ffffff;border:1px solid #EBEBEB">YTD</td>
              </tr>
            </thead>
            <tbody>
              ${dataBody}
            </tbody>
          </table>`

            var ctx = {
                worksheet : 'Worksheet',
                table : htmls
            }

            var link = document.createElement("a");
            const filename = `rekap_penyerapan_anggaran_${tanggal.replace(/ /g,"_")}.xls`;
            link.download = filename;
            link.href = uri + base64(format(template, ctx));
            link.click();
}

  async function getDataSummaryTabel(url){
    let response = await fetch(url);
    let result = await response.json();		
    return result;
  }

  function pickColor(regional) {
    if(regional == 'TREG 1'){
      return "#0abb87";
    }else if(regional == 'TREG 2'){
      return '#ac31c7';
    }else if(regional == 'TREG 3'){
      return '#1728ed';
    }else if(regional == 'TREG 4'){
      return '#359bff';
    }else if(regional == 'TREG 5'){
      return '#6afda5';
    }else if(regional == 'TREG 6'){
      return '#84e645';
    }else if(regional == 'TREG 7'){
      return '#f39c12';
    }else{
      return '#ac31c7';
    }
  }

  function donutChartData(data) {
    let result = [];
    let total = 0;
    data.forEach(val=>{
      if(val.regional!=='NASIONAL'){
        total += parseFloat(val.anggaran_ytd);
        const regionalColor = pickColor(val.regional);
        result.push({name: val.regional, y: parseFloat(val.anggaran_ytd), color: regionalColor, sliced: true, 
              dataLabels: {
                  format: `<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">${val.regional}</span></span>`,
                  style: {
                      fontSize: "2rem",
                  }
              }
        });
      }
    });

    document.getElementById('circlebgValue').innerHTML = `Rp. `+Intl.NumberFormat('id-ID').format(total);
    
    return [{
      "id": "idData",
      "name": "Data",
      "data": result
    }];
  }

  async function generateFirstTime() {
    const data = await getDataSummaryTabel('{{route("admin.data")}}');

    const dataDonutChart = donutChartData(data.results);

    const titleText = 'Penyerapan Anggaran per-TREG YTD';
    generateDonutChart(dataDonutChart, titleText);

    generateDatatable(data.results)
  }

  generateFirstTime();

  document.getElementById('pdfBtn').addEventListener('click', function () {
    const data = JSON.parse(this.getAttribute('data-pdf'));
    const inputDateValue = document.getElementById('example-date-input').value;
    const tanggal = dayjs(inputDateValue).format('DD MMMM YYYY');
    exportToPdf(data, tanggal)
  });

  document.getElementById('excelBtn').addEventListener('click', function () {
    const data = JSON.parse(this.getAttribute('data-excel'));
    const inputDateValue = document.getElementById('example-date-input').value;
    const tanggal = dayjs(inputDateValue).format('DD MMMM YYYY');
    exportToExcel(data, tanggal)
  });

  function generateDatatable(data) {
    document.getElementById('pdfBtn').disabled = false;
    document.getElementById('excelBtn').disabled = false;
    document.getElementById('pdfBtn').setAttribute('data-pdf', JSON.stringify(data));
    document.getElementById('excelBtn').setAttribute('data-excel', JSON.stringify(data));
    $('#testing').DataTable({
        data: data,
        deferRender: true,
        "columns": [
            {"data": "regional",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
            },
            {"data": "jumlah_on_progress",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
            },
            {"data": "jumlah_pengajuan_mtd",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
            }, 
            {"data": "jumlah_pengajuan_ytd",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
            },
            {"data": "jumlah_ap_mtd",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
            },
            {"data": "jumlah_ap_ytd",
              "render": function(data, type, row) {
                      return '<font color="black"><center>' + data + '</center></font>'
                        }
                      },
            {"data": "anggaran_mtd",
              "render": function(data, type, row) {
                  return `<font color="black">Rp. ` + Intl.NumberFormat('id-ID').format(parseFloat(data))+`</center></font>`;
                }
            },
            {"data": "anggaran_ytd",
              "render": function(data, type, row) {
                  return `<font color="black">Rp. ` + Intl.NumberFormat('id-ID').format(parseFloat(data))+`</center></font>`;
              }
            },
            {"data": "persentase",
              "render": function(data, type, row) {
                  return `<font color="black">`+data+`</center></font>`;
              }
            },
        ],
        columnDefs: [
          {
            "targets": -2, // your case first column
            "className": "text-right"
          },
          {
            "targets": -3, // your case first column
            "className": "text-right"
          },
        ],
        "bPaginate": false,
        "bLengthChange": false,
        "bInfo": false,
        "ordering": false,
        "searching": false,
        "destroy": true,
        "sErrMode": "throw",
        "scrollCollapse": true
    });
}

const tanggal = document.getElementById('example-date-input');
if(tanggal){
  tanggal.addEventListener('change', async function(e) {
    const newUrl = '{{route("admin.data")}}?date='+e.target.value;
    const lastDate = dayjs(e.target.value).format('DD MMM YYYY');
    const firstDateThisYear = dayjs(e.target.value).startOf('year').format('DD MMM YYYY')
    const firstDateThisMonth = dayjs(e.target.value).startOf('month').format('DD MMM YYYY');
    
    document.getElementById('ytd-label').innerHTML = firstDateThisYear+' - '+lastDate;
    document.getElementById('mtd-label').innerHTML = firstDateThisMonth+' - '+lastDate;
    
    const data = await getDataSummaryTabel(newUrl);
    generateDatatable(data.results);
  });
}
</script>
@endif

@if(auth()->user()->hasRole('treg'))
<script>
  var data = [{
    "id": "idData",
    "name": "Data",
    "data":[
        {
          name: 'Disetujui', y: parseInt("{{$data->jumlah_approve}}"), color: '#0abb87', sliced: true, 
            dataLabels: {
                format: '<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">Disetujui</span></span>',
                style: {
                    fontSize: "2rem",
                }
            }
        },
        {
          name: 'Dalam proses', y: parseInt("{{$data->jumlah_on_progress}}"), color: '#f39c12', sliced: true, 
            dataLabels: {
                format: '<span style="font-weight: 500">{point.percentage:.1f}%<br><span style="font-size: 1rem; color: #999; font-weight: 300;">Dalam proses</span></span>',
                style: {
                    fontSize: "2rem",
                }
            }
        }
    ]
  }];


  const titleText = 'Total Access Point Tahun {{date("Y")}}';

  generateDonutChart(data, titleText)
</script>
@endif
@endpush