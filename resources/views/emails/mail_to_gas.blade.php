<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>No Reply</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
    </style>
</head>

<body style="font-family: verdana; font-size: 14px;">
    <div class="bg" style="background: #FFF; width: 70%; margin: 0 auto;">
        <div id="logo" style="background: #FFF;"><img src="{{asset('assets/images/logo.png')}}"
                style="max-height: 70px; margin-top: 20px;"></div>
        <div id="confirmation-message">
            <div class="ravis-title-t-2" style="text-align: left; margin-top: 20px;">
                <div class="title" style="color: #1e1e1e; font-size: 14px;">
                    <span style="text-transform:capitalize;">Nama: {{$user['name']??' - '}}</span><br>
                    <span style="text-transform:capitalize;">Regional: TREG {{$user['regional']??' - '}}</span><br>
                    <span style="text-transform:capitalize;">Jumlah AP: {{$verified_access_point??' - '}}</span><br>
                    <span style="text-transform:capitalize;">Nilai Invoice: Rp. {{$payment_value?number_format($payment_value,2,',', '.'):' - '}}</span><br>
                    <span style="text-transform:capitalize;">Tanggal NDE: {{$nde_date??' - '}}</span><br>
                </div>
            </div>
            <div class="desc" style="color: #1e1e1e; margin-top:20px; font-siz: 14px;">
                <div style="border-bottom: 1px dashed #efefef; padding-bottom: 10px;">
                    Mengajukan invoice dismantle access point pada tanggal {{$tanggal}}<br>
                Klik <a href="{{config('app.url')}}/panel/gas/{{$uuid??'-'}}" target="_blank" style="color: #5d78ff;">disini</a> untuk melihat detail pengajuan.
                    <br><br>

                    Hubungi Admin jika Anda butuh bantuan lebih lanjut.<br><br>
                </div>
            </div>
        </div>
        <div style="font-size: 8pt">
            ---------------------<br>
            <i>
            DISCLAIMER :
            This electronic mail and/ or any files transmitted with it may contain confidential or copyright information of PT. Telekomunikasi Indonesia, Tbk. and/ or its Subsidiaries. If you are not an intended recipient, you must not keep, forward, copy, use, or rely on this electronic mail, and any such action is unauthorized and prohibited. If you have received this electronic mail in error, please reply to this electronic mail to notify the sender of its incorrect delivery, and then delete both it and your reply. Finally, you should check this electronic mail and any attachments for the presence of viruses. PT. Telekomunikasi Indonesia, Tbk. accepts no liability for any damages caused by any viruses transmitted by this electronic mail.
            </i>
        </div>
    </div>

</body>

</html>