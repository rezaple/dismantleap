
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>No Reply</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
    </style>
</head>

<body style="font-family: verdana; font-size: 14px;">
    <div class="bg" style="background: #FFF; width: 70%; margin: 0 auto;">
        <div id="logo" style="background: #FFF;"><img src="{{asset('assets/images/logo.png')}}"
                style="max-height: 70px; margin-top: 20px;"></div>
        <div id="confirmation-message">
            <div class="ravis-title-t-2" style="text-align: left; margin-top: 20px;">
                <div class="title" style="color: #1e1e1e; font-size: 24px;">
                    <span style="text-transform:capitalize;">Hello, {{$name}}</span>
                </div>
            </div>
            <div class="desc" style="color: #1e1e1e; margin-top:20px; font-siz: 14px;">
                <div style="border-bottom: 1px dashed #efefef; padding-bottom: 10px;">
                Email ini berisi username dan password bawaan untuk masuk ke dalam Aplikasi Dismantle AP. <br><br>
                <b>Username: </b> {{$username}} <br><br>
                <b>Password: </b> {{$password}} <br><br>
                Mohon untuk segera mengubah password bawaan setelah Anda masuk ke dalam Aplikasi Dismantle AP. Terima Kasih. <br><br>
                    Hubungi Admin jika Anda butuh bantuan lebih lanjut.<br><br>
                </div>
            </div>
        </div>
        <div style="font-size: 8pt">
            ---------------------<br>
            <i>
            DISCLAIMER :
            This electronic mail and/ or any files transmitted with it may contain confidential or copyright information of PT. Telekomunikasi Indonesia, Tbk. and/ or its Subsidiaries. If you are not an intended recipient, you must not keep, forward, copy, use, or rely on this electronic mail, and any such action is unauthorized and prohibited. If you have received this electronic mail in error, please reply to this electronic mail to notify the sender of its incorrect delivery, and then delete both it and your reply. Finally, you should check this electronic mail and any attachments for the presence of viruses. PT. Telekomunikasi Indonesia, Tbk. accepts no liability for any damages caused by any viruses transmitted by this electronic mail.
            </i>
        </div>
    </div>

</body>

</html>