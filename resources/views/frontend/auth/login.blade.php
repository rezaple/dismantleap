<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->

<head>
	<meta charset="utf-8" />
	<title>Dismantle AP | Login Page</title>
	<meta name="description" content="Dismantle">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!--begin::Fonts -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
	</script>

	<link href="{{ asset ('assets')}}/theme/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets') }}/theme/assets/app/custom/login/login-v6.default.css" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('assets') }}/theme/assets/demo/default/base/style.bundle.css" rel="stylesheet"
		type="text/css" />
	<link rel="shortcut icon" href="{{ asset('images') }}/favicon.ico" />

	<style>
		.kt-login.kt-login--v6 .kt-login__aside {
			z-index: 2;
			position: relative;
		}

		.kt-login__logo img {
			max-height: 3.5vw;
			margin-top: 2vw;
			opacity: 0.7;
		}

		.titlelog {
			font-weight: bold;
			margin-bottom: -0.5vw;
			color: #505050;
			text-align: left;
		}

		.titlelog span {
			font-weight: 300;
		}

		.logotitle {
			margin-top: 6.5vw;
		}

		.kt-login__title {
			color: #9a9a9a !important;
			font-size: 1.2vw !important;
			letter-spacing: 0.1vw;
		}

		.kt-login.kt-login--v6 {
			background: transparent;
		}

		.kt-login.kt-login--v6 .kt-login__aside {
			background: transparent;
		}

		body {
			background: url('{{ asset ('assets') }}/images/bg1.png') no-repeat center right;
			background-size: 50% 110%;
		}

		.btn-success {
			background: #037482;
			border-color: #037482;
		}

		.btn-success:hover {
			background: #025863;
			border-color: #025863;
		}

		.kt-login.kt-login--v6 .kt-login__aside .kt-login__wrapper .kt-login__container .kt-login__body {
			margin-bottom: -6.5vw;
		}

		@media (max-width: 1024px) {

			html,
			body {
				background: #FFF;
				background-size: cover !important;
				background-position: 70% 0vw !important;
			}

			.titlelog {
				font-size: 3.7rem;
			}

			.kt-login.kt-login--v6 .kt-login__aside .kt-login__wrapper .kt-login__container {
				width: 360px;
			}

			.kt-login.kt-login--v6 {
				background: rgba(255, 255, 255, .6);
			}

			.kt-login__title {
				font-size: 1.3rem !important;
			}

			.kt-login__logo img {
				max-height: 3rem;
				margin-top: 5rem;
			}
		}

		.text-md {
			font-size: 1.15rem;
		}
	</style>
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body
	class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

	<!-- begin:: Page -->
	<div class="kt-grid kt-grid--ver kt-grid--root">
		<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
			<div
				class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
				<div
					class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
					<div class="kt-login__wrapper">
						<div class="kt-login__container">
							<div class="kt-login__body">
								<div class="text-center logotitle">
									<!-- <a href="#"> -->
									<h1 class="titlelog">Login to Dismantle AP</h1>
									<!-- </a> -->
								</div>

								<div class="kt-login__signin">
									<div class="kt-login__head">
										<h3 class="kt-login__title text-left"><i>Sign into your account</i></h3>
									</div>
									<div class="kt-login__form">

										@include('includes.partials.messages')

										<form id="signin" class="sycerdaslogin kt-form" action="{{route('frontend.auth.login.post')}}" method="post">
											{!! csrf_field() !!}
											<div class="form-group">
												<input class="form-control" type="text" name="username" id="username"
													maxlength="191" placeholder="username" required=""
													value="{{old('username')}}" autocomplete="off">
											</div>
											<br>
											<div class="form-group input-group">
												<input class="form-control" autocomplete="new-password" type="password" name="password"
													id="password" placeholder="Password" required="" >
												<div class="input-group-append" id="toggleShowPass">
													<span class="input-group-text" id="basic-addon2">
														<i class="fa fa-eye-slash" id="eye-icon"></i>
													</span>
												</div>
											</div>
											<br>
											<div class="form-group input-group">
												<a href="{{route('frontend.auth.password.email')}}" id="forget-password" class="forget-password">Forgot Password?</a>
											</div>
											<div class="kt-login__actions text-left">
												<button class="btn btn-success btn-sm btn-elevate text-md"
													type="input">Login</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-login__account">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
			const toggleShowPass = document.getElementById('toggleShowPass');

			toggleShowPass.addEventListener('click', function(){
				this.classList.toggle('active');

				const pass = document.getElementById('password');
				const iconEye = document.getElementById("eye-icon");

				if (this.classList.contains('active')){
					pass.type = 'text';
					iconEye.classList.remove('fa-eye-slash');
					iconEye.classList.add('fa-eye');
				} else {
					iconEye.classList.remove('fa-eye');
					iconEye.classList.add('fa-eye-slash');
					pass.type = 'password';
				}
			});

			document.addEventListener('click', function (event) {
				// If the clicked element doesn't have the right selector, bail
				if (!event.target.matches('.close')) return;

				// Don't follow the link
				event.preventDefault();
				// Log the clicked element in the console
				event.target.parentNode.parentNode.removeChild(event.target.parentNode)

			}, false);


	</script>
</body>

<!-- end::Body -->

</html>