@extends('frontend.layouts.main')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')
    <div class="row justify-content-center mt-5 h-100">
        <div class="col-md-5 my-auto">

            @include('includes.partials.messages')
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">

                    {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
                        <h1>Login</h1>
                        <p class="text-muted">Sign In to your account test</p>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend"><span class="input-group-text">
                                <svg class="bi bi-person-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z"/>
                                    <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                    <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z"/>
                                  </svg></span></div>

                            {{ html()->text('username')
                                        ->class('form-control')
                                        ->attribute('maxlength', 191)
                                        ->placeholder('username')
                                        ->required() }}
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend"><span class="input-group-text">
                                <svg class="bi bi-lock-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <rect width="11" height="9" x="2.5" y="7" rx="2"/>
                                    <path fill-rule="evenodd" d="M4.5 4a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z"/>
                                  </svg></span>
                            </div>
                                  {{ html()->password('password')
                                  ->class('form-control')
                                  ->placeholder(__('validation.attributes.frontend.password'))
                                  ->required() }}
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <button class="btn btn-primary px-4" type="input">Login</button>
                            </div>
                        </div>
                    </div>

                    {{ html()->form()->close() }}
                </div>
            </div>
        </div>
    </div>
@endsection