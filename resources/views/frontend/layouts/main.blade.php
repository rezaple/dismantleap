<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>

        {{ style(mix('css/frontend.css')) }}

    </head>
    <body class="bg-light">

        <div id="app">

            <div class="container">
                @yield('content')
            </div><!-- container -->
        </div><!-- #app -->

        @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!}
        {!! script(mix('js/frontend.js')) !!}
        @stack('after-scripts')
    </body>
</html>
