<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
                flex-direction: column;
            }

            .message-group{
                display: flex;
                align-items: center;
                justify-content: center;
            }

            .link{
                text-align: center;
                font-size: 18px;
            }

            .position-ref {
                position: relative;
            }

            .code {
                font-size: 32px;
                padding: 0 15px 0 15px;
                text-align: center;
            }

            .message {
                padding-left:20px; 
                font-size: 26px;
                text-align: center;
            }

            .message a{
                text-decoration: none;
                color: #3a9450;
            }
            
            .code-group{
                border-right: 2px solid;
                padding-right: 20px;
                display: flex;
                flex-direction: column;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height ">
            <div class="message-group">
                <div class="code-group">
                    <div class="code">
                        @yield('code')
                    </div>
    
                    <div class="link">

                    @yield('message')
                    </div>
                </div>

                <div class="message">
                    <a href="{{route('frontend.index')}}">Home</a>
                </div>
            </div>

            
        </div>
        
    </body>
</html>
