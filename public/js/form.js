function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    const passStrengText = document.getElementById('password-strength-text');
    if(pass.length > 8){

        const score = scorePassword(pass);
        let scoreText = "";
        if (score > 80)
            scoreText = `Strength: <span class="dark-green">Strong</span>`;
        else if (score > 60)
            scoreText = `Strength: <span class="green">Good</span>`;
        else if (score >= 30)
            scoreText = `Strength: <span class="red">Weak</span>`;

        passStrengText.innerHTML = scoreText;
    }else{
        passStrengText.innerHTML = '';
    }
}

document.getElementById("password").addEventListener("keydown", function(){
    checkPassStrength(this.value)
});

const ldap = document.getElementById('ldap');
if(ldap){
    ldap.onclick = function(){
        const isChecked = this.checked
        document.getElementById('email').disabled = isChecked;
        document.getElementById('phone_number').disabled = isChecked;
        document.getElementById('password').disabled = isChecked;
        document.getElementById('password_confirmation').disabled = isChecked;
        document.getElementById('generateGroup').disabled = isChecked;
    }
    if(ldap.checked){
        const isChecked = ldap.checked
        document.getElementById('email').disabled = isChecked;
        document.getElementById('phone_number').disabled = isChecked;
        document.getElementById('password').disabled = isChecked;
        document.getElementById('password_confirmation').disabled = isChecked;
        document.getElementById('generateGroup').disabled = isChecked;
    }
}

const btnGenerate = document.getElementById('generatePassword');
btnGenerate.addEventListener('click', function(){
    const pass = generatePassword();
    document.getElementById('passwordResult').value = pass;
});

const btnCopy = document.getElementById('copyToClip');
btnCopy.addEventListener('click', function(){
    copyPassword()
});

function generatePassword() {
    var length = 12,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

function copyPassword(){
    const passResult = document.getElementById("passwordResult");
    const passwordInput = document.getElementById('password');
    const passwordConfirmationInput = document.getElementById('password_confirmation');
    passwordConfirmationInput.value = passResult.value;
    passwordInput.value = passResult.value;
    passResult.select();
    document.execCommand("Copy");
    alert("Password copied to clipboard!");

    checkPassStrength(passwordInput.value);
}