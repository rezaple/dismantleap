<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidateAccessPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_access_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->unsignedBigInteger('submission_id');
            $table->string('ap_name')->nullable();
            $table->string('mac_address');
            $table->string('witel');
            $table->string('regional');
            $table->string('tipe')->nullable();
            $table->string('status')->nullable();
            $table->string('periode')->nullable();
            $table->string('flag_dismantle',12)->nullable();
            $table->date('date_dismantle')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });

        Schema::table('candidate_access_points', function (Blueprint $table) {
            $table->foreign('submission_id')->references('id')->on('submissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_access_points');
    }
}
