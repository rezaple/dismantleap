<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->unsignedBigInteger('user_id');
            $table->string('name')->nullable();
            $table->string('vendor_name')->nullable();
            $table->string('vendor_no')->nullable();
            $table->decimal('payment_value', 12, 2)->default(0);
            $table->decimal('contract_value', 12, 2)->default(0);
            $table->decimal('total', 12, 2)->default(0);
            $table->integer('total_access_point')->default(0);
            $table->integer('verified_access_point')->default(0);
            $table->integer('unverified_access_point')->default(0);
            $table->date('contract_date')->nullable();
            $table->date('submission_date')->nullable();
            $table->date('approved_date')->nullable();
            $table->date('nde_date')->nullable();
            $table->string('regional')->nullable();
            $table->string('tipe',32)->nullable();
            $table->string('status');
            $table->timestamps();
        });

        Schema::table('submissions', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submissions');
    }
}
