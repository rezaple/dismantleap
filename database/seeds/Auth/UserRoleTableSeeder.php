<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        User::find(1)->assignRole(config('access.users.admin_role'));

        if(config('prod.status') === false) {

            User::where('username','user_treg')->first()->assignRole('treg');
            User::where('username','user_wdm')->first()->assignRole('wdm');
            User::where('username','user_gas')->first()->assignRole('gas');
            User::where('username','user_treg2')->first()->assignRole('treg');
            User::where('username','user_treg3')->first()->assignRole('treg');
            User::where('username','user_treg4')->first()->assignRole('treg');
            User::where('username','user_treg5')->first()->assignRole('treg');
            User::where('username','user_treg6')->first()->assignRole('treg');
            User::where('username','user_treg7')->first()->assignRole('treg');
        }

        $this->enableForeignKeys();
    }
}
