<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $adminRole = Role::create(['name' => config('access.users.admin_role')]);
        $userRole = Role::create(['name' => 'treg']);
        $wdmRole = Role::create(['name' => 'wdm']);
        $gasRole = Role::create(['name' => 'gas']);

        // Create Permissions
        Permission::create(['name' => 'view backend']);
        Permission::create(['name' => 'view logs']);
        Permission::create(['name' => 'manage submissions']);
        Permission::create(['name' => 'manage approval wdm']);
        Permission::create(['name' => 'manage approval gas']);
        Permission::create(['name' => 'manage roles']);
        Permission::create(['name' => 'manage users']);

        $userRole->givePermissionTo('view backend');
        $userRole->givePermissionTo('manage submissions');

        $wdmRole->givePermissionTo('view backend');
        $wdmRole->givePermissionTo('manage approval wdm');

        $gasRole->givePermissionTo('view backend');
        $gasRole->givePermissionTo('manage approval gas');

        // Assign Permissions to other Roles
        // Note: Admin (User 1) Has all permissions via a gate in the AuthServiceProvider
        // $user->givePermissionTo('view backend');

        $this->enableForeignKeys();
    }
}
