<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        User::create([
            'name' => 'Super Admin',
            'email' => 'admin@mail.com',
            'username' => 'dis_ap_ad',
            'password' => 'secret',
            'phone_number' => '085283979510',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed' => true,
            'otp_login' => true,
        ]);

        if(config('prod.status') === false) {
            User::create([
                'name' => 'John Doe',
                'email' => 'treg_1@mail.com',
                'username' => 'user_treg',
                'password' => 'secret',
                'regional' => '1',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);
    
            User::create([
                'name' => 'Smithem',
                'email' => 'nurul.muhsinin@telkom.co.id',
                'username' => 'user_wdm',
                'password' => 'secret',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);
    
            User::create([
                'name' => 'Michele',
                'email' => 'azer.ple@gmail.com',
                'username' => 'user_gas',
                'password' => 'secret',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);
            
            User::create([
                'name' => 'Jane',
                'email' => 'treg_2@mail.com',
                'username' => 'user_treg2',
                'password' => 'secret',
                'regional' => '2',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);

            User::create([
                'name' => 'Karina',
                'email' => 'treg_3@mail.com',
                'username' => 'user_treg3',
                'password' => 'secret',
                'regional' => '3',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);

            User::create([
                'name' => 'Susan',
                'email' => 'treg_4@mail.com',
                'username' => 'user_treg4',
                'password' => 'secret',
                'regional' => '4',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);

            User::create([
                'name' => 'Donald',
                'email' => 'treg_5@mail.com',
                'username' => 'user_treg5',
                'password' => 'secret',
                'regional' => '5',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);

            User::create([
                'name' => 'Randy',
                'email' => 'treg_6@mail.com',
                'username' => 'user_treg6',
                'password' => 'secret',
                'regional' => '6',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);

            User::create([
                'name' => 'Jonhy',
                'email' => 'treg_7@mail.com',
                'username' => 'user_treg7',
                'password' => 'secret',
                'regional' => '7',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);

            User::create([
                'name' => 'Smith',
                'email' => 'user_wdm@mail.com',
                'username' => 'user_wdm2',
                'password' => 'secret',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'otp_login' => false,
            ]);
        }

        $this->enableForeignKeys();
    }
}
