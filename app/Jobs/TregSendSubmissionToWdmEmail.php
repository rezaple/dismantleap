<?php

namespace App\Jobs;

use Mail;
use App\Models\Auth\User;
use App\Mail\SubmissionMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TregSendSubmissionToWdmEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    public function __construct($data)
    {
        $this->queue = 'email';
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $data['tanggal'] = date('d M Y, H:i');
        $data['pesan'] = 'Mengajukan Pengajuan';

        $users = User::select('id','uuid','email','name')->whereHas('roles', function ($query) {
            $query->where('name', 'wdm');
        })->get();

        $userWdm = $users->toArray();

        //$emails = $users->pluck('email');

        // Mail::send('emails.mail_to_wdm', $data, function ($m) use ($emails) {
        //     $m->from("no_reply@mail.com",'Dismantle AP')->to(['reza.muhsinin@gmail.com'])->subject('Pengajuan Invoice Dismantle');
        // });

        Mail::to($userWdm)->send(new SubmissionMail('wdm',$data));
    }
}
