<?php

namespace App\Jobs;

use Mail;
use App\Mail\ReturnMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GasSendReturnEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mail;

    protected $data;

    public function __construct($mail, $data)
    {
        $this->queue = 'email';
        $this->mail = $mail;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $data['pesan'] = 'Dikembalikan';
        $data['by_user'] = 'User GAS';

        // $email = $this->email;
        // Mail::send('emails.mail', $data, function ($m) use ($email) {
        //     $m->from("no_reply@mail.com",'Dismantle AP');
        //     $m->to($email)->subject('Pengajuan Dikembalikan');
        // });

        Mail::to($this->mail)->send(new ReturnMail($data));
    }
}
