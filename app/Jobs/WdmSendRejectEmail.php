<?php

namespace App\Jobs;

use App\Mail\RejectMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class WdmSendRejectEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mail;

    protected $data;

    public function __construct($mail, $data)
    {
        $this->queue = 'email';
        $this->mail = $mail;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $data['pesan'] = 'Ditolak';
        $data['by_user'] = 'User WDM';

        // $email = $this->email;
        // Mail::send('emails.mail', $data, function ($m) use ($email) {
        //     $m->from("no_reply@mail.com",'Dismantle AP');
        //     $m->to($email)->subject('Pengajuan Ditolak');
        // });

        Mail::to($this->mail)->send(new RejectMail($data));
    }
}
