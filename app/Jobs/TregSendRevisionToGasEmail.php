<?php

namespace App\Jobs;

use Mail;
use App\Models\Auth\User;
use App\Mail\RevisionMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TregSendRevisionToGasEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    public function __construct($data)
    {
        $this->queue = 'email';
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $data['tanggal'] = date('d M Y, H:i');
        $data['pesan'] = 'Revisi';

        $users = User::select('id','uuid','email','name')->whereHas('roles', function ($query) {
            $query->where('name', 'gas');
        })->get();

        $userGas = $users->toArray();

        $emails = $users->pluck('email');

        // Mail::send('emails.mail_revision_gas', $data, function ($m) use ($emails) {
        //     $m->from("no_reply@mail.com",'Dismantle AP');
    
        //     $m->to($emails) ->subject('Pengajuan Kembali Invoice Dismantle');
        // });

        Mail::to($userGas)->send(new RevisionMail($data));
    }
}
