<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubmissionMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    protected $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($type, $data)
    {
        $this->data = $data;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $markwdown = 'emails.mail_to_gas';

        if($this->type=='wdm'){
            $markwdown = 'emails.mail_to_wdm';
        }

        return $this->from("no_reply@mail.com",'Dismantle AP')
                ->subject('Pengajuan Invoice Dismantle')->markdown($markwdown)->with($this->data);
    }
}
