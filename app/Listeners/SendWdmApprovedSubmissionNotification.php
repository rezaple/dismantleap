<?php

namespace App\Listeners;

use Notification;
use App\Models\Auth\User;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\WdmApproveSubmission;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWdmApprovedSubmissionNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $userTreg = User::where('regional', $event->data->regional)->get();

        Notification::send($userTreg, new WdmApproveSubmission($event->data));
    }
}
