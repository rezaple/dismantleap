<?php

namespace App\Listeners;

use App\Models\Auth\User;
use App\Notifications\RevisionSubmissionToGas;
use Illuminate\Queue\InteractsWithQueue;
use Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRevisionSubmissionToGasNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $gas = User::whereHas('roles', function ($query) {
            $query->where('name', 'gas');
        })->get();

        Notification::send($gas, new RevisionSubmissionToGas($event->user, $event->data));
    }
}
