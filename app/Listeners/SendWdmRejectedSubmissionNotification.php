<?php

namespace App\Listeners;

use Notification;
use App\Models\Auth\User;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\WdmRejectSubmission;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWdmRejectedSubmissionNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $userTreg = User::whereHas('roles', function ($query) {
            $query->where('name', 'treg');
        })->where('regional', $event->data->regional)->get();

        Notification::send($userTreg, new WdmRejectSubmission($event->data));
    }
}
