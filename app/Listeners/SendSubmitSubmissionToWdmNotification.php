<?php

namespace App\Listeners;

use App\Models\Auth\User;
use App\Notifications\SubmitSubmissionToWdm;
use Illuminate\Queue\InteractsWithQueue;
use Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSubmitSubmissionToWdmNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $wdm = User::whereHas('roles', function ($query) {
            $query->where('name', 'wdm');
        })->get();

        Notification::send($wdm, new SubmitSubmissionToWdm($event->user, $event->data));
    }
}
