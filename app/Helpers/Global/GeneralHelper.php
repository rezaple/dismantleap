<?php

if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}
if (! function_exists('array_flatten')) {
    function array_flatten($array) {
        $return = array();
        foreach ($array as $key => $value) {
            if (is_array($value)){
                $return = array_merge($return, array_flatten($value));
            } else {
                $return[$key] = $value;
            }
        }
    
        return $return;
    }
}

if (! function_exists('print_props')) {
    function print_props($props) {
        $datas = json_decode($props);
        $result = "";
        foreach($datas as $key => $data){
            $title = \Str::title(str_replace('_',' ',$key));
            $result .= "$title: $data <br>";
        }

        return $result;
    }
}

if (! function_exists('print_status')) {
    function print_status($status) {
        if($status == 'draft'){
            return '<span class="badge badge-secondary badge-pill">Draft</span>';
        }else if($status === 'submitted-to-wdm' || $status === 'submitted-to-gas'){
            $return = '<span class="badge badge-info badge-pill">Submitted</span>';
        }else if($status === 'revision-to-wdm' || $status === 'revision-to-gas'){
            $return = '<span class="badge badge-info badge-pill">Revision</span>';
        }else if($status === 'approved-by-wdm' || $status === 'approved-by-gas'){
            $return = '<span class="badge badge-success badge-pill">Approved</span>';
        }else if($status === 'return-by-wdm' || $status === 'return-by-gas'){
            $return = '<span class="badge badge-warning badge-pill">Returned</span>';
        }else if($status === 'reject-by-wdm' || $status === 'reject-by-gas'){
            $return = '<span class="badge badge-danger badge-pill">Rejected</span>';
        }else{
            $return = '<span class="badge badge-dark badge-pill">No Info</span>';
        }

        return $return;
    }
}

if (! function_exists('print_notif')) {
    function print_notif($status) {
        if($status === 'reject'){
            return '<span class="badge badge-danger text-xs">Rejected</span>';	
        }else if($status === 'approved'){
            return '<span class="badge badge-success text-xs">Approved</span>';	
        }else if($status === 'submitted'){
            return '<span class="badge badge-info text-xs">Submitted</span>';	
        }else if($status === 'revision'){
            return '<span class="badge badge-info text-xs">Revision</span>';	
        }else{
            return '<span class="badge badge-dark text-xs">No Info</span>';
        }
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('home_route')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function home_route()
    {
        if (auth()->check()) {
            if (auth()->user()->can('view backend')) {
                return 'admin.dashboard';
            }

            return 'frontend.index';
        }

        return 'frontend.index';
    }
}
