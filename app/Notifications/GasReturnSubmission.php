<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GasReturnSubmission extends Notification
{
    use Queueable;

    protected $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Pengajuan Dismantle Access Point Anda telah Ditolak oleh User GAS.')
                    ->action('Notification Action', url('/'))
                    ->line('Hubungi Admin jika Anda butuh bantuan lebih lanjut.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'uuid' => $this->data->uuid,
                'status' => 'return',
                'label' => 'to-user-treg',
                'message' => 'Pengajuan dikembalikan'
            ]
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'uuid' => $this->data->uuid,
            'status' => 'return',
            'label' => 'to-user-treg',
            'message' => 'Pengajuan dikembalikan'
        ];
    }
}
