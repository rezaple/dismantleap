<?php

namespace App\Http\Controllers\Frontend\Auth;

use Hash;
use GuzzleHttp\Client;
use App\Models\Auth\User;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\Controllers\Traits\Otp;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Events\Frontend\Auth\UserLoggedIn;
use App\Events\Frontend\Auth\UserLoggedOut;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

class LoginController extends Controller
{
    use AuthenticatesUsers, Otp;

    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    public function redirectPath()
    {
        return route(home_route());
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('frontend.auth.login');
    }

    protected function attemptLogin(Request $request)
    {
        $fieldType = filter_var($request->input($this->username()), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $newRequest = array($fieldType => $request->input($this->username()), 'password' => $request->password);
        
        return $this->guard()->attempt($newRequest, $request->filled('remember'));
    }

    public function ldapLogin($request)
    {
        $client = new Client(["base_uri" => "https://apifactory.telkom.co.id:8243", "http_errors"=>false]);

        $options = [
            'form_params' => [
                "username" => $request->username,
                "password" => $request->password
            ]
        ]; 

        $response = $client->post('/hcm/auth/v1/token', $options);

        return $response;
    }

    public function sendOtpTelegram(Request $request)
    {
        $users =  \Cookie::get('users');
        
        if($users){
            $user = json_decode($users);

            $response = $this->otpStatusCode($user->username);
            if($response && $response->getStatusCode() === 201){
                return redirect()->route('frontend.otp.verify');
            }
        }

        return redirect()->route('frontend.auth.login');
    }

    public function validateOtp(Request $request){
        $users =  \Cookie::get('users');
        
        if($users){
            $user = json_decode($users);

            $otpCode = $request->otp_code ?? "";

            $response = $this->otpStatusCode($user->username, $otpCode, 'check');;
                
            if($response && $response->getStatusCode() === 200){
                auth()->loginUsingId($user->id);
                $this->sendLoginResponse($request);
                
                $ip= $request->ip();
                $browser= $this->getBrowser();
                $this->addDevice($user->username, $ip, $browser);

                return redirect('/');
            }else{
                return redirect()->back()->with(['flash_danger'=>"OTP Code Invalid"]);
            }
        }
        return redirect('/');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $user = User::where('username', $request->username)->orWhere('email', $request->username)->select('id','ldap','password','otp_login','username','phone_number')->first();

        if($user && $user->otp_login){
            //jika bukan user ldap
            if($user && !$user->ldap){
                if(Hash::check($request->password, $user->password)){
                    $username = $request->username;
                    $ip= $request->ip();
                    $browser= $this->getBrowser();

                    $response =  $this->cekPermitted($username, $ip, $browser);

                    $result = json_decode($response->getBody()->getContents());

                    if($response->getStatusCode() === 200){
                        auth()->loginUsingId($user->id);
                        return $this->sendLoginResponse($request);
                    }else{
                        $cookie = cookie('users', $user, 10);
                        return redirect()->route('frontend.otp')->cookie($cookie);;
                    }
                }
            }elseif($user && $user->ldap){
                $response = $this->ldapLogin($request);
    
                $result = json_decode($response->getBody()->getContents());
    
                if($response->getStatusCode() === 201 && $result->status === "success"){
                    $username = $request->username;
                    $ip= $request->ip();
                    $browser= $this->getBrowser();

                    $response =  $this->cekPermitted($username, $ip, $browser);

                    $result = json_decode($response->getBody()->getContents());

                    if($response->getStatusCode() === 200){
                        auth()->loginUsingId($user->id);
                        return $this->sendLoginResponse($request);
                    }else{
                        $cookie = cookie('users', $user, 10);
                        return redirect()->route('frontend.otp')->cookie($cookie);
                    }
                }
            }
        }else{
            if($user && !$user->ldap && $this->attemptLogin($request)){
                return $this->sendLoginResponse($request);
            }elseif($user && $user->ldap){
                $response = $this->ldapLogin($request);
    
                $result = json_decode($response->getBody()->getContents());
    
                if($response->getStatusCode() === 201 && $result->status === "success"){
                    auth()->loginUsingId($user->id);
                    return $this->sendLoginResponse($request);
                }
            }
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function getBrowser()
    {
        $agent = new Agent();
        $browser = $agent->browser();
        $version = explode('.', $agent->version($browser));
        return "$browser $version[0]"; 
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return config('access.users.username');
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => PasswordRules::login(),
            'g-recaptcha-response' => ['required_if:captcha_status,true', 'captcha'],
        ], [
            'g-recaptcha-response.required_if' => __('validation.required', ['attribute' => 'captcha']),
        ]);
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param         $user
     *
     * @throws GeneralException
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function authenticated(Request $request, $user)
    {
        // Check to see if the users account is confirmed and active
        if (! $user->isConfirmed()) {
            auth()->logout();

            // If the user is pending (account approval is on)
            if ($user->isPending()) {
                throw new GeneralException(__('exceptions.frontend.auth.confirmation.pending'));
            }

            // Otherwise see if they want to resent the confirmation e-mail

            throw new GeneralException(__('exceptions.frontend.auth.confirmation.resend', ['url' => route('frontend.auth.account.confirm.resend', e($user->{$user->getUuidName()}))]));
        }

        if (! $user->isActive()) {
            auth()->logout();

            throw new GeneralException(__('exceptions.frontend.auth.deactivated'));
        }

        event(new UserLoggedIn($user));

        if (config('access.users.single_login')) {
            auth()->logoutOtherDevices($request->password);
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        // Remove the socialite session variable if exists
        if (app('session')->has(config('access.socialite_session_name'))) {
            app('session')->forget(config('access.socialite_session_name'));
        }

        // Fire event, Log out user, Redirect
        event(new UserLoggedOut($request->user()));
        //activity()->log('logout');

        // Laravel specific logic
        $this->guard()->logout();
        $request->session()->invalidate();

        return redirect()->route('frontend.index');
    }
}
