<?php

namespace App\Http\Controllers\Frontend;

use GuzzleHttp\Client;
use App\Models\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.auth.login');
    }

    public function showVerification(Request $request)
    {
        $users =  \Cookie::get('users');

        if($users){
            $user = json_decode($users);
            return view('frontend.auth.verification', compact('user'));
        }
        return redirect()->route('frontend.auth.login');
    }

    public function showInputVerification(Request $request)
    {
        $users =  \Cookie::get('users');
        
        if($users){
            $user = json_decode($users);
            return view('frontend.auth.input', compact('user'));
        }

        return redirect()->route('frontend.auth.login');
    }

    //cara daftar bot telegram
    public function showOtpTelegram()
    {
        return view('frontend.auth.otp-telegram');
    }

    public function checkUserLdap($username)
    {
        $urlGetProfile = "https://auth.telkom.co.id/api/call/$username";
        $profileJson = file_get_contents($urlGetProfile);
        return $profileJson;
    }

    public function checkUserLocal($username)
    {
        $user = User::where('username', $username)->first();
        if($user){
            return response()->json([
                'status' => 'OK',
                'exist' => true,
                'message' => 'User found'
            ]);
        }
        return response()->json([
            'status' => 'OK',
            'exist' => false,
            'message' => 'User not found'
        ]);
    }
}
