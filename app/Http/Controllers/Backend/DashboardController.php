<?php

namespace App\Http\Controllers\Backend;

use DB;
use App\Models\Submission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        $treg = [];
        $tot = 0;
        //tambah tanggal dari awal tahun ke tanggal sekarang
        $startDateThisYear = date('Y').'-01-01';
        $startDateThisMonth = date('Y-m').'-01';
        $endDate = $request->has('date')?date('Y-m-d', strtotime($request->date)):date('Y-m-d');
        if($request->has('date')){
            $startDateThisYear = date('Y', strtotime($request->date)).'-01-01';
            $startDateThisMonth = date('Y-m', strtotime($request->date)).'-01';
        }

        $data = DB::table('submissions')
                ->select(DB::raw("COALESCE(SUM(CASE  
                                WHEN status='approved-by-gas' then payment_value ELSE 0 END),0) as nilai_approve, 
                                COALESCE(SUM(CASE  
                                WHEN status IN ('submitted-to-wdm','submitted-to-gas','return-by-gas','approved-by-wdm','revision-to-gas') then payment_value ELSE 0 END),0) as nilai_on_progress, 
                                COALESCE(SUM(CASE WHEN status='approved-by-gas' then 1 ELSE 0 END),0) as jumlah_approve,
                                COALESCE(SUM(CASE WHEN status IN ('submitted-to-wdm','submitted-to-gas','return-by-gas','approved-by-wdm','revision-to-gas') then 1 ELSE 0 END),0) as jumlah_on_progress"))
                ->where('regional',$user->regional)
                ->first();

        if($user->hasRole('treg')){
            $tot=$data->jumlah_on_progress + $data->jumlah_approve;
        }

        return view('backend.dashboard', compact('data','treg','tot'));
    }

    public function showSubmission()
    {
		return view('backend.submission');
    }

    public function dataSubs(Request $request)
	{
		$draw = $request->get('draw')?$request->get('draw'):'';
		$start = $request->get('start')?$request->get('start'):'0';
		$length = $request->get('length')?$request->get('length'):'15';
		$filter = $request->get('search');
		$search = (isset($filter['value']))? $filter['value'] : false;

		$query = Submission::where(function($query) use($search) {
                return $query->where('name', 'LIKE', "%$search%")
                    ->orWhere('total_access_point', 'LIKE', "%$search%")
                    ->orWhere('verified_access_point', 'LIKE', "%$search%")
                    ->orWhere('regional', 'LIKE', "%$search%")
                    ->orWhere('created_at', 'LIKE', "%$search%")
                    ->orWhere('status', 'LIKE', "%$search%");
            });

        $total = $query->get()->count();
		
		$submission = $query->skip($start)->take($length)->orderBy('created_at', 'DESC')->get();
		return response()->json([
			'draw'=> $draw,
			'recordsTotal' => $total,
            'recordsFiltered' => $total,
			'results'=>$submission
		]);
    }
    
    public function destroySubs($id)
	{
		try {
			$user = auth()->user();
			$submission = Submission::where('uuid', $id)->firstOrFail();
			$submission->candidateAccessPoints()->delete();
			$submission->submissionTracks()->delete();
			$submission->documents()->delete();
			$submission->delete();

			$this->storeLog($user, $submission, 'deleted');

			return redirect()->back()->with('flash_success', 'Data berhasil dihapus.');
		} catch (\Exception $e) {
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
    }
    
    public function storeLog($user, $submission, $log){
		activity()
		->causedBy($user)
		->performedOn($submission)
		->withProperties(['uuid'=>$submission->uuid,
						'name' => $submission->name,
						'payment_value' => $submission->payment_value,
						'status' => $submission->status,
		])
		->log($log);
	}

    public function getRegionalData($id, $startYear, $startMonth, $endDate)
    {
        $regional1 =  DB::table('submissions')
                    ->select(
                        DB::raw("COALESCE(SUM(CASE WHEN status='approved-by-gas' AND nde_date >= '$startMonth' AND nde_date<='$endDate' then payment_value ELSE 0 END),0) as anggaran_mtd, 
                        COALESCE(SUM(CASE WHEN status='approved-by-gas' AND nde_date >= '$startYear' AND nde_date<='$endDate' then payment_value ELSE 0 END),0) as anggaran_ytd, 
                        COALESCE(SUM(CASE WHEN status='approved-by-gas' AND nde_date >= '$startMonth' AND nde_date<='$endDate' then 1 ELSE 0 END),0) as jumlah_pengajuan_mtd,
                        COALESCE(SUM(CASE WHEN status='approved-by-gas' AND nde_date >= '$startYear' AND nde_date<='$endDate' then 1 ELSE 0 END),0) as jumlah_pengajuan_ytd,
                        COALESCE(SUM(CASE WHEN status IN ('submitted-to-wdm','submitted-to-gas','return-by-gas','approved-by-wdm','revision-to-gas') then 1 ELSE 0 END),0) as jumlah_on_progress")
                    )->where('regional', $id)->first();

        $dataYear =  $this->getJumlahAP($id, $startYear, $endDate);
        
        $dataMonth =  $this->getJumlahAP($id, $startMonth, $endDate);

        $datas =  collect($regional1);
        $datas->put('jumlah_ap_mtd',$dataMonth->jumlah_ap);
        $datas->put('jumlah_ap_ytd',$dataYear->jumlah_ap);
        $datas->put('regional',"TREG $id");
        $datas->put('persentase',0);
        return $datas;
    }

    public function getJumlahAP($regional, $startDate, $endDate)
    {
        $result = DB::table('candidate_access_points as acp')
                    ->join('submissions as sub', function ($query) use($regional, $startDate, $endDate)
                    {
                        $query->on('sub.id','=','acp.submission_id')
                        ->where('sub.regional',$regional)
                        ->whereBetween('nde_date', [$startDate, $endDate]);
                    })
                    ->select(DB::Raw("count(*) as jumlah_ap"))->where('acp.status', 'verified')->where('sub.status','approved-by-gas')->first();
        return $result;
    }

    public function data(Request $request)
	{
        $startDateThisYear = date('Y').'-01-01';
        $startDateThisMonth = date('Y-m').'-01';
        $endDate = $request->has('date')?date('Y-m-d', strtotime($request->date)):date('Y-m-d');
        if($request->has('date')){
            $startDateThisYear = date('Y', strtotime($request->date)).'-01-01';
            $startDateThisMonth = date('Y-m', strtotime($request->date)).'-01';
        }

        $regional1 = $this->getRegionalData(1, $startDateThisYear, $startDateThisMonth, $endDate);
        $regional2 = $this->getRegionalData(2, $startDateThisYear, $startDateThisMonth, $endDate);
        $regional3 = $this->getRegionalData(3, $startDateThisYear, $startDateThisMonth, $endDate);
        $regional4 = $this->getRegionalData(4, $startDateThisYear, $startDateThisMonth, $endDate);
        $regional5 = $this->getRegionalData(5, $startDateThisYear, $startDateThisMonth, $endDate);
        $regional6 = $this->getRegionalData(6, $startDateThisYear, $startDateThisMonth, $endDate);
        $regional7 = $this->getRegionalData(7, $startDateThisYear, $startDateThisMonth, $endDate);
       
        $data = [
            $regional1,
            $regional2,
            $regional3,
            $regional4,
            $regional5,
            $regional6,
            $regional7
        ];

        $nasionalData = [
            'anggaran_mtd'=>0,
            'anggaran_ytd'=> 0,
            'jumlah_pengajuan_mtd'=> 0,
            'jumlah_pengajuan_ytd'=> 0,
            'jumlah_on_progress'=> 0,
            'jumlah_ap_mtd'=> 0,
            'jumlah_ap_ytd' => 0,
            'regional'=> "NASIONAL",
            'persentase'=>100,
        ];

        $totalApYtd = array_reduce($data, function($total, $value) {
            return $total + $value['jumlah_ap_ytd'];
        });

        foreach($data as $val){
            $val['persentase'] = $totalApYtd>0?round(($val['jumlah_ap_ytd']/$totalApYtd)*100, 1):0;
            $nasionalData['anggaran_mtd'] += $val['anggaran_mtd'];
            $nasionalData['anggaran_ytd'] += $val['anggaran_ytd'];
            $nasionalData['jumlah_pengajuan_mtd'] += $val['jumlah_pengajuan_mtd'];
            $nasionalData['jumlah_pengajuan_ytd'] += $val['jumlah_pengajuan_ytd'];
            $nasionalData['jumlah_on_progress'] += $val['jumlah_on_progress'];
            $nasionalData['jumlah_ap_mtd'] += $val['jumlah_ap_mtd'];
            $nasionalData['jumlah_ap_ytd'] += $val['jumlah_ap_ytd'];
        }

        $data[] =  $nasionalData;
        return response()->json([
            'results'=>$data
        ]);
	}

    public function trendline(){
        $user = auth()->user();
        $query = DB::table('submissions')
            ->select(
                DB::raw("
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=1 then payment_value ELSE 0 END),0) as jan,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=2 then payment_value ELSE 0 END),0) as feb,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=3 then payment_value ELSE 0 END),0) as mar,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=4 then payment_value ELSE 0 END),0) as apr,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=5 then payment_value ELSE 0 END),0) as mei,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=6 then payment_value ELSE 0 END),0) as jun,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=7 then payment_value ELSE 0 END),0) as jul,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=8 then payment_value ELSE 0 END),0) as agu,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=9 then payment_value ELSE 0 END),0) as sep,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=10 then payment_value ELSE 0 END),0) as okt,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=11 then payment_value ELSE 0 END),0) as nov,
                COALESCE(SUM(CASE WHEN EXTRACT(MONTH FROM nde_date)=12 then payment_value ELSE 0 END),0) as des")
            )->whereYear('nde_date', date('Y'));

        if($user->hasRole('treg')){
            $trendline= $query->where('regional',$user->regional)->first();
        }else{
            $trendline= $query->first();
        }

        return json_encode($trendline, JSON_NUMERIC_CHECK);
    }

    public function profile()
    {
        return view('backend.auth.profile.account');
    }

    public function showUploadView()
	{
		return view('backend.upload');
	}
}
