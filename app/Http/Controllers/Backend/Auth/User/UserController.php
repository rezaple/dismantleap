<?php

namespace App\Http\Controllers\Backend\Auth\User;

use DB;
use App\Models\Auth\User;
use Illuminate\Http\Request;
use App\Controllers\Traits\Otp;
use Illuminate\Validation\Rule;
use App\Jobs\SendCredentialEmail;
use App\Http\Controllers\Controller;
use App\Events\Backend\Auth\User\UserDeleted;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\Backend\Auth\PermissionRepository;
use App\Http\Requests\Backend\Auth\User\StoreUserRequest;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
use App\Http\Requests\Backend\Auth\User\UpdateUserRequest;

/**
 * Class UserController.
 */
class UserController extends Controller
{
    use Otp;
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function notifications()
    {
        return auth()->user()->notifications()->limit(10)->get()->toArray();
    }

    public function readAllNotification()
    {
        try {
            $unread = auth()->user()->unreadNotifications;
            
            if(!empty($unread)){
                auth()->user()->unreadNotifications->markAsRead();
                return response()->json([
                    'status' => 'Ok',
                    'message' => 'Success mark notifications as read'
                ],200);
            }

            return response()->json([
                'status' => 'Not Ok',
                'message' => "Nothing can't mark as read"
            ],200);
            
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Internal Server Error',
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function readNotification($id)
    {
        try {
            $notification = auth()->user()->notifications()->where('id', $id)->first();
            if($notification) {
                $notification->markAsRead();
            }
            return response()->json([
                'status' => 'Ok',
                'message' => 'Success mark notifications as read'
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Internal Server Error',
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function unReadNotification()
    {
        return auth()->user()->unreadNotifications()->get()->toArray();
    }

    public function showNotifications()
    {
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('backend.auth.user.notification', compact('notifications'));
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.auth.user.index')
            ->withUsers($this->userRepository->getActivePaginated(15, 'id', 'asc'));
    }

    /**
     * @param ManageUserRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function create(ManageUserRequest $request, RoleRepository $roleRepository)
    {
        $regionals = DB::table('witel')->select('regional')->orderBy('regional')->groupBy('regional')->get();
        return view('backend.auth.user.create')
            ->withRoles($roleRepository->with('permissions')->get(['id', 'name']))
            ->withRegionals($regionals);
    }

    /**
     * @param StoreUserRequest $request
     *
     * @throws \Throwable
     * @return mixed
     */
    public function store(StoreUserRequest $request)
    {
        if($request->has('ldap')){
            $data = $request->only(
                'ldap',
                'name',
                'username',
                'regional',
                'active',
                'roles'
            );
            $user = $this->userRepository->createUserLdap($data);
        }else{
            $user = $this->userRepository->create($request->only(
                'name',
                'username',
                'email',
                'password',
                'phone_number',
                'active',
                'confirmed',
                'confirmation_email',
                'roles',
                'regional'
            ));

            //send mail

			SendCredentialEmail::dispatch($request->only(
                'name',
                'username',
                'email',
                'password'
            ));
        }

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.created'));
    }

    /**
     * @param ManageUserRequest $request
     * @param User              $user
     *
     * @return mixed
     */
    public function show(ManageUserRequest $request, User $user)
    {
        return view('backend.auth.user.show')
            ->withUser($user);
    }

    /**
     * @param ManageUserRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param User                 $user
     *
     * @return mixed
     */
    public function edit(ManageUserRequest $request, RoleRepository $roleRepository, User $user)
    {
        $regionals = DB::table('witel')->select('regional')->orderBy('regional')->groupBy('regional')->get();
        return view('backend.auth.user.edit')
            ->withUser($user)
            ->withRoles($roleRepository->get())
            ->withRegionals($regionals)
            ->withUserRoles($user->roles->pluck('id')->all());
    }

    public function sendOtpTelegram(Request $request)
    {
        $username = auth()->user()->username;

        $response = $this->otpStatusCode($username);

        if($response && $response->getStatusCode() === 201){
            $cookie = cookie('input-pass', true, 5);
            return redirect()->back()->cookie($cookie);
        }

        return redirect()->back();
    }


    public function validateOtp(Request $request){

        $username = auth()->user()->username;

        $otpCode = $request->otp_code ?? '';

        $response = $this->otpStatusCode($username, $otpCode, 'check');

        $result = json_decode($response->getBody()->getContents());

        if($response && $response->getStatusCode() === 200){   
            $cookie = cookie('form-pass', true, 10);
            return redirect()->back()->cookie($cookie);
        }
        return redirect()->back()->with('flash_danger','OTP Code Invalid');
    }

    /**
     * @param UpdateUserRequest $request
     * @param User              $user
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     * @return mixed
     */
    public function update(Request $request, User $user)
    {
        if($user->ldap===1){
            $this->validate($request,[
                'username' => ['required', Rule::unique('users')->ignore($user->id, 'id')],
                'email' => ['required', 'email', Rule::unique('users')->ignore($user->id, 'id')],
                'name' => ['required'],
                'roles' => ['required','numeric'],
                'phone_number' => ['nullable',Rule::unique('users')->ignore($user->id, 'id')],
                'regional' => ['required','numeric'],
            ]);
            
        }else{
            $this->validate($request, [
                'username' => ['required', Rule::unique('users')->ignore($user->id, 'id')],
                'email' => ['required', 'email', Rule::unique('users')->ignore($user->id, 'id')],
                'name' => ['required'],
                'roles' => ['required','numeric'],
                'phone_number' => ['nullable',Rule::unique('users')->ignore($user->id, 'id')],
                'regional' => ['required','numeric'],
            ]);
        }

        $this->userRepository->update($user, $request->only(
            'name',
            'username',
            'email',
            'regional',
            'phone_number',
            'roles'
        ));

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.updated'));
    }

    /**
     * @param ManageUserRequest $request
     * @param User              $user
     *
     * @throws \Exception
     * @return mixed
     */
    public function destroy(ManageUserRequest $request, User $user)
    {
        $this->userRepository->deleteById($user->id);

        event(new UserDeleted($user));

        activity()->performedOn($user)->log('delete');

        return redirect()->route('admin.auth.user.deleted')->withFlashSuccess(__('alerts.backend.users.deleted'));
    }

    public function check(Request $request)
    {
        $username = $request->get('username', '');
        
        $phoneNumber = $request->get('phone', '');

        $response = $this->checkUser($username, $phoneNumber);
        
        return $response->getBody()->getContents();
    }
}
