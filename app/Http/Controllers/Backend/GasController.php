<?php

namespace App\Http\Controllers\Backend;

use DB;
use App\Models\Submission;
use App\Events\GasApproved;
use App\Events\GasRejected;
use App\Events\GasReturned;
use Illuminate\Http\Request;
use App\Jobs\GasSendRejectEmail;
use App\Jobs\GasSendApproveEmail;
use App\Http\Controllers\Controller;
use App\Models\CandidateAccessPoint;

class GasController extends Controller
{
    public function index()
	{
		return view('backend.pages.gas.index');
	}

	public function show($id)
	{
		try {
			$submission = Submission::with(['user', 'submissionTracks'=> function($query){
				$query->orderBy('created_at');
			}])->where('uuid', $id)->firstOrFail();
			return view('backend.pages.gas.show', compact('submission'));
		} catch (\Exception $e) {
			return redirect()->route('admin.gas.index')->with('flash_danger', $e->getMessage());
		}
	}

	public function listCAP(Request $request, $id)
	{
		$limit = $request->has('limit')? $request->limit: 10;
		$offset = $request->has('offset')? $request->offset: 0;
		$submission = Submission::with('user')->where('uuid', $id)->firstOrFail();
		$query = CandidateAccessPoint::where('submission_id',$submission->id)->where('status','verified');

		//datatables punya

		$draw = $request->get('draw')?$request->get('draw'):'';
		$start = $request->get('start')?$request->get('start'):'0';
		$length = $request->get('length')?$request->get('length'):'15';
		$filter = $request->get('search');
		$search = (isset($filter['value']))? $filter['value'] : false;
		

		// if($request->has('q')){
		// 	$search = $request->q;
			$query->where(function($query) use($search) {
                return $query->where('ap_name', 'LIKE', "%$search%")
                    ->orWhere('mac_address', 'LIKE', "%$search%")
                    ->orWhere('witel', 'LIKE', "%$search%")
                    ->orWhere('regional', 'LIKE', "%$search%")
                    ->orWhere('tipe', 'LIKE', "%$search%")
                    ->orWhere('periode', 'LIKE', "%$search%")
                    ->orWhere('note', 'LIKE', "%$search%")
                    ->orWhere('status', 'LIKE', "%$search%");
            });
		// }

		$total = $query->get()->count();
		//$ap = $query->skip($start)->take($length)->get();
		$ap = $query->get();
		// return response()->json([
		// 	'status' =>200,
		// 	'count' => $total,
		// 	'results'=>$ap
		// ]);

		return response()->json([
			'draw'=> $draw,
			'recordsTotal' => $total,
            'recordsFiltered' => $total,
			'results'=>$ap
		]);
	}

	public function list(Request $request)
	{
		$limit = $request->has('limit')? $request->limit: 10;
		$offset = $request->has('offset')? $request->offset: 0;
		$status = ['submitted-to-gas', 'review-by-gas', 'return-by-gas', 'revision-to-gas'];
		if($request->has('type')){
			$stat = $request->type;
			if($stat === 'approved'){
				$status = ['approved-by-gas'];
			}elseif($stat === 'rejected'){
				$status = ['reject-by-gas'];
			}
		}
		$query = DB::table('submissions as sub')
				->join('users','users.id', '=','sub.user_id')
				->whereIn('sub.status', $status)
				->select('sub.name','sub.uuid','sub.regional' ,'sub.payment_value','sub.nde_date', 'sub.total_access_point', 'sub.verified_access_point', 'sub.created_at', 'sub.status');

		if($request->has('q')){
			$search = $request->q;
			$query->where(function($query) use($search) {
                return $query->where('sub.name', 'LIKE', "%$search%")
                    ->orWhere('total_access_point', 'LIKE', "%$search%")
                    ->orWhere('verified_access_point', 'LIKE', "%$search%")
                    ->orWhere('sub.created_at', 'LIKE', "%$search%")
					->orWhere('status', 'LIKE', "%$search%")
					->orWhere('users.name', 'LIKE', "%$search%");
            });
		}

		$total = $query->get()->count();
		$submission = $query->skip($offset)->take($limit)
						->orderBy('sub.created_at', 'DESC')
						->get();
		return response()->json([
			'status' =>200,
			'count' => $total,
			'results'=>$submission
		]);
	}

	public function returnSubmission(Request $request, $id)
	{
		$message = $request->has('message')?$request->message:null;
		try {
			$user = auth()->user();
			$submission = Submission::where('uuid', $id)->whereIn('status', ['submitted-to-gas','revision-to-gas'])->firstOrFail();

			$submission->submissionTracks()->create([
				'user_id'=> $user->id,
				'title' => 'User GAS mengembalikan pengajuan',
				'message' => $message,
				'label' => 'warning',
				'status' => 'return-by-gas',
			]);

			$submission->update([
				'status' => 'return-by-gas'
			]);

			$this->storeLog($user, $submission, 'returned');
			
			event(new GasReturned($submission));

			return response()->json([
				'status' => 200,
				'message' => 'Pengajuan dikembalikan'
			], 200);
		} catch (\Exception $e) {
			return response()->json([
				'status' => 500,
				'message' => $e->getMessage()
			], 500);
		}
	}

	public function rejectSubmission(Request $request, $id)
	{
		$message = $request->has('message')?$request->message:null;
		try {
			$user = auth()->user();
			$submission = Submission::where('uuid', $id)->whereIn('status', ['submitted-to-gas','revision-to-gas'])->firstOrFail();

			$submission->submissionTracks()->create([
				'user_id'=> $user->id,
				'title' => 'User GAS menolak pengajuan',
				'message' => $message,
				'label' => 'danger',
				'status' => 'reject-by-gas',
			]);

			$submission->candidateAccessPoints()->update([
				'flag_dismantle' => 'reject'
			]);

			$submission->update([
				'status' => 'reject-by-gas'
			]);

			$this->storeLog($user, $submission, 'rejected');

			event(new GasRejected($submission));

			GasSendRejectEmail::dispatch($submission->user->email, $submission->toArray());
			
			return response()->json([
				'status' => 200,
				'message' => 'Pengajuan ditolak'
			], 200);
		} catch (\Exception $e) {
			return response()->json([
				'status' => 500,
				'message' => $e->getMessage()
			], 500);
		}
	}

	//update kandidat ap yang verified dengan tgl dismantle
	public function approveSubmission($id)
	{
		try {
			$user = auth()->user();
			$submission = Submission::where('uuid', $id)->whereIn('status', ['submitted-to-gas','revision-to-gas'])->firstOrFail();
			
			$submission->submissionTracks()->create([
				'user_id'=> $user->id,
				'title' => 'User GAS menyetujui pengajuan',
				'message' => '',
				'label' => 'success',
				'status' => 'approved-by-gas',
			]);

			$submission->update([
				'approved_date' => date('Y-m-d'),
				'status' => 'approved-by-gas'
			]);
			
			$submission->candidateAccessPoints()->update([
				'date_dismantle'=> $submission->nde_date,
				'flag_dismantle'=> 'OK'
			]);

			$this->storeLog($user, $submission, 'approved');


			event(new GasApproved($submission));

			GasSendApproveEmail::dispatch($submission->user->email, $submission->toArray());
			
			return response()->json([
				'status' => 200,
				'message' => 'Pengajuan disetujui'
			], 200);
		} catch (\Exception $e) {
			return response()->json([
				'status' => 500,
				'message' => $e->getMessage()
			], 500);
		}
	}


	public function storeLog($user, $submission, $log){
		activity()
		->causedBy($user)
		->performedOn($submission)
		->withProperties(['uuid'=>$submission->uuid,
						'name' => $submission->name,
						'payment_value' => $submission->payment_value,
						'status' => $submission->status,
		])
		->log($log);
	}

}
