<?php

namespace App\Http\Controllers\Backend;

use DB;
use File;
use App\Models\Auth\User;
use App\Models\Submission;
use App\Models\AccessPoint;
use Illuminate\Http\Request;
use App\Events\UserSubmitToGas;
use App\Events\UserSubmitToWdm;
use App\Events\UserRevisionToGas;
use App\Http\Controllers\Controller;
use App\Models\CandidateAccessPoint;
use Ramsey\Uuid\Uuid as PackageUuid;
use App\Jobs\TregSendRevisionToGasEmail;
use App\Jobs\TregSendSubmissionToGasEmail;
use App\Jobs\TregSendSubmissionToWdmEmail;

class PengajuanController extends Controller{

	public function index()
	{
		return view('backend.pages.pengajuan.index');
	}

	public function list(Request $request)
	{
		// $limit = $request->has('limit')? $request->limit: 10;
		// $offset = $request->has('offset')? $request->offset: 0;
		// $status = ['draft'];

		$draw = $request->get('draw')?$request->get('draw'):'';
		$start = $request->get('start')?$request->get('start'):'0';
		$length = $request->get('length')?$request->get('length'):'15';
		$filter = $request->get('search');
		$search = (isset($filter['value']))? $filter['value'] : false;

		if($request->has('type')){
			$stat = $request->type;
			if($stat === 'wdm'){
				$status = ['submitted-to-wdm', 'review-by-wdm', 'approved-by-wdm', 'return-by-wdm','reject-by-wdm'];
			}elseif($stat === 'gas'){
				$status = ['submitted-to-gas', 'review-by-gas', 'approved-by-gas', 'return-by-gas', 'reject-by-gas'];
			}
		}

		// $query = Submission::where('user_id', auth()->user()->id)->where('status',$status); jika ingin per tabs
		$query = Submission::where('regional', auth()->user()->regional);

		// if($request->has('q')){
		// 	$search = $request->q;
			$query->where(function($query) use($search) {
                return $query->where('name', 'LIKE', "%$search%")
                    ->orWhere('total_access_point', 'LIKE', "%$search%")
                    ->orWhere('verified_access_point', 'LIKE', "%$search%")
                    ->orWhere('created_at', 'LIKE', "%$search%")
                    ->orWhere('status', 'LIKE', "%$search%");
            });
		// }

		$total = $query->get()->count();
		//$submission = $query->skip($offset)->take($limit)->orderBy('created_at', 'DESC')->get();
		// return response()->json([
		// 	'status' =>200,
		// 	'count' => $total,
		// 	'results'=>$submission
		// ]);
		$submission = $query->skip($start)->take($length)->orderBy('created_at', 'DESC')->get();
		return response()->json([
			'draw'=> $draw,
			'recordsTotal' => $total,
            'recordsFiltered' => $total,
			'results'=>$submission
		]);
	}

	public function create()
	{
		return view('backend.pages.pengajuan.create');
	}

	public function createTest()
	{
		return view('backend.pages.pengajuan.create-test');
	}

	public function storeDoneData(Request $request)
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '0'); 
		DB::beginTransaction();
		try {			
			$file = $request->file('file');

			$uniqueNumb = time().rand(10,99);
			$nama_file = "_$uniqueNumb.".$file->getClientOriginalExtension();
			$file->move('kandidat_ap', $nama_file);

			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			$spreadsheet = $reader->load(public_path('kandidat_ap/'.$nama_file));
		
			$worksheet = $spreadsheet->getActiveSheet();
			$highestRow = $worksheet->getHighestRow(); // e.g. 10
			$highestColumn = $worksheet->getHighestColumn(); //e.g F

			$sheetData = $worksheet->rangeToArray("A2:{$highestColumn}{$highestRow }");
			foreach($sheetData as $data){
				if($data[3] !== null && $data[5] === 'OK'){
					$ap = AccessPoint::updateOrCreate(
						['mac_address' => $data[3]],
						[
							'name' => $data[2],
							'mac_address' => $data[3],
							'witel'  => $data[12],
							'regional'  => $data[13],
							'status_dismantle'  => $data[5],
							'status_ap'  => $data[10],
							'date_dismantle'  => empty($data[6]) ? null : date('Y-m-d',strtotime($data[6])),
							'type'  => $data[29],
						]
					);
					// $ap = AccessPoint::create([
					// 	'name' => $data[2],
					// 	'mac_address' => $data[3],
					// 	'witel'  => $data[10],
					// 	'regional'  => $data[11],
					// 	'status_dismantle'  => $data[5],
					// 	'status_ap'  => $data[8],
					// 	'date_dismantle'  => empty($data[6]) ? null : date('Y-m-d',strtotime($data[6])),
					// 	'type'  => $data[27],
					// ]);
				}
			}
			DB::commit(); 
			return response()->json([
				'status' => 200,
				'message' => 'Berhasil'
			]);
        } catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'status' => 500,
				'message' => $e->getMessage()
			]);
		}
	}

	//jadiin datatable
	public function show($id)
	{
		try {
			$submission = Submission::with('user','documents', 'submissionTracks')->where('regional', auth()->user()->regional)->where('uuid', $id)->firstOrFail();
			return view('backend.pages.pengajuan.show', compact('submission'));
		} catch (\Throwable $e) {
			return redirect()->route('admin.submission.index')->with('flash_danger', $e->getMessage());
		}
	}

	public function listCAP(Request $request, $id)
	{
		$limit = $request->has('limit')? $request->limit: 10;
		$offset = $request->has('offset')? $request->offset: 0;

		$draw = $request->get('draw')?$request->get('draw'):'';
		$start = $request->get('start')?$request->get('start'):'0';
		$length = $request->get('length')?$request->get('length'):'15';
		$filter = $request->get('search');
        $search = (isset($filter['value']))? $filter['value'] : false;
		
		$submission = Submission::with('user')->where('regional', auth()->user()->regional)->where('uuid', $id)->firstOrFail();
		$query = CandidateAccessPoint::where('submission_id',$submission->id);

		// if($request->has('search')){
		// 	$search = $request->q;
			$query->where(function($query) use($search) {
                return $query->where('ap_name', 'LIKE', "%$search%")
                    ->orWhere('mac_address', 'LIKE', "%$search%")
                    ->orWhere('witel', 'LIKE', "%$search%")
                    ->orWhere('regional', 'LIKE', "%$search%")
                    ->orWhere('tipe', 'LIKE', "%$search%")
                    ->orWhere('periode', 'LIKE', "%$search%")
                    ->orWhere('note', 'LIKE', "%$search%")
                    ->orWhere('status', 'LIKE', "%$search%");
            });
		// }

		$total = $query->get()->count();
		// $ap = $query->skip($start)->take($length)->get();
		$ap = $query->get();
		// return response()->json([
		// 	'status' =>200,
		// 	'count' => $total,
		// 	'results'=>$ap
		// ]);

		return response()->json([
			'draw'=> $draw,
			'recordsTotal' => $total,
            'recordsFiltered' => $total,
			'results'=>$ap
		]);
	}

	public function destroy($id)
	{
		try {
			$user = auth()->user();
			$submission = Submission::where('uuid', $id)->where('regional', $user->regional)->whereIn('status',['draft','reject'])->firstOrFail();
			$submission->candidateAccessPoints()->delete();
			$submission->submissionTracks()->delete();
			$submission->delete();

			$this->storeLog($user, $submission, 'deleted');

			return redirect()->back()->with('flash_success', 'Data berhasil dihapus.');
		} catch (\Exception $e) {
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
	}

	public function store(Request $request)
	{
		$user = auth()->user();

		$this->validate($request, [
            'file' => 'required|max:500|mimes:xlsx,csv,txt,xls',
			'name' => 'required|min:4|max:64'
		]);

		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '0'); 
		DB::beginTransaction();
		try {
			$file = $request->file('file');
			$totalVerified = 0;
			$totalUnVerified = 0;

			$sheetData = $this->getDataFromFile($file);

			$dataArray = str_replace(' ','', array_flatten($sheetData));
			$resultArray = array_map('strtolower', $dataArray);
			$listMacAddress = "'" . implode ( "', '", $resultArray ) . "'";
			//$totalAccessPoint = count($sheetData);

			if(count($sheetData) > 500){
				DB::rollback();
				return redirect()->back()->with('flash_danger', 'Data Mac Address melebihi 400 row.');
			}

			$sub = new Submission();
			$sub->name = $request->name;
			$sub->status = 'draft';
			$sub->regional = $user->regional;
			$submission = $user->submissions()->save($sub);
			$year = date('Y');

			// $kandidatAP = $this->getKandidatAP($listMacAddress);
			$kandidatAP = $this->listKandidatAP($listMacAddress, $year);
			//compare kandidatAP dan listmacaddress
			$listMacAddressValid = [];

			//cek by table access_point and candidate access point
			foreach($kandidatAP as $ap){
				$macAddress = strtolower($ap->mac_address);
				array_push($listMacAddressValid, $macAddress);
				//cari access point yang sudah di dismantle by mac address dan tahun ini, jika ditemukan maka statusnya unverified
				if($ap->tipe !== 'P2' && $ap->tipe !== 'P3'){
					$status = 'unverified';
					$totalUnVerified +=1;
					$note = 'MAC Address kandidat P1 tidak dapat dipertanggungkan.';
				}elseif($ap->regional !== $user->regional){
					$status = 'unverified';
					$totalUnVerified +=1;
					$note = 'Regional Access Point tidak sesuai dengan Regional User';
				}else{

					$accessPoint = AccessPoint::where("status_dismantle", 'OK')->whereRaw("LOWER(mac_address) = (?)", $macAddress)->whereYear('date_dismantle', $year)->orderBy('date_dismantle', 'DESC')->first();

					//jika akses point tidak ditemukan atau tgl akses point 
					if($accessPoint){
						$status = 'unverified';
						$totalUnVerified +=1;
						$note = 'Kandidat akses poin sudah pernah diajukan.';
					}else{
						//cari yang draft atau on progress
						$candidateAccessPoint = CandidateAccessPoint::whereRaw("LOWER(mac_address) = (?)", $macAddress)->whereNull('flag_dismantle')->where('status','verified')->orderBy('created_at', 'DESC')->first();

						//cari yang approved terus tahunnnya tahun sekarang
						$candidateAccessPoint2 = CandidateAccessPoint::whereRaw("LOWER(mac_address) = (?)", $macAddress)->where('flag_dismantle','OK')->whereYear('date_dismantle', $year)->first();

						if($candidateAccessPoint){
							$status = 'unverified';
							$totalUnVerified +=1;
							$note = 'Kandidat akses poin sedang dalam proses pengajuan.';
						}elseif($candidateAccessPoint2){
							$status = 'unverified';
							$totalUnVerified +=1;
							$note = 'Kandidat akses poin sudah pernah diajukan.';
						}else{
							$status = 'verified';
							$totalVerified +=1;
							$note = '';
						}
					}
				}
				
				//butuh kepastian data periode/tanggal dismantle untuk ap
				$submission->candidateAccessPoints()->create([
					'mac_address' => $ap->mac_address,
					'ap_name' => $ap->ap_name,
					'regional' => $ap->regional??0,
					'witel' => $ap->witel??'none',
					'periode' => $ap->periode??'',
					'tipe' => $ap->tipe,
					'status' => $status,
					'note' => $note
				]);
			}

			$result = array_diff($resultArray,$listMacAddressValid);
			if(count($result) > 0){
				foreach($result as $res){

					if(preg_match('/^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/i',$res)){
						$note = 'Mac Address bukan merupakan kandidat P2-P3.';
					}else{
						$note = 'Format Mac Address invalid';
					}
					
					$submission->candidateAccessPoints()->create([
						'mac_address' => $res,
						'ap_name' => '-',
						'regional' => '-',
						'witel' => '-',
						'periode' => '-',
						'tipe' => '-',
						'status' => 'unverified',
						'note' => $note
					]);
					$totalUnVerified +=1;
				}
			}

			$submission->submissionTracks()->create([
				'user_id'=> $user->id,
				'title' => 'User mengunggah daftar kandidat access point',
				'status' => 'draft'
			]);

			$submission->update([
				'verified_access_point' => $totalVerified,
				'unverified_access_point' => $totalUnVerified,
				'total_access_point' => $totalVerified + $totalUnVerified
			]);

			$this->storeLog($user, $submission, 'created');

			DB::commit(); 
			return redirect()->route('admin.submission.show', $submission->uuid)->with('flash_success', 'File berhasil diunggah');
        } catch (\Exception $e) {
            DB::rollback();
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
	}

	public function testStore(Request $request)
	{
		$year = date('Y');
		$user = auth()->user();

		$this->validate($request, [
            'file' => 'required|max:50000|mimes:xlsx,csv,txt,xls',
			'name' => 'required|min:4|max:64'
		]);

		DB::beginTransaction();
		try {
			$file = $request->file('file');
			$totalVerified = 0;
			$totalUnVerified = 0;

			$sheetData = $this->getDataFromFile($file);
			$dataArray = str_replace(' ','', array_flatten($sheetData));
			$resultArray = array_map('strtolower', $dataArray);
			$listMacAddress = "'" . implode ( "', '", $resultArray ) . "'";
			//$totalAccessPoint = count($sheetData);

			// $kandidatAP = $this->getKandidatAP($listMacAddress);
			$kandidatAP = $this->listKandidatAP($listMacAddress, $year);

			//compare kandidatAP dan listmacaddress
			$listMacAddressValid = [];
			$rest = [];

			//cek by table access_point and candidate access point
			foreach($kandidatAP as $ap){
				array_push($listMacAddressValid, strtolower($ap->mac_address));
				//cari access point yang sudah di dismantle by mac address dan tahun ini, jika ditemukan maka statusnya unverified
				if($ap->tipe !== 'P2' && $ap->tipe !== 'P3'){
					$status = 'unverified';
					$totalUnVerified +=1;
					$note = 'MAC Address merupakan kandidat P1 tidak dapat dipertanggungkan.';
				}elseif($ap->regional !== $user->regional){
					$status = 'unverified';
					$totalUnVerified +=1;
					$note = 'Regional Access Point tidak sesuai dengan Regional User';
				}else{

					$accessPoint = AccessPoint::where("status_dismantle", 'OK')->whereRaw("LOWER(mac_address) = (?)", $ap->mac_address)->whereYear('date_dismantle', $year)->orderBy('date_dismantle', 'DESC')->first();

					//jika akses point tidak ditemukan atau tgl akses point 
					if($accessPoint){
						$status = 'unverified';
						$totalUnVerified +=1;
						$note = 'Kandidat akses poin sudah pernah diajukan.';
					}else{
						//cari yang draft atau on progress
						$candidateAccessPoint = CandidateAccessPoint::whereRaw("LOWER(mac_address) = (?)", strtolower($ap->mac_address))->whereNull('flag_dismantle')->where('status','verified')->orderBy('created_at', 'DESC')->first();

						//cari yang approved terus tahunnnya tahun sekarang
						$candidateAccessPoint2 = CandidateAccessPoint::whereRaw("LOWER(mac_address) = (?)", strtolower($ap->mac_address))->where('flag_dismantle','OK')->whereYear('date_dismantle', $year)->first();

						if($candidateAccessPoint){
							$status = 'unverified';
							$totalUnVerified +=1;
							$note = 'Kandidat akses poin sedang dalam proses pengajuan.';
						}elseif($candidateAccessPoint2){
							$status = 'unverified';
							$totalUnVerified +=1;
							$note = 'Kandidat akses poin sudah pernah diajukan.';
						}else{
							$status = 'verified';
							$totalVerified +=1;
							$note = '';
						}
					}
				}
				
				array_push($rest, ['status'=>'valid','data'=>$ap, 'note' => $note]);
			}

			$result = array_diff($resultArray,$listMacAddressValid);
			if(count($result) > 0){
				foreach($result as $res){
					
					if(preg_match('/^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/i',$res)){
						$note = 'Mac Address bukan merupakan kandidat P2-P3.';
					}else{
						$note = 'Format Mac Address invalid';
					}
					
					array_push($rest, ['data'=>$res, 'message' => $note]);
				}
			}
			array_push($rest, ['kandidatAP' => $kandidatAP]);
			array_push($rest, ['listmacaddressvalid' => $listMacAddressValid]);
			array_push($rest, ['hasil' =>$result]);
			return $rest;
        } catch (\Exception $e) {
            DB::rollback();
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
	}

	public function getKandidatAP($listMacAddress)
	{
		$result = DB::connection('oracle')->select("select MAC_ADDRESS, AP_NAME, WITEL, REGIONAL, PERIODE, TIPE from 
						(
							select t1.MAC_ADDRESS, t1.AP_NAME, t1.WITEL, t1.REGIONAL, t1.PERIODE, t1.TIPE,
								ROW_NUMBER() OVER 
									(
									PARTITION BY MAC_ADDRESS ORDER BY PERIODE desc
									) as seqnum
							from MART_WIFI.KANDIDAT_AP_RELOKASI t1
						) 
						where seqnum=1 AND LOWER(MAC_ADDRESS) IN ($listMacAddress)");
		return $result;
	}

	public function listKandidatAP($listMacAddress, $year)
	{
		$result = DB::connection('oracle')->select("select MAC_ADDRESS, AP_NAME, WITEL, REGIONAL, PERIODE, TIPE from MART_WIFI.KANDIDAT_AP_RELOKASI WHERE PERIODE=$year AND LOWER(MAC_ADDRESS) IN ($listMacAddress)");
		return $result;
	}

	public function getDataFromFile($file)
	{
		$uniqueNumb = time().rand(10,99);
		$nama_file = "_$uniqueNumb.".$file->getClientOriginalExtension();
		$file->move('kandidat_ap', $nama_file);

		if($file->getClientOriginalExtension() === 'csv'){
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			$reader->setInputEncoding('UTF-8');
			$reader->setDelimiter(';');
			$reader->setEnclosure('');
			$reader->setSheetIndex(0);
		}else{
			//jika filenya xls atau office 2003 ke bawah
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}

		$spreadsheet = $reader->load(public_path('kandidat_ap/'.$nama_file));

		$worksheet = $spreadsheet->getActiveSheet();
		$highestRow = $worksheet->getHighestRow();
		$sheetData = $worksheet->rangeToArray("A1:A{$highestRow}");
		$cellColumnB = $spreadsheet->getActiveSheet()->getCell('B1')->getFormattedValue();

		if(empty($sheetData) || !empty($cellColumnB)){
			File::delete('kandidat_ap/'.$nama_file);
			throw new \Exception("File tidak sesuai format", 1);
		}
		File::delete('kandidat_ap/'.$nama_file);
		return $sheetData;
	}

	public function storeRevision(Request $request, $id)
	{
		$user = auth()->user();
		$this->validate($request, [
            'file' => 'required|max:50000|mimes:xlsx,csv,txt,xls',
		]);

		DB::beginTransaction();
		try {
			$file = $request->file('file');
			$totalVerified = 0;
			$totalUnVerified = 0;
			
			$sheetData = $this->getDataFromFile($file);

			$resultArray = str_replace(' ','',array_flatten($sheetData));
			$listMacAddress = strtolower("'" . implode ( "', '", $resultArray ) . "'");
			$totalAccessPoint = count($sheetData);

			$submission = Submission::where('regional', $user->regional)->where('uuid', $id)->whereIn('status', ['draft','return-by-wdm'])->firstOrFail();

			$submission->candidateAccessPoints()->delete();

			$kandidatAP = $this->getKandidatAP($listMacAddress);

			//cek by table access_point and candidate access point
			//cek periode nya
			foreach($kandidatAP as $ap){
				//cari access point yang sudah di dismantle by mac address dan tahun ini, jika ditemukan maka statusnya unverified
				//REVISI DISINI
				if($ap->tipe !== 'P2' && $ap->tipe !== 'P3'){
					$status = 'unverified';
					$totalUnVerified +=1;
					$note = 'Tipe Access Point tidak sesuai kriteria P2 atau P3';
				}elseif($ap->regional !== $user->regional){
					$status = 'unverified';
					$totalUnVerified +=1;
					$note = 'Regional Access Point tidak sesuai dengan regional User';
				}else{
					$accessPoint = AccessPoint::where("mac_address", $ap->mac_address)->whereYear('date_dismantle', '=', date('Y'))->first();

					$candidateAccessPoint = CandidateAccessPoint::where("mac_address", $ap->mac_address)->whereYear('periode', '=', date('Y'))->first();

					if($accessPoint){
						$status = 'unverified';
						$totalUnVerified +=1;
						$note = 'Kandidat akses poin sudah pernah diajukan.';
					}elseif($candidateAccessPoint){
						$status = 'unverified';
						$totalUnVerified +=1;
						$note = 'Kandidat akses poin sedang dalam proses pengajuan diajukan.';
					}else{
						$status = 'verified';
						$totalVerified +=1;
						$note = '';
					}
				}
				
				//butuh kepastian data periode/tanggal dismantle untuk ap
				$submission->candidateAccessPoints()->create([
					'mac_address' => $ap->mac_address,
					'ap_name' => $ap->ap_name,
					'regional' => $ap->regional??'none',
					'witel' => $ap->witel??'none',
					'periode' => $ap->periode??'',
					'tipe' => $ap->tipe,
					'status' => $status,
					'note' => $note
				]);
			}

			$submission->update([
				'total_access_point' => $totalAccessPoint,
				'verified_access_point' => $totalVerified,
				'unverified_access_point' => $totalUnVerified,
			]);

			$this->storeLog($user, $submission, 'Memperbarui daftar access point');

			DB::commit(); 
			return redirect()->route('admin.submission.show', $submission->uuid)->with('flash_success', 'File berhasil diunggah');
        } catch (\Exception $e) {
            DB::rollback();
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
	}

	public function submitToWDM($id)
	{
		DB::beginTransaction();
		try {
			$user = auth()->user();
			$submission = Submission::with('user')->where('regional', $user->regional)->where('uuid', $id)->where('status', 'draft')->firstOrFail();

			if($submission->unverified_access_point >= $submission->total_access_point){
				throw new \Exception("All candidate access point unverified", 1);
			}

			if($submission->verified_access_point > 400){
				throw new \Exception("Access point yang terverifikasi melebihi batas maksimum 400", 1);
			}

			$submissionTracks = $submission->submissionTracks()->create([
				'user_id'=> $user->id,
				'title' => 'User submit submission to WDM',
				'status' => 'submitted-to-wdm',
				'label' => 'info',
				'message' => 'Menunggu review oleh WDM'
			]);

			$submission->update([
				'status' => 'submitted-to-wdm'
			]);

			$this->storeLog($user, $submission, 'updated');

			event(new UserSubmitToWdm($user, $submission));
			
			TregSendSubmissionToWdmEmail::dispatch($submission->toArray());
			
			DB::commit(); 
			return redirect()->back()->with('flash_success', 'Menunggu review oleh WDM user');
        } catch (\Exception $e) {
            DB::rollback();
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
	}

	public function storeLog($user, $submission, $log){
		activity()
		->causedBy($user)
		->performedOn($submission)
		->withProperties(['uuid'=>$submission->uuid,
						'name' => $submission->name,
						'payment_value' => $submission->payment_value,
						'status' => $submission->status,
		])
		->log($log);
	}

	public function submitRevisionToWDM($id)
	{
		$user = auth()->user();
		DB::beginTransaction();
		try {
			$submission = Submission::where('regional', $user->regional)->where('uuid', $id)->where('status', 'return-by-wdm')->firstOrFail();

			if($submission->unverified_access_point >= $submission->total_access_point){
				throw new \Exception("Semua status kandidat access point tidak terverifikasi", 1);
			}

			$submission->submissionTracks()->create([
				'user_id'=> $user->id,
				'title' => 'User submit submission revision to WDM',
				'status' => 'revision-to-wdm',
				'label' => 'info',
				'message' => 'Menunggu review oleh WDM'
			]);

			$submission->update([
				'status' => 'revision-to-wdm'
			]);

			$this->storeLog($user, $submission, 'updated');

			DB::commit(); 
			return redirect()->back()->with('flash_success', 'Menunggu review oleh WDM user');
        } catch (\Exception $e) {
            DB::rollback();
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
	}

	public function submitRevisionToGAS($id)
	{
		$user = auth()->user();
		DB::beginTransaction();
		try {
			$submission = Submission::with('user')->where('regional', $user->regional)->where('uuid', $id)->where('status', 'return-by-gas')->firstOrFail();
			
			if($submission->unverified_access_point >= $submission->total_access_point){
				throw new \Exception("Semua status kandidat access point tidak terverifikasi", 1);
			}

			if(empty($submission->nde_date)){
				throw new \Exception("Tanggal NDE Date tidak boleh kosong.");
			}

			$submission->submissionTracks()->create([
				'user_id'=> $user->id,
				'title' => 'User submit submission revision to GAS',
				'status' => 'revision-to-gas',
				'label' => 'info',
				'message' => 'Menunggu review oleh GAS'
			]);

			$submission->update([
				'status' => 'revision-to-gas'
			]);

			$this->storeLog($user, $submission, 'updated');

			event(new UserRevisionToGas($user, $submission));

			TregSendRevisionToGasEmail::dispatch($submission->toArray());

			DB::commit(); 
			return redirect()->back()->with('flash_success', 'Menunggu review oleh GAS user');
        } catch (\Exception $e) {
            DB::rollback();
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
	}

	public function uploadDocument(Request $request, $id)
	{
		$user = auth()->user();
		$this->validate($request, [
			'nde_date' => 'required|date',
			'invoice_value' => 'required|regex:/^[0-9]{1,3}(.[0-9]{3})*\.[0-9]+$/',
			'invoice' => 'file|max:20000|mimes:pdf',
			'bast' => 'file|max:20000|mimes:pdf',
			'pajak' => 'file|max:20000|mimes:pdf',
			'spk' => 'file|max:20000|mimes:pdf'
		]);

		DB::beginTransaction();
		try {

			$sub = Submission::with('documents')->where('regional', $user->regional)->where('uuid', $id)->whereIn('status', ['approved-by-wdm', 'return-by-gas'])->firstOrFail();

			if($request->has('invoice')){
				list($inoviceName, $invoicePath) = $this->uploadFile($request->file('invoice'), 'invoice');
				$invoiceData = [
					'uuid' => PackageUuid::uuid4()->toString(),
					'submission_id' => $sub->id,
					'filename' => $inoviceName,
					'filepath' => $invoicePath,
					'type' => 'invoice'
				];

				//delete
				//$sub->documents()->insert($invoiceData);
				DB::table('documents')
					->updateOrInsert(
						['submission_id' => $sub->id, 'type' => 'invoice'],
						$invoiceData
					);
			}

			if($request->has('bast')){
				list($bastName, $bastPath) = $this->uploadFile($request->file('bast'), 'bast');
				$bastData = [
					'uuid' => PackageUuid::uuid4()->toString(),
					'submission_id' => $sub->id,
					'filename' => $bastName,
					'filepath' => $bastPath,
					'type' => 'bast'
				];
				//$sub->documents()->insert($bastData);
				DB::table('documents')
					->updateOrInsert(
						['submission_id' => $sub->id, 'type' => 'bast'],
						$bastData
					);
			}

			if($request->has('pajak')){
				list($pajakName, $pajakPath) = $this->uploadFile($request->file('pajak'), 'pajak');
				$pajakData = [
					'uuid' => PackageUuid::uuid4()->toString(),
					'submission_id' => $sub->id,
					'filename' => $pajakName,
					'filepath' => $pajakPath,
					'type' => 'pajak'
				];

				//$sub->documents()->insert($pajakData);
				DB::table('documents')
					->updateOrInsert(
						['submission_id' => $sub->id, 'type' => 'pajak'],
						$pajakData
					);
			}

			if($request->has('spk')){
				list($spkName, $spkPath) = $this->uploadFile($request->file('spk'), 'spk');
				$spkData = [
					'uuid' => PackageUuid::uuid4()->toString(),
					'submission_id' => $sub->id,
					'filename' => $spkName,
					'filepath' => $spkPath,
					'type' => 'spk'
				];

				//$sub->documents()->insert($spkData);
				DB::table('documents')
					->updateOrInsert(
						['submission_id' => $sub->id, 'type' => 'spk'],
						$spkData
					);
			}

			$sub->update([
				'payment_value' => str_replace('.','',$request->invoice_value),
				'nde_date' => $request->nde_date,
			]);

			$this->storeLog($user, $sub, 'updated and uploaded document');

			DB::commit(); 
			return redirect()->back()->with('flash_success', 'Berhasil memperbarui pengajuan');
        } catch (\Exception $e) {
            DB::rollback();
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
	}

	public function uploadFile($file, $folder){
		$originalName = str_replace(" ", "_", $file->getClientOriginalName());
		
		$name = time().rand(10,99)."_".$originalName;

		$filePath = $file->storeAs(
			$folder, $name, 'public');

		return [$name, $filePath];
	}

	public function submitToGAS($id)
	{
		$user = auth()->user();
		DB::beginTransaction();
		try {

			$submission = Submission::with('user','documents')->where('regional', $user->regional)->where('uuid', $id)->where('status', 'approved-by-wdm')->firstOrFail();

			if(empty($submission->nde_date)){
				throw new \Exception("Tanggal NDE tidak boleh kosong.");
			}

			if(count($submission->documents) < 4){
				throw new \Exception("Documents dibutuhkan.");
			}

			$submission->update([
				'status' => 'submitted-to-gas'
			]);

			$submission->submissionTracks()->create([
				'user_id'=> $user->id,
				'title' => 'User mengirim pengajuan invoice ke GAS',
				'status' => 'submitted-to-gas',
				'message' => 'Menunggu review oleh GAS'
			]);

			$this->storeLog($user, $submission, 'updated');

			TregSendSubmissionToGasEmail::dispatch($submission->toArray());

			event(new UserSubmitToGas($user, $submission));

			DB::commit(); 
			return redirect()->back()->with('flash_success', 'Menunggu review oleh GAS user');
        } catch (\Exception $e) {
            DB::rollback();
			return redirect()->back()->with('flash_danger', $e->getMessage());
		}
	}
}