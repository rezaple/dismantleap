<?php

namespace App\Http\Requests\Backend\Auth\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;
use LangleyFoxall\LaravelNISTPasswordRules\Rules\BreachedPasswords;
use LangleyFoxall\LaravelNISTPasswordRules\Rules\ContextSpecificWords;
use LangleyFoxall\LaravelNISTPasswordRules\Rules\DerivativesOfContextSpecificWords;
use LangleyFoxall\LaravelNISTPasswordRules\Rules\DictionaryWords;
use LangleyFoxall\LaravelNISTPasswordRules\Rules\RepetitiveCharacters;
use LangleyFoxall\LaravelNISTPasswordRules\Rules\SequentialCharacters;

/**
 * Class StoreUserRequest.
 */
class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required_if:ldap,0','string','min:4','max:64'],
            'username' => ['required','string', Rule::unique('users'),'min:6','max:32'],
            'email' => ['required_if:ldap,0', 'email', Rule::unique('users'),'max:64'],
            'phone_number' => ['required_if:ldap,0', Rule::unique('users'),'max:16'],
            'password' => [
                'required_if:ldap,0',
                'string',
                'min:12',
                'max:128',
                'confirmed',
                new SequentialCharacters(),
                new RepetitiveCharacters(),
                new DictionaryWords(),
                new ContextSpecificWords($this->email),
                new DerivativesOfContextSpecificWords($this->email),
            ],
            'regional' => ['required', 'numeric'],
            'roles' => ['required', 'numeric'],
        ];
    }
}
