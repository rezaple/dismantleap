<?php

namespace App\Providers;

use App\Listeners\SendSubmitSubmissionNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider.
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserSubmitToWdm' => [
            'App\Listeners\SendSubmitSubmissionToWdmNotification',
        ],
        'App\Events\UserSubmitToGas' => [
            'App\Listeners\SendSubmitSubmissionToGasNotification',
        ],
        'App\Events\UserRevisionToGas' => [
            'App\Listeners\SendRevisionSubmissionToGasNotification',
        ],
        'App\Events\GasApproved' => [
            'App\Listeners\SendGasApprovedSubmissionNotification',
        ],
        'App\Events\GasRejected' => [
            'App\Listeners\SendGasRejectedSubmissionNotification',
        ],
        'App\Events\GasReturned' => [
            'App\Listeners\SendGasReturnedSubmissionNotification',
        ],
        'App\Events\WdmApproved' => [
            'App\Listeners\SendWdmApprovedSubmissionNotification',
        ],
        'App\Events\WdmRejected' => [
            'App\Listeners\SendWdmRejectedSubmissionNotification',
        ],
    ];

    /**
     * Class event subscribers.
     *
     * @var array
     */
    protected $subscribe = [
        // Frontend Subscribers

        // Auth Subscribers
        \App\Listeners\Frontend\Auth\UserEventListener::class,

        // Backend Subscribers

        // Auth Subscribers
        \App\Listeners\Backend\Auth\User\UserEventListener::class,
        \App\Listeners\Backend\Auth\Role\RoleEventListener::class,
    ];

    /**
     * Register any events for your application.
     */
    public function boot()
    {
        parent::boot();

        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
