<?php
namespace App\Models;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class KandidatApRelokasi extends Eloquent {

    protected $table = 'MART_WIFI.KANDIDAT_AP_RELOKASI';

}