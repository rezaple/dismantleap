<?php

namespace App\Models;

use App\Models\Auth\User;
use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    use Uuid;

    protected $fillable =[
        'user_id',
        'name',
        'vendor_name',
        'vendor_no',
        'payment_value',
        'contract_value',
        'total',
        'total_access_point',
        'verified_access_point',
        'unverified_access_point',
        'contract_date',
        'submission_date',
        'approved_date',
        'nde_date',
        'regional',
        'tipe',
        'status'
    ];

    public function submissionTracks()
    {
        return $this->hasMany(SubmissionTrack::class);
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function candidateAccessPoints()
    {
        return $this->hasMany(CandidateAccessPoint::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
