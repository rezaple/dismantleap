<?php

namespace App\Models;

use App\Models\Auth\User;
use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class SubmissionTrack extends Model
{
    use Uuid;

    protected $fillable =[
        'submission_id',
        'user_id',
        'title',
        'message',
        'label',
        'status'
    ];

    public function submission()
    {
        return $this->belongsTo(Submission::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
