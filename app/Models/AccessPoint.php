<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessPoint extends Model
{
    protected $fillable = [
        'name',
        'mac_address',
        'witel',
        'regional',
        'status_dismantle',
        'status_ap',
        'date_dismantle',
        'type',
    ];
}
