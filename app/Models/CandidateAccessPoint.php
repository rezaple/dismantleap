<?php

namespace App\Models;

use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class CandidateAccessPoint extends Model
{
    use Uuid;

    protected $fillable =[
        'submission_id',
        'ap_name',
        'mac_address',
        'witel',
        'regional',
        'date_dismantle',
        'flag_dismantle',
        'tipe',
        'status',
        'note',
        'periode'
    ];

    public function submission()
    {
        return $this->belongsTo(Submission::class);
    }
}
