<?php

namespace App\Models;

use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use Uuid;

    protected $fillable =[
        'uuid',
        'submission_id',
        'filename',
        'filetype',
        'filepath',
        'filesize',
        'properties',
        'type'
    ];

    public function submission()
    {
        return $this->belongsTo(Submission::class);
    }
}
